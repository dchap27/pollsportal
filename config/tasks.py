from celery import shared_task
from project.account.models import User
from django.conf import settings

from email.mime.image import MIMEImage
from django.contrib.staticfiles import finders
import environ
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string

@shared_task
def new_vote(user_id, object_id):
    """
    Task to send an e-mail notification when a user voted on opinion poll.
    """
    from project.polls.models import Question
    performer = User.objects.get(id=user_id)
    question = Question.objects.select_related('owner').get(id=object_id)
    owner = question.owner

    # context for the email template
    context = {
        'owner': owner.username,
        'performer': performer.username,
        'question': question,
        'protocol':'https',
        'domain':settings.SITE_HOST
    }
    # set email variables
    subject = "A new vote occured on your poll"
    recipient = [owner.email]
    reply_to = ['noreply@pollsportal.com']

    html_content = render_to_string(
        "polls/mail/new_vote_message.html",
        context=context).strip()
    for file in ['polls/imgs/mail/img_poll1.jpg','polls/imgs/mail/pp-logo-new.png']:
        msg.attach(create_cid_image(file))
    msg = EmailMultiAlternatives(subject,html_content,settings.DEFAULT_FROM_EMAIL,
        to=recipient,reply_to=reply_to)
    msg.content_subtype = 'html'  # Main content is text/html
    msg.mixed_subtype = 'related'  # This is critical, otherwise images
    # will be displayed as attachments!
    msg.send()

@shared_task
def welcome(user_id):
    """
    Send a welcome email to the new user
    """
    try:
        user = User.objects.get(id=user_id)
    except:
        return
    # set email variables
    subject = "Welcome to PollsPortal"
    recipient = [user.email]
    reply_to = ['support@pollsportal.com']

    # context for the email template
    context = {
        'subject':subject,
        'name':user.username,
        'sender':'PollsPortal Team',
        'domain': settings.SITE_HOST,
        'protocol':'https',
    }
    html_content = render_to_string(
        "polls/mail/welcome_message.html",
        context=context).strip()

    msg = EmailMultiAlternatives(subject,html_content,'PollsPortal Team <support@pollsportal.com>',
        to=recipient,reply_to=reply_to)
    msg.content_subtype = 'html'
    for file in ['polls/imgs/mail/img_poll1.jpg','polls/imgs/mail/pp-logo-new.png']:
        msg.attach(create_cid_image(file))
    # to allow image to display properly and not attachments!
    msg.mixed_subtype = 'related'
    msg.send()

@shared_task
def invitation_mail(code, sender_id, recipient_email, recipient_name):
    """
    send an invitation email
    """
    try:
        sender = User.objects.get(id=sender_id)
    except:
        return
    subject = 'Invitation to PollsPortal Community'
    link = 'http://{}/polls/friend/accept/{}/'.format(settings.SITE_HOST,code)
    context = {
       'name' : recipient_name,
       'subject':subject,
       'link' : link,
       'inviter':sender,
       'domain':"www.pollsportal.com",
       'sender': 'PollsPortal Team',
    }
    html_content = render_to_string(
        "polls/mail/invitation_email.html",
        context=context).strip()

    msg = EmailMultiAlternatives(subject,html_content,settings.DEFAULT_FROM_EMAIL,
        to=[recipient_email])
    msg.content_subtype = 'html'
    for filename in ['polls/imgs/mail/img_poll1.jpg','polls/imgs/mail/pp-logo-new.png']:
        msg.attach(create_cid_image(filename))
    # to allow image to display properly and not attachments!
    msg.mixed_subtype = 'related'
    msg.send()


def create_cid_image(filename):
    """
    create cid image and get the path to the image
    """
    # find files from staticfiles
    try:
        with open(finders.find(filename), 'rb') as f:
            file_img = f.read()
    except TypeError: # exception if no match is found for filename
        return
    image = MIMEImage(file_img)
    name = (filename.split('/')) # split into list by '/'
    image.add_header('Content-ID', '<{}>'.format(name[-1]))
    image.add_header("Content-Disposition", "inline", filename=name[-1])
    return image
    