from channels.routing import route
from channels import route_class
from project.realtime.consumers import Demultiplexer

channel_routing = [
    # route("websocket.receive", ws_message),
    # route("websocket.connect", ws_connect, path="r^/notify/"),
    # route("websocket.disconnect", ws_disconnect),
    route_class(Demultiplexer, path="^/ws/"),
]
