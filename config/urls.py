"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include, handler404, handler500
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from project.polls import views
# import the verbs to ensure it is registered
from project.polls.verbs import Vote, Like, Share, UpdateProfile, FollowVerb

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^polls/', include('project.polls.urls',namespace='polls',
        app_name='polls')),
    url(r'^account/', include('project.account.urls', namespace='account',
        app_name='account')),
    url(r'^$', views.index, name='index'), # homepage
    # url(r'^index_graph/$', views.index_graph, name='index_graph'),
    url(r'^privacy-policy/$', views.privacy_policy, name='privacy'),
    url(r'^terms-of-service/$', views.terms_of_service, name='terms'),
    url(r'^help-using-our-services/$', views.help_page, name='help'),
    url(r'^feedback/$', views.feedback_page, name="feedback"),

    #Add social URL
    url(r'^social-auth/', include('social_django.urls', namespace='social')),
]

handler404 = views.error_404
handler500 = views.error_500
