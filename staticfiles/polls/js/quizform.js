$(document).ready(function() {

  // apply dynamic opening and closing of relevant input choices

  function showCorrespondingElement(el, action) {
    var $select_el_id = $(el).attr('id');
    var $open_answer_el = $(el).parent().parent().siblings('.answer-item')//$(el).parent().next();
    var $answer_el = $open_answer_el.children(':input');
    var selectedValue = document.getElementById($select_el_id).value
    var $options = $(el).parents('.section').children('fieldset');
    var $delete_boxes = $options.find(':checkbox');
    // $open_answer_el.hide();
    if(selectedValue === 'open'){
      $open_answer_el.show();
      if(action == 'clicked'){
        // check all options for delete
        $delete_boxes.each(function(){
          $(this).attr('checked','true');
        });
      }
      $options.hide()
      // show answer input box
    }
    else if (selectedValue === 'choice') {
      $options.show()
      // hide answer input box
      $open_answer_el.hide();
    }
  }
  // perform this action on opening of document
  $('select').each(function(){
    return showCorrespondingElement(this,null);
  })

  // perform this action on click
  $('select').click(function(){
    return showCorrespondingElement(this,'clicked');
  })

  /*
  code for adding new form in formset
  */
  // Code adapted from http://djangosnippets.org/snippets/1389/
    function updateElementIndex(el, prefix, ndx) {
        var id_regex = new RegExp('(' + prefix + '-\\d+-)');
        var replacement = prefix + '-' + ndx + '-';
        if ($(el).attr("for")) $(el).attr("for", $(el).attr("for").replace(id_regex,
        replacement));
        if (el.id) el.id = el.id.replace(id_regex, replacement);
        if (el.name) el.name = el.name.replace(id_regex, replacement);
    }

    function deleteForm(btn, prefix) {
        var formCount = parseInt($('#id_' + prefix + '-TOTAL_FORMS').val());
        if (formCount > 1) {
            // Delete the item/form
            $(btn).parents('.item').remove();
            var forms = $('.item'); // Get all the forms
            // Update the total number of forms (1 less than before)
            $('#id_' + prefix + '-TOTAL_FORMS').val(forms.length);
            var i = 0;
            // Go through the forms and set their indices, names and IDs
            for (formCount = forms.length; i < formCount; i++) {
                $(forms.get(i)).children().children().each(function () {
                    if ($(this).attr('type') == 'text') updateElementIndex(this, prefix, i);
                });
            }
        } // End if
        else {
            alert("You have to enter at least one option item!");
        }
        return false;
    }

    function addForm(btn, prefix) {
        var formCount = parseInt($('#id_' + prefix + '-TOTAL_FORMS').val());
        // You can only submit a maximum of 10 todo items
        if (formCount < 5) {
            // Clone a form (without event handlers) from the first form
            var row = $(".item:first").clone(false).get(0);
            // Insert it after the last form
            $(row).removeAttr('id').hide().insertAfter(".item:last").slideDown(300);

            // Remove the bits we don't want in the new row/form
            // e.g. error messages
            $(".errorlist", row).remove();
            $(row).children().removeClass("error");

            // Relabel or rename all the relevant bits
            $(row).children().children().each(function () {
                updateElementIndex(this, prefix, formCount);
                $(this).val("");
            });
            // this lines was added to suit the layout of the form which was Adjusted
            // with JavaScript
            $(row).children().children().children().each(function () {
                updateElementIndex(this, prefix, formCount);
                $(this).val("");
            });

            // Add an event handler for the delete item/form link
            $(row).find(".delete").click(function () {
                return deleteForm(this, prefix);
            });
            // Update the total form count
            $("#id_" + prefix + "-TOTAL_FORMS").val(formCount + 1);
        } // End if
        else {
            alert("Sorry, you can only enter a maximum of 5 options.");
        }
        return false;
    }
  // Register the click event handlers
    $("#add-option").click(function () {
        var prefix;
        var id = $(this).attr('prefix');
        if(id === ""){
          prefix = "choices"
        }else{
          prefix = "choices"+id
        }
        return addForm(this, prefix);
    });

    $(".delete").click(function () {
        var prefix;
        var id = $(this).attr('prefix');
        if(id === ""){
          prefix = "choices"
        }else{
          prefix = "choices"+id
        }
        return deleteForm(this, prefix);
    });

  $('form#add-quiz-question').submit(function(){

    // submit form load response into target
    $(this).ajaxSubmit(
      {
        target: '.target',
        clearForm: true
      }
    );
    return false;
  })

  $('form#single_question_edit').submit(function(){
    // get the next url
    var url = $('form p.initial_data').attr('url');
    var $parentContainer = $('div.main-quiz-form-container')
    // submit form load response into target
    $(this).ajaxSubmit(
      {
        target: '.target',
        success: function(){
          $parentContainer.empty();
          var msg = '<h5 class="text-success">Question edited successfully</h5>\
            <a class="btn btn-primary" href="' + url + '">Continue Editing</a>'
            $(msg).prependTo($parentContainer);
        }
      }
    );
    return false;
  })

  /*
  hide choices if selection is open ended
  */
  // execute for chrome browser (open_answer input for quiz questions)
  $('form#add-quiz-question').click(function(event){
    if(($(event.target).is('#id_quizquestions-0-question_type')) || ($(event.target).is('#id_quizquestions-0-question_type > option')) ){
      var selectedValue = document.getElementById('id_quizquestions-0-question_type').value
      var $target = $(event.target).parent();
      if(selectedValue == 'open'){
        // remove answer-item if already exists and avoid duplication
        $('p.answer-item').remove();
        $('.item').hide(); // hide options
        $('#add-option').hide(); // hide option addition button
        // set an input[text] for question_type and show
        $('<p></p>')
          .addClass('answer-item required')
          .html('<label class="control-label col-sm-4" for="id_open_answer">provide an answer:</label>\
          <div class="col-sm-5">\
          <input type="text"  class="form-control" name="quizquestions-open_answer" id="id_open_answer" />\
          </div>')
          .insertAfter($target);
      }else {
        $('p.option-text-guide').show();
        $('.item').show(); // show options
        $('#add-option').show(); // show option addition button
        // remove input for question_type
        $('p.answer-item').remove();
      }
    }
  });

  $('form#edit-quiz').click(function(event){
    var timeLimit = $('p.duration-value').attr('duration');
    if(($(event.target).is('#id_newquiz-mode')) || ($(event.target).is('#id_newquiz-mode > option')) ){
      var selectedValue = document.getElementById('id_newquiz-mode').value
      var $target = $(event.target).parent().parent('.form-group'); // element position new input-text
      if(selectedValue === 'timed'){
        // remove duration-item element if already exists and avoid duplication
        $('p.duration-item.required').remove();
        // set an input[text] for quiz_duration and show
        var html1 = '<p class="form-group duration-item required"></p>'
        var html2 = '<label class="control-label col-sm-4" for="id_quiz_duration">Time limit (in Minutes):</label>';
        var html3 = '<div class="col-sm-5">';
        html3 += '<input type="number" class="form-control" value="' +timeLimit+ '"' +'name="newquiz-quiz_duration" min="0" id="id_quiz_duration" />';
        html3 += '</div>';
        $(html1).append($(html2)).append($(html3)).insertAfter($target);

        // prevent user from entering any char except digits
        $('#id_quiz_duration').keypress(function(event) {
          if (event.charCode && (event.charCode < 48 || event.charCode > 57))
          {
            event.preventDefault();
          }
        });
      } else {
        // remove input for question_type
        $('p.duration-item.required').remove();
      }
    };
    if($(event.target).is('a#save-continue')){
      $(this).ajaxSubmit({
         target: '.main-target',
      })
      return false;
    };

  })

  $('form.new-quiz').click(function(event){
    if(($(event.target).is('#id_newquiz-mode')) || ($(event.target).is('#id_newquiz-mode > option')) ){
      var selectedValue = document.getElementById('id_newquiz-mode').value
      var $target = $(event.target).parent().parents('.form-group'); // element position new input-text
      if(selectedValue === 'timed'){
        // remove duration-item element if already exists and avoid duplication
        $('p.duration-item').remove();
        // set an input[text] for quiz_duration and show
        $('<p></p>')
          .addClass('form-group duration-item required')
          .html('<label class="control-label col-sm-4" for="id_quiz_duration">Time limit (in Minutes):</label>\
          <div class="col-sm-5">\
          <input type="number" class="form-control" name="newquiz-quiz_duration" min="0" id="id_quiz_duration" />\
          </div>')
          .insertAfter($target);
        // prevent user from entering any char except digits
        $('#id_quiz_duration').keypress(function(event) {
          if (event.charCode && (event.charCode < 48 || event.charCode > 57))
          {
            event.preventDefault();
          }
        });
      } else {
        // remove input for question_type
        $('p.duration-item').remove();
      }
    };
  })

  $('div.section').click(function(event){
    if($(event.target).is('a.edit-question')){
      var $target = $('.main-quiz-form-container');
      // var $target = $(event.target).parent().parent();
      event.preventDefault();
      $.ajax({
        type: 'get',
        url: $(event.target).attr('href'),
        data: {},
        success: function(data){
          $target.empty();
          $target.append(data);
        },
        error: function(){}
      })
    }
  })

  // validate input in form
  // prevent user from entering any char except digits
  $('#id_newquiz-attempt').keypress(function(event) {
    if (event.charCode && (event.charCode < 48 || event.charCode > 57))
    {
      event.preventDefault();
    }
  });

})

$(document).ready(function() {
  $('<div class="loading">loading...</div>')
    .ajaxStart(function() {
      $('.main-target').fadeIn();
      $(this).prependTo('.main-target');
    }).ajaxStop(function() {
      $(this).hide();
    });
});
