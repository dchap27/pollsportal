$(document).ready(function() {

    // get create poll page and insert into .main-poll-div
    $('#side-menu .create-poll-btn').click(function(){
      var username = $('#id_user a').text();
      $('.main-poll-div').load("/polls/" + encodeURIComponent(username) + "/save/");
      return false;
    });
    $('#page-wrapper').click(function(event){
      if($(event.target).is('h4.show-comment-box')){
        $('.comment-box').toggleClass('hidden');
      };
    })

    // search button
    $("#index-search2 button").click(function (event) {
      var query = $("#id_query2").val();
      if(query !== ''){
          $('.search-result').load(
            "polls/search/?ajax&query=" + encodeURIComponent(query));
            return false;
      }
    });
    $("#index-search button").click(function (event) {
      var query = $("#id_query").val();
      if(query !== ''){
          $('.search-result').load(
            "polls/search/?ajax&query=" + encodeURIComponent(query));
            return false;
      }
    });
    // pressing keys or submit for mobile search
    $('#index-search').keypress(function(event){
      var query = $("#id_query").val();
      // keyCode 13 is user pressed 'enter key'
      if(event.keyCode == 13 && query !== ''){
        $('.search-result').load(
          "polls/search/?ajax&query=" + encodeURIComponent(query));
          return false;
      }else if (event.keyCode == 13 && query === '') {
        // do nothing since query is empty
        event.preventDefault();
      }
    })

    // pressing keys or submit for mobile search
    $('#index-search2').keypress(function(event){
      var query = $("#id_query2").val();
      // keyCode 13 is user pressed 'enter key'
      if(event.keyCode == 13 && query !== ''){
        $('.search-result').load(
          "polls/search/?ajax&query=" + encodeURIComponent(query));
          return false;
      }else if (event.keyCode == 13 && query === '') {
        // do nothing since query is empty
        event.preventDefault();
      }
    })

    $('#main-feeds').click(function(event){
      // load result into same div
      if($(event.target).is('.result-link')){
        var url = $(event.target).attr('href');
        event.preventDefault();
        $($(event.target).parents('.panel-body')).load(url);
      }
      // load question_ feed back into same div
      if($(event.target).is('.question-link')){
        var url = $(event.target).attr('href');
        event.preventDefault();
        $($(event.target).parents('.panel-body')).load(url);
      }

      // view comments after clicking
      if($(event.target).is('.view-comments')){
        var url = $(event.target).attr('url');
        var pos = $(event.target).attr('position');
        // $('.search-result').load(url);
        event.preventDefault();
        $($(event.target).parents('.panel-body')).load(url);

        // $('.comment-'+pos).toggleClass('hidden');
      }
      // submit comments in same question_feed div
      if($(event.target).is("form #submitcomment")){
        var question_id = $(event.target).attr('questionid');
        var url = $(event.target).parents('form').attr('action');
        event.preventDefault();
        var $targetLocation = $(event.target);
        var $body = $(event.target).parents('form').children().children() // textarea element
        var $copiedElement = $('#ajax-loading-image img').clone();
        // setup ajax loader
        $.ajax({
              global: false,
              type: "POST",
              url: url,
              data: {question_id,
                    'body':$body.val(),
                    csrfmiddlewaretoken:$targetLocation.parent().prev().val(),
                  },
              beforeSend: function () {
                 $copiedElement.insertAfter($targetLocation);
              },
              complete: function () {
                  $copiedElement.remove();
              },
              // dataType: "json",
              success: function(response) {
                $($(event.target).parents('.panel-body')).load(response['data']);
                     //window.location.reload();
               },
               error: function() {
                      alert('error');
               }
        });
      }
      // share button
      if($(event.target).is('.share-count')){
        var url = $(event.target).attr('href');
        var poll_type = $(event.target).attr('poll_type');
        $('#shareModal .modal-footer').addClass('hidden');
        // $('#shareModal').modal('hide');
        event.preventDefault();
        // load share content into share modal
        $.ajax({
          type: "GET",
          url: url,
          data:{
          },
          success:function(data){
            $('#shareModal .modal-body').html(data);
            var $title = $('.modal-body .shared-content').children('div.content-title');
            $('#shareModal .modal-header .modal-title').html($title);
          },
          error: function(){
            // something happened
          },
          complete: function(){
            $('#shareModal').modal({
              keyboard: true,
              backdrop: true, // allow closing when clicked outside
            });
            $('#shareModal').on('show.bs.modal',function(){
              //
            })
          }

        });

      }
      // like button ajax code to asynchronously
      if($(event.target).is('.col-xs-3 a#mylike')){
        var question_id = $(event.target).attr('name');
        var pollType = $(event.target).attr('poll_type');
        // event.preventDefault();
        // var liked_color = $(".col-sm-12 > .panel > .feed-footer > .row > .col-xs-3 > a#mylike").css('color');
        $.ajax({
                 type: "POST",
                 url: "/polls/like/"+question_id+"/",
                 data: {question_id,
                        poll_type:pollType,
                        action : $(event.target).attr('data-action'),
                       csrfmiddlewaretoken:$(event.target).attr('csrf'),
                     },
                 dataType: "json",
                 success: function(response) {
                   if (response['status'] == '200')
                      {
                        var previous_action = $('a.mylike'+question_id).data('action');

                        // toggle data-action
                        $('a.mylike'+question_id).data('action', previous_action == 'like' ?
                          'unlike' : 'like');
                        // toggle link text
                        $('a.mylike'+question_id).text(previous_action == 'like' ? 'Unlike' : 'Like');
                          // update total likes
                        var previous_likes = parseInt($('span.count'+question_id).text());
                        $('span.count'+question_id).text(previous_action == 'like' ? previous_likes + 1 : previous_likes - 1);
                      }
                        //window.location.reload();
                  },
                  error: function() {
                         alert("error!!");
                  }
            });
      }

      if($(event.target).is('div.vote-btn')){
        // code for voting asynchronously
        $('div.vote-error-message').remove();
        var question_id = $(event.target).attr('questionId');
        var loopCounter = $(event.target).attr('loopnum');
        var choice = $(event.target).attr('data-value');
        var $form = $(event.target).parents('form');
        var $copiedElement = $('#ajax-loading-image img').clone();
        var $targetLocation;
        // var choice = $(event.target).children('#choice'+loopCounter).val();
        $.ajax({
                 type: "POST",
                 url: "/polls/vote/"+question_id+"/",
                 data: {question_id,
                       choice,
                       csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
                     },
                 beforeSend: function () {
                    $('#shareModal .modal-footer').addClass('hidden');
                    $('#shareModal .modal-header').addClass('hidden');
                    $('#shareModal .modal-body').html($copiedElement);
                    $('#shareModal').modal({
                      keyboard: false,
                      backdrop: false, // allow closing when clicked outside
                    });
                 },
                 complete: function () {
                     $('#shareModal').modal('hide');
                 },
                 success: function(data) {
                        if(data['status']=='ok'){
                          $($(event.target).parents('.panel-body')).load(data['success_url']);
                          var total_votes = data['total_votes'];
                          $('span#vote-count'+question_id).text(total_votes);
                        }else {
                          $('<div></div>').addClass('warning vote-error-message')
                            .html('<h5>'+data['message']+'</h5>')
                            .insertBefore($form);
                        }
                        // window.location.reload();
                  },
                  error: function() {
                    $('<div></div>').addClass('warning vote-error-message')
                      .html('<h5>Something went wrong. Try again later!</h5>')
                      .insertBefore($form);
                  }
            });
      }


    });

    $('#action-list').click(function(event){
      if($(event.target).is('a#action-performer')){
        event.preventDefault();
        var username = $(event.target).attr('user_name');
        var position = $(event.target).attr('position');
        if (username != 'You'){
          $($(event.target).parents('.panel-primary'))
            .load("/polls/user/" + encodeURIComponent(username));
        }
      }
      if($(event.target).is('.panel-primary .panel-heading a#follow')){
          var user_id = $($(event.target)).attr('name');
          event.preventDefault();
          $.ajax({
                   type: "POST",
                   url: "/polls/friend/follow/",
                   data: {user_id,
                          action : $($(event.target)).attr('data-action'),
                         csrfmiddlewaretoken:$($(event.target)).attr('csrf'),
                       },
                   dataType: "json",
                   success: function(response) {
                     if (response['status'] == 'ok')
                        {
                          var previous_action = $('a#follow').data('action');

                          // toggle data-action
                          $('a#follow').data('action', previous_action == 'follow' ?
                            'unfollow' : 'follow');
                          // toggle link text
                          $('a#follow').text(previous_action == 'follow' ?
                            'unfollow' : 'follow');
                            // update total followers

                        }
                    },
                    error: function() {
                           alert("error!!");
                    }
              });
      }
    });


    $('.question-feed #pending-btn').click(function(event){
      alert("Poll is pending! Will be available soon!!");
    });

    // if($('.determine-feeds-loading > button#load-feeds')){
    //   alert('nice');
    // }

    var page = 1;
    var empty_page = false;
    var block_request = false;
    $('button#load-feeds').removeAttr('disabled')

    // $('button#load-feeds').click(function() {
    $(window).scroll(function(){
      var margin = $(document).height() - $(window).height() - 200;
      if ($(window).scrollTop() > margin && empty_page == false &&
    block_request == false) {
      block_request = true;
      page += 1;
      $.get('?page=' + page, function(data) {
        if(data == '') {
          $('button#load-feeds').removeClass('btn-primary').attr({'disabled':'disabled'}).text('No more feeds');
          empty_page = true;
        }
        else {
          block_request = false;
          $('#main-feeds').append(data);
          $('.share-count').each(function(){
            var $share = $(this);
            $share.getNumberOfShares();
          });
        }
      });
    }
  });
});


$(document).ready(function(){
  // create a plug-in for internal use
  jQuery.fn.getNumberOfShares = function(){
    var question_id = this.attr('questionid');
    var poll_type = this.attr('poll_type');
    var $result = this.children('span.badge');
      // performe ajax asynchronous task to get total_shares
    $.ajax({
      type: 'GET',
      url: "/polls/share/ranking/?question_id="+question_id,
      data: {
        poll_type,
      },
      dataType: 'json',
      success: function(response){
        var total_shares = response['total_shares'];
        $result.text(total_shares);
      },
      error: function(){
        //
      }
    });
    return this;
  }
  // iterate over all the share-count class
    $('.share-count').each(function(){
      var $share = $(this);
      $share.getNumberOfShares();
    });
})
