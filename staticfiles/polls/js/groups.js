$(document).ready(function(){

  function displaySuccessMessage(result_el,messageColor,message){
    // display a success message and disappear in 10seconds
    var message = $('<div></div>')
                      .addClass(messageColor)
                      .append(message)
                      .insertAfter(result_el)
                      .fadeIn('fast').delay(10000).fadeOut('slow')
    return message
  }

  // Add a new participant to group
  $('.list-group').click(function(event){

    if($(event.target).is('a.add-new-member')){
      var result;
      event.preventDefault()
      $.ajax({
        type: "get",
        url: $(event.target).attr('href'),
        success: function(data){
          result = data['friends'];
          alert(result)
        }
      })

      // // load the form script
      // $.getScript('/static/polls/js/questionForm.js');
      // event.preventDefault()
      // var group_name = $(event.target).attr('group_name');
      // // create a form element
      // var group = '<input type="hidden" name="group_name" id="id_group_name" value="'+ group_name +'\">'
      // var html = '<form id="add-new-member" method="post" action="/account/my-group-list/">' + group
      // html += '<div class="form-group">'
      // html += '<label for="id_new_member_email">Member\'s email</label>'
      // html += '<div class="fieldwrapper">'
      // html += '<input class="form-control email required" type="text" name="new_member_email" required id="id_new_member_email" maxlength="120">'
      // html += '</div>'
      // html += '</div>'
      // html += '<input type="submit" class="btn btn-primary btn-sm" name="submit" value="add"> <span class="clickable cancel-add-member">cancel</span>'
      //
      // $(event.target).parent().append(html)
      $(event.target).toggleClass('hidden')

    }

    if($(event.target).is('.cancel-add-member')){
      $(event.target).parents('.list-group-item').find('a.add-new-member')
        .removeClass("hidden")
      $(event.target).parents('.list-group-item').find('form')
        .remove()
    }
    // remove membership from group
    if($(event.target).is('.remove-member')){
      var group = $(event.target).attr('group_name');
      var action = confirm("Delete your membership to "+group+"?");
      if(action==true){
        var display_el = $(event.target).prev();
        var status;
        var message;
        var messageColor;
        $.ajax({
          type: "POST",
          url: "/account/my-group-list/",
          data: {
            'remove-membership':true,
            'group_name':group,
          },
          success: function(response){
            status = response['status']
            message = response['message']
            // determine the text color
            if(status=='ok'){
              messageColor = "text-success";
              // remove previous messages
              $('.text-success').remove()
            }else{
              messageColor = 'text-danger';
              $('.text-danger').remove()
            }
            displaySuccessMessage(display_el,messageColor,message)
          },
          complete: function(){
            if(status=='ok'){
              // remove group from list
              $(event.target).parent().remove();
            }
          }
        });
      };
    };
    if($(event.target).is('.delete-group')){
      event.preventDefault()
      var group_name = $(event.target).attr('group_name');
      var action = confirm("Delete group: "+group_name.toUpperCase()+"?");
      if(action==true){
        var display_el = $(event.target).prev();
        var status;
        var message;
        var messageColor;
        $.ajax({
          type: "POST",
          url: "/account/my-group-list/",
          data: {
            'delete-group':true,
            group_name,
          },
          success: function(response){
            status = response['status']
            message = response['message']
            // determine the text color
            if(status=='ok'){
              messageColor = "text-success";
              // remove previous messages
              $('.text-success').remove()
            }else{
              messageColor = 'text-danger';
              $('.text-danger').remove()
            }
            displaySuccessMessage(display_el,messageColor,message)
          },
          complete: function(){
            if(status=='ok'){
              // remove group from list
              $(event.target).parent().remove();
            }
          }
        })
      }
    }

  });

})
