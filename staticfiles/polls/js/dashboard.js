$(document).ready(function() {

  $('#all-polls-table').click(function(event){
    if($(event.target).is('.custom-settings')){
      var url = $(event.target).attr('href');
      var status = $(event.target).attr('status');
      event.preventDefault();

      $.ajax({
        type: 'GET',
        url: url,
        data: {},
        success: function(data){
          $('#settings-div').empty();
          $('#settings-div').append(data);
          // Adjust its presentation
          $('#settings-div form').parents('div.col-sm-8').addClass('col-sm-10')
            .removeClass('col-sm-8')
        },
        complete: function(){
          if(status == "Closed"){
            $('#id_who_can_respond').parents('.fieldwrapper').prev('label')
              .addClass('hidden');
            $('#id_who_can_respond').addClass('hidden');
            $('#id_presentation').parents('.fieldwrapper').prev('label')
              .addClass('hidden');
            $('#id_presentation').addClass('hidden');
            $('#settings-div form').removeClass('form-vertical')
              .addClass('form-horizontal');

          }
          // load the plug-in
          $('#settings-div form').showSelectiveElements();
          // $.getScript('/static/polls/questionForm.js')
        },
        error: function(){
        }
      })
    };
  })
  // spruce the table with rows color but exclude the settings row
  // $('#all-polls-table tbody tr:not(#settings-div) td:even').addClass('evenrows');
  // $('#all-polls-table tbody tr:not(#settings-div):odd').addClass('oddrows');

  $('.panel-body').click(function(event){
    if($(event.target).is('a#dashboard-prev')){
      var page = $(event.target).attr('pageInView');
      event.preventDefault();
      page -= 1;
      $.get('?page=' + page, function(data) {
        if(data == '') {
          // empty_page = true;
          // do nothing
        }
        else {
          // block_request = false;
          $('.panel-body').html(data);
        }
      });
    }
    if($(event.target).is('a#dashboard-next')){
      var page = parseInt($(event.target).attr('pageInView'));
      event.preventDefault();
      page += 1;
      $.get('?page=' + page, function(data) {
        if(data == '') {
          // empty_page = true;
          // do nothing
        }
        else {
          // block_request = false;
          $('.panel-body').html(data);
        }
      });
    }

    // remove a poll from database (suspend)
    if($(event.target).is('a#remove-poll i')){
      var question_id = $(event.target).parents('a#remove-poll').attr('questionid');
      var poll_type = $(event.target).parents('a#remove-poll').attr('poll-type');
      var action = confirm("Delete this poll?");
      if(action == true){
        // $(event.target).parents('a#remove-poll').preventDefault();
        // send request
        $.ajax({
                 type: "POST",
                 url: "/polls/remove/poll/"+question_id+"/",
                 data: {question_id,
                        poll_type,
                       csrfmiddlewaretoken:$(event.target).parents('a#remove-poll').attr('csrf'),
                     },
                 dataType: "json",
                 success: function(response) {
                   if(response['status']=='ok'){
                     window.location.reload();
                   }else{
                     alert(response['status']);
                   }
                  },
                  error: function() {
                         alert("error!!");
                  }
            });
        }
    }
  });

  // bind behaviour to a scope
  var bindBehaviourToPanels = function(scope){
    $('a', scope).click(function() {
      var url = $(this).attr('href');
      $('div.ajax-result').load(url)
      });
  }

  // // load user stats with ajax
  // $('div#user-stats').click(function(event){
  //   event.preventDefault()
  //   if($(event.target).is('a.new_polls .panel-footer span, a.new_polls .panel-footer span i')){
  //     window.location.reload('#all-polls-table');
  //   }
  // });
  $('a','div#user-stats').click(function(event){
    event.preventDefault();
    var url = $(this).attr('href');
    $('div.ajax-result').load(url);
  });

  // pagination for ajax loading
  $('div#polls-container').click(function(event){
    if($(event.target).is('ul.pagination li a#nextpage, ul.pagination li a#previouspage')){
      var user = $('div#user-stats').attr('user');
      var page = parseInt($(event.target).attr('page'));
      var url_suffix = $(event.target).attr('view');
      var url = "/polls/" + encodeURIComponent(user) + "/"+encodeURIComponent(url_suffix)+"/"
      event.preventDefault();
      // page += 1;
      $.get(url +'?page=' + page, function(data) {
        if(data == '') {
          // do nothing
        }
        else {
          // block_request = false;
          $('div#polls-container').html(data);
        }
      });
    }
  })

  // submission of settings form with AJAX
  $('input#submit').click(function(event){
    var position = $(this).attr('position');
    var responder = $('div#question-settings-parameters'+position).attr('responder');
    var duration = $('div#question-settings-parameters'+position).attr('duration');
    var quota = $('div#question-settings-parameters'+position).attr('quota');
    var enddate = $('div#question-settings-parameters'+position).attr('enddate');
    var startdate = $('div#question-settings-parameters'+position).attr('startdate');
    var begin = $('div#question-settings-parameters'+position).attr('begin');
    var presentation = $('div#question-settings-parameters'+position).attr('present');

    // check if any of the required field is disabled
    // to avoid invalid form submission
    if($('select#id_who_can_respond').attr('disabled') == 'disabled'){
      $('select#id_who_can_respond > option').attr({'value':responder});
      $('select#id_who_can_respond').removeAttr('disabled');
    }
    if($('select#id_duration').attr('disabled') == 'disabled'){;
      $('select#id_duration > option').attr({'value':duration});
      $('#lock-date'+position).children('input').attr({'value':enddate});
      $('select#id_duration').removeAttr('disabled');
    }
    if($('select#id_presentation').attr('disabled') == 'disabled'){
      $('select#id_presentation > option').attr({'value':presentation});
      $('select#id_presentation').removeAttr('disabled');
    }
    if($('select#id_begin_when').attr('disabled') == 'disabled'){
      $('select#id_begin_when > option').attr({'value':begin});
      $('select#id_begin_when').removeAttr('disabled');
    }

    //var disable = $('select#id_who_can_respond').attr('disabled');

  });

  $('a#follow').click(function(event){
    var user_id = $(this).attr('name');
    event.preventDefault();

    $.ajax({
             type: "POST",
             url: "/polls/friend/follow/",
             data: {user_id,
                    action : $(this).attr('data-action'),
                   csrfmiddlewaretoken:$(this).attr('csrf'),
                 },
             dataType: "json",
             success: function(response) {
               if (response['status'] == 'ok')
                  {
                    var previous_action = $('a#follow').data('action');

                    // toggle data-action
                    $('a#follow').data('action', previous_action == 'follow' ?
                      'unfollow' : 'follow');
                    // toggle link text
                    $('a#follow').text(previous_action == 'follow' ?
                      'unfollow' : 'follow');
                      // update total followers

                  }
              },
              error: function() {
                     alert("error!!");
              }
        });
  });


});

/*
  create sorted table data refer to
  Learning jQuery
  Better Interaction Design and Web Development
  with Simple JavaScript Techniques
  by Jonathan Chaffer

  chapter 11 Manipulating Tables for details

*/

// $(document).ready(function() {
//   $('table.sortable').each(function() {
//     var $table = $(this);
//     $('th', $table).each(function(column) {
//       var findSortKey;
//       if ($(this).is('.sort-alpha')) {
//         findSortKey = function($cell) {
//           return $cell.find('.sort-key').text().toUpperCase()
//                                   + ' ' + $cell.text().toUpperCase();
//         };
//       }
//       else if ($(this).is('.sort-numeric')) {
//         findSortKey = function($cell) {
//           var key = parseInt($cell.text());
//           return isNaN(key) ? 0 : key;
//         };
//       }
//       if (findSortKey) {
//         $(this).addClass('clickable').hover(function() {
//           $(this).addClass('hover');
//         }, function() {
//           $(this).removeClass('hover');
//         }).click(function() {
//           var newDirection = 1;
//           if ($(this).is('.sorting_asc')) {
//             newDirection = -1;
//           }
//           var rows = $table.find('tbody > tr').get();
//           $.each(rows, function(index, row) {
//             row.sortKey =
//                         findSortKey($(row).children('td').eq(column));
//           });
//           rows.sort(function(a, b) {
//             if (a.sortKey < b.sortKey) return -newDirection;
//             if (a.sortKey > b.sortKey) return newDirection;
//             return 0;
//           });
//           $.each(rows, function(index, row) {
//             $table.children('tbody').append(row);
//             row.sortKey = null;
//           });
//           $table.find('th').removeClass('sorting_asc')
//                                          .removeClass('sorting_desc');
//           var $sortHead = $table.find('th').filter(
//                 ':nth-child(' + (column + 1) + ')');
//           if (newDirection == 1) {
//             $sortHead.addClass('sorting_asc');
//           } else {
//             $sortHead.addClass('sorting_desc');
//           }
//           $table.find('td').removeClass('sorting')
//             .filter(':nth-child(' + (column + 1) + ')')
//                                                .addClass('sorting');
//
//           $table.trigger('repaginate');
//         });
//       }
//     });
//   });
//
//   /*
//    Add pagination to the table
//   */
//   $('table.paginated').each(function() {
//     var currentPage = 0;
//     var numPerPage = 10;
//     var $table = $(this);
//     // var repaginate;
//     $table.bind('repaginate', function() {
//       $table.find('tbody tr').show()
//         .slice(0,(currentPage * numPerPage)) // replaced .lt(currentPage * numPerPage)
//           .hide()
//         .end()
//         .slice(((currentPage + 1) * numPerPage - 1)+1) // replaced .gt((currentPage + 1) * numPerPage - 1)
//           .hide()
//         .end();
//     });
//     var numRows = $table.find('tbody tr').length;
//     var numPages = Math.ceil(numRows / numPerPage);
//     var $pager = $('<div class="pagination"></div>');
//     for (var page = 0; page < numPages; page++) {
//       $('<span class="page-number">' + (page + 1) + '</span>')
//         .bind('click', {'newPage': page}, function(event) {
//         currentPage = event.data['newPage'];
//         $table.trigger('repaginate');
//         // marking current page
//         $(this).addClass('active').siblings().removeClass('active');
//
//       })
//         .appendTo($pager).addClass('clickable');
//     }
//     // marking current page
//     $pager.find('span.page-number:first').addClass('active');
//     $pager.insertBefore($table);
//
//     $table.trigger('repaginate');
//   });
//
// });

/*
dataTables activation
*/
// Page-Level Scripts - Tables - Use for reference
$(document).ready(function() {
    $('#all-polls-table').DataTable({
            responsive: true,
            "lengthMenu": [ 5, 10, 25, 50, 75 ]
    });

    // get type of poll
    jQuery.fn.showSelectiveElements = function(){
      var pollType = this.attr('poll-type');
      if(pollType === 'Opinion'){
        $('div.form-group.text-muted').remove()
        $('select#id_view_poll').parent().parent().hide();
        $('select#id_view_result').parent().parent().hide();
        $('select#id_share_poll').parent().parent().hide();
      }else {
        this.find('div.form-group.text-muted').remove()
        var $displayInfo_el = $('select#id_presentation').parent().parent();
        var message = $('<div></div>')
          .addClass('form-group text-muted')
          .html('<h5>Select the category of people that can perform the following actions</h5>')
          .insertAfter($displayInfo_el)
      }
      return this;
    }

});

$(document).ready(function(){
  $.getScript('/static/polls/questionForm.js');

  $('li.create-group a').click(function(event){
    event.preventDefault()
    $('.create-group').toggleClass('hidden');
  })
  $('a.cancel-btn').click(function(){
    $('.create-group').toggleClass('hidden')
  })
})
