import re
from django import forms
from django.core.exceptions import ObjectDoesNotExist
from project.polls.models import Question, Quiz, QuizChoice,\
    QuizQuestion, OpenAnswer
from project.polls.models import Comment, FeedBack, PollSettings
from datetime import datetime, timedelta, date
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

TODAY = date.today()

class QuestionSaveForm(forms.Form):
    political = 'Politics'
    religion ='Religion'
    social = 'Social'
    educational ='Education'
    health = 'Health'
    nutrition = 'Nutrition'
    general = 'General'
    relationship = "Relationships"
    automobile = "Automobile"
    business = "Business"
    sport = "Sports"
    entertainment = 'Entertainment'
    games = 'Games'
    computer = 'Computers'
    CATEGORY_OF_POLLS = (
      (None,'Select a category'),
      (political,'Government & Politics'),
      (religion, 'Religion'),
      (social, 'Social'),
      (educational,'Education'),
      (health, 'Health & Fitness'),
      (nutrition, 'Nutrition/Diets'),
      (general, 'General'),(relationship,"Relationships"),
      (automobile,"Cars & Automobile"),(business,"Business & Money"),
      (sport,"Sports"),(entertainment,"Entertainment"),(games,"Games"),
      (computer,"Computers")
    )
    question = forms.CharField(max_length=160,label= 'Poll Question')
    # import the choice field created in the models.py
    # category_name = forms.ChoiceField(label="category",choices = CATEGORY_OF_POLLS)
    choice1 = forms.CharField(max_length=60,label='option 1')
    choice2 = forms.CharField(max_length=60,label='option 2')
    choice3 = forms.CharField(max_length=60,label='option 3',
                    required=False)
    choice4 = forms.CharField(max_length=60,label='option 4',
                    required=False)
    choice5 = forms.CharField(max_length=60,label='option 5',
                    required=False)
    info = forms.CharField(label='Additional info (optional)',
                    required=False,
                    widget=forms.Textarea)
    instant_poll = forms.BooleanField(label="instant poll", required=False)

    class Media:
        css = {
             'all': ('polls/css/style.css')
        }

    # clean_question removed to allow same question created!

    def clean_question(self):
        question = self.cleaned_data['question']
        if len(question) < 4:
            raise forms.ValidationError(
               "Question is too short and ambigious! Try a more explicit question")
        return question
        # try:
        #     Question.objects.get(question_text=question)
        # except ObjectDoesNotExist:
        #     return question
        # raise forms.ValidationError("Sorry! Poll question already created!!")

    def clean_choice3(self):
        choice3 = self.cleaned_data['choice3']
        if len(choice3) == 0:
            choice3 = None
        return choice3

    def clean_choice4(self):
        choice4 = self.cleaned_data['choice4']
        if len(choice4) == 0:
            choice4 = None
        return choice4

    def clean_choice5(self):
        choice5 = self.cleaned_data['choice5']
        if len(choice5) == 0:
            choice5 = None
        return choice5


class SearchForm(forms.Form):
    query = forms.CharField(
           label='Enter a keyword to search for',
           widget=forms.TextInput(attrs={'size': 32})
    )


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('body',)

class FeedBackForm(forms.ModelForm):
    class Meta:
        model= FeedBack
        fields = ('feedback_text',)

class PollSettingsForm(forms.Form):
    # constants
    default_end_date = timezone.now() + timedelta(days=60)
    WHOTORESPOND = (
        ('registered-users', 'Registered users only'),
        ('anyone','Anyone including anonymous'),
        ('group', 'Group Participants'),
        ('male','Male only'),
        ('female','Female only'),
    )
    DURATION = (
        ('Auto', 'Auto lock'),
        ('quota', 'Lock after votes quota'),
        ('custom','Set custom day/time')
    )
    BEGINS = (
        ('now','now'),
        ('1hr', '1 hour from now'),
        ('5mins','5 mins from now'),
        ('5hr', '5 hours from now'),
        ('next_day','24 hours'),
        ('custom', 'set date')
    )
    PRESENTATION = (
        ('hide_everybody', 'Hide result till closing date'),
        ('show_everyone', 'Show result to everyone'),
        ('voters_only', 'Show result after voting')
    )
    PEOPLE_CATEGORY = (
        ('everyone','Everyone (all registered users)'),
        ('participants','Participants (by group)'),
        ('anonymous','Everyone (include unregistered user)'),
        ('none', 'Nobody')
    )
    who_can_respond = forms.ChoiceField(choices=WHOTORESPOND,initial='anyone')
    duration = forms.ChoiceField(choices=DURATION,initial='Auto')
    votes_quota=forms.IntegerField(min_value=0,localize=False,required=False)
    end_date = forms.DateTimeField(required=False,initial=default_end_date)
    begin_when = forms.ChoiceField(choices=BEGINS,required=False)
    start_date = forms.DateTimeField(required=False,initial=timezone.now)
    presentation = forms.ChoiceField(choices=PRESENTATION,initial='voters_only')
    view_poll = forms.ChoiceField(choices=PEOPLE_CATEGORY,initial='everyone',required=False)
    share_poll = forms.ChoiceField(choices=PEOPLE_CATEGORY,initial='everyone',required=False)
    comment_on_poll = forms.ChoiceField(choices=PEOPLE_CATEGORY,initial='everyone')
    view_result = forms.ChoiceField(choices=PEOPLE_CATEGORY, initial='everyone')

    def clean_votes_quota(self):
        quota = self.cleaned_data['votes_quota']
        if quota is None:
            return quota
        elif quota < 0:
            raise forms.ValidationError("Invalid input! Positive values only")
        return quota


class QuizForm(forms.ModelForm):
    class Meta:
        model = Quiz
        fields = ['title','mode','attempt']
        labels = {
           'title': _("Give your quiz a title"),
           'mode': _('Select mode of the quiz'),
           'attempt': _('Set the number of allowed attempt')
        }

class QuizQuestionForm(forms.ModelForm):
    class Meta:
        model = QuizQuestion
        fields = ['quiz_question_text','question_type']
        labels = {
            'quiz_question_text': _('Enter quiz question'),
            'question_type': _('Type of question'),
        }

class QuizChoiceForm(forms.ModelForm):
    class Meta:
        model = QuizChoice
        fields = ['choice_text','answer']
        labels = {
            'choice_text': _('option'),
            'answer': _('is answer?'),
        }

class OpenAnswerForm(forms.ModelForm):
    class Meta:
        model = OpenAnswer
        fields = ['answer']
        labels = {
            'answer': _('Provide an answer'),
        }


class SendMailForm(forms.Form):
    subject = forms.CharField(label="Email's subject")

class OneMessageForm(forms.Form):
    subject = forms.CharField(label="Email's subject")
    name = forms.CharField(label="Recipient's name", required=False)
    email = forms.EmailField(label="Recipient's Email")
