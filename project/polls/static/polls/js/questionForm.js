$(document).ready(function() {
  $('.hide-choices').hide();
  $('div.show-button').click(function() {
    $('.hide-choices').show();
    $(this).hide();
  });
  // hide end and start date fields form to wait selection before showing
  $('#id_start_date').hide();
  $('#id_start_date').parents('.fieldwrapper').prev('label').hide();
  $('#id_end_date').parents('.fieldwrapper').prev('label').hide();
  $('#id_end_date').hide();
  $('#id_votes_quota').hide();
  $('#id_votes_quota').parents('.fieldwrapper').prev('label').hide();
  //$('#eligibility').hide();
  $('#check-eligible input').click(function(event){
    $('#eligibility').toggleClass('hidden');
    });

  // get type of poll
  var pollType = $('form.poll-settings').attr('poll-type');
  function showSelectiveElements (pollType){
    if(pollType === 'Opinion'){
      $('select#id_view_poll').parent().parent().hide();
      $('select#id_view_result').parent().parent().hide();
      $('select#id_share_poll').parent().parent().hide();
    }else {
      var $displayInfo_el = $('select#id_presentation').parent().parent();
      var message = $('<div></div>')
        .addClass('form-group text-muted')
        .html('<h5>Select the category of people that can perform the following actions</h5>')
        .insertAfter($displayInfo_el)
    }
  }
  showSelectiveElements(pollType)


  $('.form-container').click(function(event){
    // execute for chrome (startDate)
    if($(event.target).is('select#id_begin_when') || $(event.target).is('select#id_begin_when > option')){
      var selectedBeginValue = document.getElementById('id_begin_when').value
      if(selectedBeginValue == 'custom'){
          $('#id_start_date').parents('.fieldwrapper').prev('label').show();
          $('#id_start_date').show();
          $('input#id_start_date').datetimepicker({
            format:'m/d/yyyy hh:ii',
            todayHighlight: true,
            startDate: '-0d',
            autoclose: true,
          });
          // set startdate
          // $('input#id_start_date').datetimepicker('setStartDate', $(this).val());
          $('#id_start_date').attr({'required':'required'})
          .addClass('required');
        }else {
          $('#id_start_date').hide();
          $('#id_start_date').parents('.fieldwrapper').prev('label').hide();
          $('#id_start_date').removeAttr('required')
            .removeClass('required');
        }
    }

    // execute for chrome browser (endDate field)
    if($(event.target).is('select#id_duration') || $(event.target).is('select#id_duration > option')){
      // remove previous error messages
      var $labelItem = $(event.target).parent();
      $labelItem.removeClass('has-error')
        .find('span.error-message').remove()
      $labelItem.prev('label').removeClass('warning');
      var selectedDurationValue = document.getElementById('id_duration').value
      // get type of poll
      var pollType = $('form.poll-settings').attr('poll-type');
      if(selectedDurationValue == 'custom'){
        $('#id_end_date').parents('.fieldwrapper').prev('label').show();
        $('#id_end_date').show();
        // set quota not required and hide()
        $('#id_votes_quota').removeAttr('required');
        $('#id_votes_quota').hide();
        $('#id_votes_quota').parents('.fieldwrapper').prev('label').hide();
        // set picker format
        $('#id_end_date').datetimepicker({
          format:'m/d/yyyy hh:ii',
          todayHighlight: true,
          startDate: '-0d',
          endDate: '+90d',
          autoclose: true
        });

      }
      else if(selectedDurationValue == 'quota'){
        // remove required for end-date field then set quota to be required
        $('#id_end_date').parents('.fieldwrapper').prev('label').hide();
        $('#id_end_date').hide();
        if(pollType == 'Quiz'){
          // $labelItem = $(event.target).parent();
          var errorMessage = "This option is not applicable to quizzes. Select other options "
          $('<span></span>')
              .addClass('error-message')
              .text(errorMessage)
              .prependTo($labelItem);
            $labelItem.addClass('has-error');
            $labelItem.prev('label').addClass('warning');
          return false;
        }
        $('#id_votes_quota').attr({'required':'required'});
        // prevent user from entering any char except digits
        $('#id_votes_quota').keypress(function(event) {
          if (event.charCode && (event.charCode < 48 || event.charCode > 57))
          {
            event.preventDefault();
          }
        });
        $('#id_votes_quota').show();
        $('#id_votes_quota').parents('.fieldwrapper').prev('label').show();
      }
      else if(selectedDurationValue == 'Auto'){
        $('#id_votes_quota').hide();
        $('#id_votes_quota').parents('.fieldwrapper').prev('label').hide();
        $('#id_end_date').hide();
        $('#id_end_date').parents('.fieldwrapper').prev('label').hide();
        // remove required for quota
        $('#id_votes_quota').removeAttr('required');
      }

    }

  })

  /*
  style the poll question form with bootstrap through JavaScript
  */
  // hide the form on first loading of page
  $('div.quiz-container').addClass('hidden');
  $('form').parents('div.multiple-choice-container').addClass('hidden');
  // select all label tag that is direct child of form-group and add class 'control-label'
  $('form > .form-group .fieldwrapper').addClass('col-sm-8');
  $('form > .form-group label').addClass('col-sm-4 control-label');
  // select all input tag and add a class of form-control
  $('form > .form-group input').addClass('form-control');

  // optional class to choices that are optional and hide them
  // while add required class to choices that are required
  $('form :input[id="id_question"]').addClass('required');
  $('form :input[name="choice1"]').addClass('required');
  $('form :input[name="choice2"]').addClass('required');
  $('form :input[name="group_name"]').addClass('required');
  $('form label[for="id_group_name"]').addClass('required');

  $('form :input[name="choice3"]').addClass('optional');
  $('form label[for="id_choice3"]').addClass('optional');
  $('form :input[name="choice4"]').addClass('optional');
  $('form label[for="id_choice4"]').addClass('optional');
  $('form :input[name="choice5"]').addClass('optional');
  $('form label[for="id_choice5"]').addClass('optional');
  // var $choiceInput = $('form :input').filter('.optional');

  // iterate over the optional inputs and show only input with text
  $('form input.optional').each(function(){
    var $thisInput = $(this);
    var thisvalue = $thisInput.val();
    if(thisvalue == "" ){
        $thisInput.hide();
        $thisInput.parents('.fieldwrapper').prev('label').hide();
      }
  })

  // add a plus sign button for addition of choices
  $('<div></div>').addClass('plus-button').append(
    '<a href="">+ add options</a>' + '<br />')
  .insertAfter('form :input[name="choice2"]');
  // add counter attribute to plus-button
  $('.plus-button').attr({'counter':2});

  // don;t apply styling to instant poll checked input and label
  $('form > .form-group label[for="id_instant_poll"]').addClass('hidden');
  $('form > .form-group input[name="instant_poll"]').addClass('hidden');
  // select textarea tag and add a class of form-control
  $('form > .form-group textarea').addClass('form-control');
  $('form > .form-group textarea').attr({'rows':5});
  // select input and label tags for settings divs
  $('.settings-for-poll').addClass('');
  $('.settings-for-poll > .form-group label').addClass('col-sm-3 control-label');
  $('.settings-for-poll > .form-group input').addClass('form-control');
  $('.settings-for-poll > .form-group .fieldwrapper').addClass('col-sm-4');
  $('.settings-for-poll > .form-group select').addClass('form-control');
  $(":input[type=submit]").removeClass('form-control')
  $('#submit-button').addClass('col-sm-5 col-sm-offset-2')
    $("#submit-edited-question").addClass('form-control')
      .addClass('col-sm-5 col-sm-offset-2')

  // show input option when plus-button is clicked
  $('div.plus-button').click(function(event){
    event.preventDefault();
    var counter = parseInt($(this).attr('counter'));
    counter += 1;
    $(this).attr({'counter':counter})
    var $label3 = $('form label[for="id_choice3"]');
    var $label4 = $('form label[for="id_choice4"]');
    var $label5 = $('form label[for="id_choice5"]');
    var $input3 = $('form :input[name="choice3"]');
    var $input4 = $('form :input[name="choice4"]');
    var $input5 = $('form :input[name="choice5"]');
    // display corresponding label and input
    if(counter == 3){
      $label3.show();
      $input3.show();
    }else if (counter == 4) {
      $label4.show();
      $input4.show();
    }else if (counter == 5) {
      $label5.show();
      $input5.show();
      // hide button and avoid further clicking;
      $(this).hide();
    }

  })

  $('div.instant-poll').click(function(event){
    // add the active state color class and remove color
    // from the other button for each type of poll
    // and display the form
    $('div.quiz-container').addClass('hidden');
    $('form').parents('div.multiple-choice-container').removeClass('hidden');
    $(this).addClass('panel-success');
    $(this).removeClass('panel-default');
    $('div.custom-poll').addClass('panel-default').removeClass('panel-success');
    $('div.quiz-poll').addClass('panel-default').removeClass('panel-success');
    $('div.survey-poll').addClass('panel-default').removeClass('panel-success');
    $('.settings-for-poll').hide();

    // checked the hidden input
    $('form > .form-group input[name="instant_poll"]').attr({'checked':'checked'});
  });

  $('div.custom-poll').click(function(event){
    // add the active state color class and remove color
    // from the other button for each type of poll
    // and display the form
    $('div.quiz-container').addClass('hidden');
    $('form').parents('div.multiple-choice-container').removeClass('hidden');
    $(this).addClass('panel-success');
    $(this).removeClass('panel-default');
    $('div.instant-poll').addClass('panel-default').removeClass('panel-success');
    $('div.quiz-poll').addClass('panel-default').removeClass('panel-success');
    $('div.survey-poll').addClass('panel-default').removeClass('panel-success');
    $('.settings-for-poll').show();

    // remove the checked the hidden input
    $("form input[name='instant_poll']").removeAttr('checked');
  });

  $('div.quiz-poll').click(function(event){
    // add the active state color class and remove color
    // from the other button for each type of poll
    // and hide the Multiple choice form
    $('div.quiz-container').removeClass('hidden');
    $('form').parents('div.multiple-choice-container').addClass('hidden');
    $(this).addClass('panel-success');
    $(this).removeClass('panel-default');
    $('div.custom-poll').addClass('panel-default').removeClass('panel-success');
    $('div.instant-poll').addClass('panel-default').removeClass('panel-success');
    $('div.survey-poll').addClass('panel-default').removeClass('panel-success');

    // checked the hidden input
    $('form > .form-group input[name="instant_poll"]').removeAttr('checked');
    $(':input[name=newquiz-title]').addClass('form-control');

  });

  $('div.survey-poll').click(function(event){
    // add the active state color class and remove color
    // from the other button for each type of poll
    // and hide the Multiple choice form
    $('div.quiz-container').addClass('hidden');
    $('form').parents('div.multiple-choice-container').addClass('hidden');
    $(this).addClass('panel-success');
    $(this).removeClass('panel-default');
    $('div.custom-poll').addClass('panel-default').removeClass('panel-success');
    $('div.quiz-poll').addClass('panel-default').removeClass('panel-success');
    $('div.instant-poll').addClass('panel-default').removeClass('panel-success');

    // checked the hidden input
    $('form > .form-group input[name="instant_poll"]').removeAttr('checked');
  });

  // validate the text input to contain at least one alpha characters

  $('form :input[type="text"]').blur(function(){
     $(this).parents('.fieldwrapper').removeClass('has-error')
     .find('span.error-message').remove();
     $(this).parents('.fieldwrapper').prev('label').removeClass('warning');

     // check if field is optional
     if($(this).is('.optional')){
       // !/^[0-9]$/.test(this.value) check for value that doesn't start with 0-9
       // this line is required to allow valid input of one digit number e.g 4
       if((this.value != "") && (!/^[0-9]$/.test(this.value))){
         // check the regular expression to catch input that doesnt start with "a-Z or 0-9"
         // followed by one or more characters
         if(!/^[a-zA-Z0-9](.+)$/.test(this.value)){
           var $labelItem = $(this).parents('.fieldwrapper');
           var errorMessage = " enter a valid character"
           $('<span></span>')
               .addClass('error-message')
               .text(errorMessage)
               .prependTo($labelItem);
             $labelItem.addClass('has-error');
             $labelItem.prev('label').addClass('warning');
         };
       };
     }
     // perform this if field is required
     if($(this).is('.required')){
       // !/^[0-9]$/.test(this.value) check for value that doesn't start with 0-9
       // this line is required to allow valid input of one digit number e.g 4
       if((this.value != "") && (!/^[0-9]$/.test(this.value))){
         // check the regular expression to catch input that doesnt start with "a-Z or 0-9"
         // followed by one or more characters
         if(!/^[a-zA-Z0-9](.+)$/.test(this.value)){
           var $labelItem = $(this).parents('.fieldwrapper');
           var errorMessage = "\nEnter a valid character"
           $('<span></span>')
               .addClass('error-message')
               .text(errorMessage)
               .prependTo($labelItem);
               $labelItem.addClass('has-error');
               $labelItem.prev('label').addClass('warning');
         };
       };
     };
     if ($(this).is('.email')) {
      var $labelItem = $(this).parents('.fieldwrapper');
      if (this.value != '' && !/.+@.+\.[a-zA-Z]{2,4}$/
                                               .test(this.value)) {
        var errorMessage = 'Please use proper e-mail format'
                                           + '(e.g.me@example.com)';
        $('<span></span>')
          .addClass('error-message')
          .text(errorMessage)
          .prependTo($labelItem);
        $labelItem.addClass('has-error');
        $labelItem.prev('label').addClass('warning');
      };
    };

  })

  // validate on submit of form
  $('form').submit(function(){
    $('#submit-form').parent('div.form-group').removeClass('has-error')
      .find('div.warning').remove();
    // event.preventDefault
    $('#submit-message').remove();
    // trigger the blur on inputs
    $('form :input[type="text"]').trigger('blur');
    // count number of warning classes
    var numWarnings = $('.warning', this).length;
    // Get select tag with error messages
    if(numWarnings){
      // list names of fields that contain errors
      var fieldList = [];
      $('label.warning').each(function() {
        fieldList.push($(this).text());
      });
      var $submitDiv = $('#submit-form').parent('div.form-group');
      $('<div></div>')
        .addClass('col-sm-4 col-sm-offset-4 warning error-message')
        .append('please correct the errors with the following ' + numWarnings
                                                         + ' fields: <br/>')
        .append('&bull; ' + fieldList.join('<br />&bull; '))
        .prependTo($submitDiv);
      $submitDiv.addClass('has-error');
      Lobibox.notify('warning', {
        sound: false,
        delay: 4000,
        position: 'center top', //or 'center bottom'
        msg: 'Failed!, try again!!'
        });
      return false; // prevent the form from being submitted
    }

    if($(this).is('form#create-group')){
      var $result_el = $('#groups-created').prev() // element to display success
      $(this).ajaxSubmit({
        clearForm: true,
        success: function(response){
          if(response['status']=='ko'){
            // displaySuccessMessage($result_el,'label label-warning','Group NOT created!!'); // display success message
            Lobibox.notify('error', {
              size: 'mini',
              sound: false,
              rounded: true,
              delay: 4000,
              position: 'center top', //or 'center bottom'
              msg: response['message']
              });
          }
          else{
            $('#groups-created').empty();
            $('#groups-created').append(response)
            // displaySuccessMessage($result_el,'label label-success','New Group created successfully'); // display success message
            Lobibox.notify('success', {
              size: 'mini',
              sound: false,
              rounded: true,
              delay: 5000,
              position: 'top right', //or 'center bottom'
              msg: 'New Group created successfully'
              });
          }
        }
      });
      return false;
    }
    if($(this).is('form#add-new-member')){
      var message;
      var messagetype;
      var status;
      var display_el = $(this).prev();
      $(this).ajaxSubmit({
        clearForm:true,
        success: function(response){
          message = response['message']
          status = response['status']
          // determine the text color
          if(status=='ok'){
            messagetype = "success";
          }else{
            messagetype = 'error';
          }
          // displaySuccessMessage(display_el,messagetype,message)
        },
        complete: function(){
          if(status=='ok'){
            var $membersCount = display_el.siblings('h4').children().find('span.label');
            var previousCount = parseInt($membersCount.text())
            $membersCount.text(previousCount + 1);
          }
          Lobibox.notify(messagetype, {
            size: 'mini',
            sound: false,
            delay: 5000,
            position: 'top right', //or 'center bottom'
            msg: message
            });
        }
      });
      $(this).parents('.list-group-item').find('a.add-new-member')
        .removeClass("hidden")
      $(this).remove();
      return false
    }

  })

})
function displaySuccessMessage(result_el,messageColor,message){
  // display a success message and disappear in 5seconds
  $('div.message-color-disappear').remove();
  var message = $('<div class="message-color-disappear"></div>')
                    .addClass(messageColor)
                    .append(message)
                    .insertAfter(result_el)
                    .fadeIn('fast').delay(5000).fadeOut('slow')
  return message
}
