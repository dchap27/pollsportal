$(document).ready(function() {
  const webSocketBridge = new channels.WebSocketBridge();
  webSocketBridge.connect('/ws/updates/');

  // get list of voters
  $('.voted', ".card-footer").click(function(event){
    event.preventDefault()
    var url = $(this).attr('url');
    var question = $(this).attr('positionCounter');
    var title = $(this).data('text');
    $.ajax({
      type:'get',
      url: url,
      data: {'question': question},
      dataType: 'json',
      success: function(data){
        if(data['status']=='ko'){
          Lobibox.notify('error', {
            size: 'mini',
            rounded: true,
            delay: 4000,
            position: 'center top', //or 'center bottom'
            msg: data['msg']
            });
          return false;
        }
        var $container = $('#shareModal .modal-body');
        var $html = $('<div></div>');
        $html.append('<h4>Voters</h4>')
        $.each(data,function(entryIndex,entry){
            var lst = '<h5><a href="polls/user/'+entry['name']+'/">@'+entry['name']+'</a></h5>'
            $html.append(lst);
        })
        $('#shareModal .modal-header .modal-title').html(title);
       $container.html($html);
     },
     complete: function(){
       $('#shareModal').modal({
         keyboard: true,
         backdrop: true, // allow closing when clicked outside
       });
       $('#shareModal').on('show.bs.modal',function(){
         //
       })
     }
    })
  })

  $('.view-comments',".card-footer").click(function(event){
    event.preventDefault()
    var previous_action = $(this).data('action');
    var url = $(this).attr('href');
    var pos = $(this).attr('position')
    var new_url = '/polls/question/'+pos;
    // toggle data-action
    $(this).data('action', previous_action == 'view' ? 'unview' : 'view');

    if(previous_action == 'view'){
      $(this).parent().siblings('.card-title').children('.feed-post').load(url);
      $(this).children('span#comment-text').empty();
      $(this).children('span#comment-text').text('Back');
    }else{
      $(this).parent().siblings('.card-title').children('.feed-post').load(new_url);
      var $icon = $('<i class="fa fa-comment-o"></i>');
      $(this).children('span#comment-text').html($icon);
    }


  })
  // bind behaviour to a scope
    $('#mylike', ".card-footer").click(function(event) {
      event.preventDefault();
      var question_id = $(this).attr('name');
      var previous_action = $(this).data('action');
      var $count_el = $('span.count'+question_id);
      var valueOfClass = 'count'+question_id;
      var $icon_el = $count_el.prev();
      var pollType = $(this).attr('poll_type');
      $.ajax({
               type: "POST",
               url: "/polls/like/"+question_id+"/",
               data: {question_id,
                      poll_type:pollType,
                      action : $(this).attr('data-action'),
                     csrfmiddlewaretoken:$(this).attr('csrf'),
                   },
               dataType: "json",
               complete: function(response) {
                 if (response['status'] == '200')
                    {
                      // toggle data-action
                      $('a.mylike'+question_id).data('action',
                        previous_action == 'like' ? 'unlike' : 'like');
                      // update total likes
                      var previous_likes = parseInt($('span.count'+question_id).text());
                      $('span.count'+question_id).text(previous_action == 'like' ? previous_likes + 1 : previous_likes - 1);
                      // toggle icon and color
                      $icon_el.attr('class',
                        previous_action == 'like' ? 'like-icon glyphicon glyphicon-heart' : 'glyphicon glyphicon-heart-empty')
                      $count_el.attr('class',
                        previous_action == 'like' ? 'like-count '+valueOfClass : valueOfClass )

                      // send a message to stream
                      webSocketBridge.stream('messagenotify').send({'activity': 'like'});
                    }
                },
                error: function() {
                  Lobibox.notify('error', {
                    size: 'mini',
                    rounded: true,
                    delay: 3000,
                    position: 'center top', //or 'center bottom'
                    msg: 'Failed!, try again later'
                    });
                }
          });
      });

      $('.share-count','.card-footer').click(function(event){
        var url = $(this).attr('href');
        var poll_type = $(this).attr('poll_type');
        event.preventDefault();
        // load share content into share modal
        $.ajax({
          type: "GET",
          url: url,
          data:{
          },
          success:function(data){
            $('#shareModal .modal-body').html(data);
            var $title = $('.modal-body .shared-content').children('div.content-title');
            $('#shareModal .modal-header .modal-title').html($title);
          },
          error: function(){
            // something happened
            Lobibox.notify('error', {
              size: 'mini',
              rounded: true,
              delay: true,
              position: 'center top', //or 'center bottom'
              msg: 'Failed!, try again later'
              });
          },
          complete: function(){
            $('#shareModal').modal({
              keyboard: true,
              backdrop: true, // allow closing when clicked outside
            });
            $('#shareModal').on('show.bs.modal',function(){
              //
            })
          }

        });

      });

    // get create poll page and insert into .main-poll-div
    $('#side-menu .create-poll-btn').click(function(){
      var username = $('#id_user a').text();
      $('.main-poll-div').load("/polls/" + encodeURIComponent(username) + "/save/");
      return false;
    });

    $("#index-search span.glyphicon").click(function (event) {
      var query = $("#id_query").val();
      if(query !== ''){
          $('.search-result').load(
            "polls/search/?ajax&query=" + encodeURIComponent(query));
            return false;
      }
    });
    // pressing keys or submit for mobile search
    $('#index-search').keypress(function(event){
      var query = $("#id_query").val();
      // keyCode 13 is user pressed 'enter key'
      if(event.keyCode == 13 && query !== ''){
        $('.search-result').load(
          "polls/search/?ajax&query=" + encodeURIComponent(query));
          return false;
      }else if (event.keyCode == 13 && query === '') {
        // do nothing since query is empty
        event.preventDefault();
      }
    })

    // pressing keys or submit for mobile search
    $('#index-search2').keypress(function(event){
      var query = $("#id_query2").val();
      // keyCode 13 is user pressed 'enter key'
      if(event.keyCode == 13 && query !== ''){
        $('.search-result').load(
          "polls/search/?ajax&query=" + encodeURIComponent(query));
          return false;
      }else if (event.keyCode == 13 && query === '') {
        // do nothing since query is empty
        event.preventDefault();
      }
    })

    $('#main-feeds').click(function(event){
      // load result into same div
      if($(event.target).is('.result-link')){
        var url = $(event.target).attr('href');
        event.preventDefault();
        $($(event.target).parents('.panel-body')).load(url);
      }

      // submit comments in same question_feed div
      if($(event.target).is("form #submitcomment")){
        var question_id = $(event.target).attr('questionid');
        var url = $(event.target).parents('form').attr('action');
        event.preventDefault();
        var $targetLocation = $(event.target);
        var $body = $(event.target).parents('form').children().children() // textarea element
        var $copiedElement = $('#ajax-loading-image img').clone();
        var $cardfooter_el = $(event.target).parents('form').parent().parent();
        var $count_el = $cardfooter_el.parent().siblings('.card-footer')
        // setup ajax loader
        $.ajax({
              global: false,
              type: "POST",
              url: url,
              data: {question_id,
                    'body':$body.val(),
                    csrfmiddlewaretoken:$targetLocation.parent().prev().val(),
                  },
              beforeSend: function () {
                 $copiedElement.insertAfter($targetLocation);
              },
              complete: function () {
                  $copiedElement.remove();
              },
              // dataType: "json",
              success: function(response) {
                // load the comment into the box
                $(event.target).parents('form').parent().parent().load(response['data'])
                     //window.location.reload();
                // update comment count
                var previous_count = parseInt($count_el.children('a.view-comments').text());
                var counter = $count_el.children('a.view-comments').children('.comment-count');
                counter.text((previous_count+1));
                // send a message to stream
                webSocketBridge.stream('messagenotify').send({'activity': 'comment'});
               },
               error: function() {
                 // something happened
                 Lobibox.notify('error', {
                   delay: 5000,
                   position: 'top right', //or 'center bottom'
                   msg: 'Failed!, try again later'
                   });
               }
        });
      }

      if($(event.target).is('div.vote-btn')){
        // code for voting asynchronously
        $('div.vote-error-message').remove();
        var question_id = $(event.target).attr('questionId');
        var loopCounter = $(event.target).attr('loopnum');
        var choice = $(event.target).attr('data-value');
        var $form = $(event.target).parents('form');
        var $copiedElement = $('#ajax-loading-image img').clone();
        var $targetLocation;

        // var choice = $(event.target).children('#choice'+loopCounter).val();
        $.ajax({
                 type: "POST",
                 url: "/polls/vote/"+question_id+"/",
                 data: {question_id,
                       choice,
                       csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
                     },
                 beforeSend: function () {
                    var vote_class_check = $('span#vote-count'+question_id).parent().attr('class');
                    if(vote_class_check == 'voted'){

                        // something happened
                        Lobibox.notify('error', {
                          size: 'mini',
                          sound: false,
                          delay: 5000,
                          position: 'top right', //or 'center bottom'
                          msg: 'Failed!, you can vote only ONCE'
                          });
                      return false
                    }
                    $(event.target).children('label').prepend($copiedElement)
                 },
                 complete: function () {
                     $(event.target).children('label').children('img').remove();
                     $('span#vote-count'+question_id).parent().addClass('voted')
                 },
                 success: function(data) {
                        if(data['status']=='ok'){
                          $($(event.target).parents('.feed-post')).load(data['success_url']);
                          var total_votes = data['total_votes'];
                          // $('span#vote-count'+question_id).text(total_votes);
                          // send a message to stream
                          webSocketBridge.stream('messagenotify').send({'activity': 'vote'});
                        }else {
                          // $('<div></div>').addClass('warning vote-error-message')
                          //   .html('<h5>'+data['message']+'</h5>')
                          //   .insertBefore($form).fadeIn('fast').delay(5000).fadeOut('slow');
                            Lobibox.notify('warning', {
                              size: 'mini',
                              sound: false,
                              rounded: true,
                              delay: 3000,
                              position: 'center top', //or 'center bottom'
                              msg: data['message']
                              });
                        }
                        // window.location.reload();
                  },
                  error: function() {
                    // $('<div></div>').addClass('warning vote-error-message')
                    //   .html('<h5>Something went wrong. Try again later!</h5>')
                    //   .insertBefore($form);
                      // something happened
                      Lobibox.notify('error', {
                        sound: false,
                        rounded: true,
                        delay: 5000,
                        position: 'center top', //or 'center bottom'
                        msg: 'Something went wrong!, try again later'
                        });
                  }
            });
      }


    });

    $('#action-list').click(function(event){
      if($(event.target).is('a.action-performer')){
        event.preventDefault();
        var username = $(event.target).attr('user_name');
        var position = $(event.target).attr('position');
        if (username != 'You'){
          $($(event.target).parents('.feed-post'))
            .load("/polls/user/" + encodeURIComponent(username));
        }
      }
      if($(event.target).is('.feed-post .card-title a#follow')){
          var user_id = $($(event.target)).attr('name');
          var $count_el = $(event.target).parent().siblings('.card-block')
              .children('.row');
          var $counter;
          event.preventDefault();
          $.ajax({
                   type: "POST",
                   url: "/polls/friend/follow/",
                   data: {user_id,
                          action : $($(event.target)).attr('data-action'),
                         csrfmiddlewaretoken:$($(event.target)).attr('csrf'),
                       },
                   dataType: "json",
                   success: function(response) {
                     if (response['status'] == 'ok')
                        {
                          var previous_action = $('a#follow').data('action');

                          // toggle data-action
                          $('a#follow').data('action', previous_action == 'follow' ?
                            'unfollow' : 'follow');
                          // toggle link text
                          $('a#follow').text(previous_action == 'follow' ?
                            'unfollow' : 'follow');
                            // update total followers
                          var new_action = (previous_action == 'follow' ?
                            'follower' : 'following');
                          var targetclass = '.follower-count';
                          $counter = $count_el.children(targetclass).children('a')
                          var previous_count = parseInt($counter.text());
                          $counter.text(
                            previous_action == 'follow' ? previous_count + 1 : previous_count - 1
                          )
                          return
                        }
                    },
                    error: function() {
                           // something happened
                           Lobibox.notify('error', {
                             sound: false,
                             rounded: true,
                             delay: 5000,
                             position: 'top right', //or 'center bottom'
                             msg: 'Something went wrong!, try again later'
                             });
                    }
              });
      }
    });


    $('.question-feed #pending-btn').click(function(event){
      alert("Poll is pending! Will be available soon!!");
    });

    var page = 1;
    var empty_page = false;
    var block_request = false;
    $('button#load-feeds').removeAttr('disabled')

    // $('button#load-feeds').click(function() {
    $(window).scroll(function(){
      var margin = $(document).height() - $(window).height() - 200;
      if ($(window).scrollTop() > margin && empty_page == false &&
    block_request == false) {
      block_request = true;
      page += 1;
      $.get('?page=' + page, function(data) {
        if(data == '') {
          $('button#load-feeds').removeClass('btn-primary').attr({'disabled':'disabled'}).text('No more feeds');
          empty_page = true;
        }
        else {
          block_request = false;
          $('#main-feeds').append(data);
          $('.share-count').each(function(){
            var $share = $(this);
            $share.getNumberOfShares();
          });
        }
      });
    }
  });

/*
WebSockets bindings messages
*/
  webSocketBridge.listen(function(data, stream){
    if(data.stream_name == "messagenotify"){
      // update the counts of notification
      if(data.feedcount == 0){
        $('span.new-messages.badge').empty();
      }
      else{
        $('span.new-messages.badge').text(data.feedcount);
        // pull the notification messages into the Navbar if not empty()
        $.get('/polls/feed/notifications/', function(data){
          if(data !== ""){
            // $('li.no-messages').remove();
            $('#notification-list').html(data);
          }
        });
      }
    }
  });

  // demultiplex each stream
  webSocketBridge.demultiplex('questionupdates', function(payload, stream) {
    if(payload.action == "update"){
      var $container = $('*').is('div.card-footer.feed-footer');
      // update total likes and votes
      if($container){
        $('span.count'+payload.pk).text(payload.data.total_likes);
        $('span#vote-count'+payload.pk).text(payload.data.total_votes);
      }
    }
  });


});


$(document).ready(function(){
  // create a plug-in for internal use
  jQuery.fn.getNumberOfShares = function(){
    var question_id = this.attr('questionid');
    var poll_type = this.attr('poll_type');
    var $result = this.children('span.no-of-shares');
      // performe ajax asynchronous task to get total_shares
    $.ajax({
      type: 'GET',
      url: "/polls/share/ranking/?question_id="+question_id,
      data: {
        poll_type,
      },
      dataType: 'json',
      success: function(response){
        var total_shares = response['total_shares'];
        $result.text(total_shares);
      },
      error: function(){
        //
      }
    });
    return this;
  }
  // iterate over all the share-count class
    $('.share-count').each(function(){
      var $share = $(this);
      $share.getNumberOfShares();
    });
})
