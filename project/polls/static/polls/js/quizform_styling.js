$(document).ready(function(){
  /*
  style the edit form
  */

  $('form').addClass('form-horizontal');

  $('fieldset').each(function() {
    // store each legend text
    var heading = $('legend', this).remove().text();
    // change it with h3 tag
    $('<h4></h4>')
    .text(heading)
    .prependTo( this );
  });

  // style choices section
  $('.section .item').each(function(){
    $(this).wrap(
      '<fieldset></fieldset>'
    )
    $('<h5 class="page-header">Add an option</h5>').insertBefore(this);

    $(this).parent('fieldset').addClass('col-sm-12');
    $(this).children('p').addClass('form-group')
      .children('label').addClass('control-label col-sm-5');
    $(this).children('p').children(':input:not(:checkbox)').addClass('form-control');
    $(this).children('p').children(':input').each(function(){
      $(this).wrap(
        '<div class="col-sm-7"></div>'
      );
    });
  })

  // style quix text section
  $('form .quiz-title-edit label').addClass('control-label col-sm-4');
  $('form .quiz-title-edit p').addClass('form-group');

  $('form .quiz-title-edit :input').each(function(){
    $(this)
    .addClass('form-control')
      .wrap(
      '<div class="col-sm-8"></div>'
    );
  });

  // style question text section
  $('form .section > p').each(function(){
    var $p = $(this);
    $p.addClass('form-group')
      .children('label').addClass('control-label col-sm-4')
    $p.children(':input:not(:checkbox)').addClass('form-control');
    $p.children(':input').each(function(){
      $(this).wrap(
        '<div class="col-sm-8"></div>'
      );
    });
  });
})
