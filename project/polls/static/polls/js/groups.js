$(document).ready(function(){

  function displaySuccessMessage(result_el,messageColor,message){
    // display a success message and disappear in 5seconds
    $('div.message-color-disappear').remove();
    var message = $('<div class="message-color-disappear"></div>')
                      .addClass(messageColor)
                      .append(message)
                      .insertAfter(result_el)
                      .fadeIn('fast').delay(5000).fadeOut('slow')
    return message
  }

  // Add a new participant to group
  $('.list-group').click(function(event){

    if($(event.target).is('a.add-new-member')){
      event.preventDefault()

      var group_name = $(event.target).attr('group_name');
      var csrf = $(event.target).attr('csrf');
      // create a form element
      var group = '<input type="hidden" name="group_name" id="id_group_name" value="'+ group_name +'\">'
      var token = '<input type="hidden" name="csrfmiddlewaretoken" value="'+ csrf +'\">'
      var html = '<form id="add-new-member" method="post" action="/account/my-group-list/">' + group
      $.ajax({
        type: "get",
        url: $(event.target).attr('href'),
        dataType: "json",
        success: function(data){
          var $select_el = $('<select id="names-list" multiple="multiple" name="add_new_member"></select>')
          $.each(data, function(entryIndex,entry){
            // iterate over each friends names and add to select options
            var opt = '<option value='+ '\''+ entry['id'] +'\'>';
            opt += entry['name'] + '</option>';
            $select_el.append(opt);
          })
          html += '<div class="form-group">'
          html += token
          html += '<div class="fieldwrapp">'
          html += '</div>'
          html += '</div>'
          html += '<input type="submit" name="submit" value="add"> <span class="clickable cancel-add-member">cancel</span>'
          html += '</form>'

          // $(event.target).parent().append(html)
          $html = $(html);
          $html.insertAfter($(event.target))
          $('#add-new-member .fieldwrapp').append($select_el);
          // load the form script
          $.getScript('/static/polls/js/questionForm.js');
        },
      })
      $(event.target).toggleClass('hidden')

    }

    if($(event.target).is('.cancel-add-member')){
      $(event.target).parents('.list-group-item').find('a.add-new-member')
        .removeClass("hidden")
      $(event.target).parents('.list-group-item').find('form')
        .remove()
    }

    // get list of members in a group
    if($(event.target).is('a.get-members')){
      event.preventDefault()
      $thisEl = $(event.target)
      var group_name = $(event.target).attr('group_name');
      var id = $(event.target).attr('id');
      $pos_el = $(event.target).parent().parent();
      var $membersCount = $(event.target).next().text();
      $membersCount = parseInt($membersCount);
      if($membersCount < 1){
        displaySuccessMessage($pos_el,'label label-info',"You haven't added any member")
        return false;
      }
      $.ajax({
        type: 'get',
        url: $(event.target).attr('href'),
        data: {group_name,
        },
        success: function(data){
          if(data['status']=='ok'){
            $list = $('<ul></ul>');
            $list.attr({'id':id});
            $.each(data['member'], function(memberIndex,member){
              // iterate over each member
              var lst = '<li>'+ member['name'] +'</li>';
              $list.append(lst);
            })
            $list.insertAfter($pos_el)
            $thisEl.addClass('hide-members').removeClass('get-members').text(' Hide ')
          }
        }
      })
    }

    // toggle members list
    if($(event.target).is('a.hide-members')){
      event.preventDefault();
      $thisEl = $(event.target);
      $thisEl.addClass('get-members').removeClass('hide-members').text(' members ');
      var id = $thisEl.attr('id');
      $thisEl.parents('.list-group-item').find('ul#'+id).remove();

    }
    // remove membership from group
    if($(event.target).is('.remove-member')){
      var group = $(event.target).attr('group_name');
      var action = confirm("Delete your membership to "+group+"?");
      if(action==true){
        var display_el = $(event.target).prev();
        var status;
        var message;
        var messageColor;
        $.ajax({
          type: "POST",
          url: "/account/my-group-list/",
          data: {
            'remove-membership':true,
            'group_name':group,
          },
          success: function(response){
            status = response['status']
            message = response['message']
            // determine the text color
            if(status=='ok'){
              messageColor = "text-success";
              // remove previous messages
              $('.text-success').remove()
            }else{
              messageColor = 'text-danger';
              $('.text-danger').remove()
              Lobibox.notify('error', {
                sound: false,
                delay: 5000,
                position: 'top right', //or 'center bottom'
                msg: message
                });
            }
            displaySuccessMessage(display_el,messageColor,message)
          },
          complete: function(){
            if(status=='ok'){
              // remove group from list
              $(event.target).parent().remove();
              Lobibox.notify('success', {
                sound: false,
                delay: 5000,
                position: 'top right', //or 'center bottom'
                msg: message
                });
            }
          }
        });
      };
    };
    if($(event.target).is('.delete-group')){
      event.preventDefault()
      var group_name = $(event.target).attr('group_name');
      var action = confirm("Delete group: "+group_name.toUpperCase()+"?");
      if(action==true){
        var display_el = $(event.target).prev();
        var status;
        var message;
        var messageColor;
        $.ajax({
          type: "POST",
          url: "/account/my-group-list/",
          data: {
            'delete-group':true,
            group_name,
          },
          success: function(response){
            status = response['status']
            message = response['message']
            // determine the text color
            if(status=='ok'){
              messageColor = "text-success";
              // remove previous messages
              $('.text-success').remove()

            }else{
              messageColor = 'text-danger';
              $('.text-danger').remove()
              Lobibox.notify('error', {
                size: 'mini',
                sound: false,
                rounded: true,
                delay: 5000,
                position: 'top right', //or 'center bottom'
                msg: message
                });
            }
            displaySuccessMessage(display_el,messageColor,message)
          },
          complete: function(){
            if(status=='ok'){
              // remove group from list
              $(event.target).parent().remove();
              Lobibox.notify('success', {
                size: 'mini',
                sound: false,
                rounded: true,
                delay: 5000,
                position: 'top right', //or 'center bottom'
                msg: message
                });
            }
          }
        })
      }
    }

  });

})
