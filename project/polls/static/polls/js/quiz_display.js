$(document).ready(function() {

    function startQuizTimer (quizTime,duration_el, form){
      var timer = $(duration_el)
                    .countdown(quizTime)
                    .on('update.countdown', function(event){
                      $(duration_el).children().children('span.quiz-time').text(
                        event.strftime('%-H hr%!H %M min%!M %S sec%!S:s;')
                      )
                    })
                    .countdown('start')
                    .on('finish.countdown', function(event){
                      $(form).ajaxSubmit(
                        {
                          target: '.target',
                          clearForm: true
                        }
                      )
                    })

      return timer;
    }
    $('#id_start_quiz').click(function(event){
      // get the quiz time and start the timer
      $.ajax({
        type: 'get',
        url: $(this).attr('url'),
        success: function(data){
          // convert the data to JavaScript date object
          var time = new Date(data);
          startQuizTimer(time,'p.quiz-time','form#submit-quiz')
        },
        error: function(){
          alert('error');
        }
      })
      $('div.form-options').show() // display the questions
      $('div.instructions').hide();
      $(this).hide();// hide the start button
    })

  //   /*
  //    Add pagination to the table
  //   */
    $('div.paginated').each(function() {
      var currentPage = 0;
      var numPerPage = 1;
      var $division = $(this);
      // var repaginate;
      $division.bind('repaginate', function() {
        $division.find('form div.section').show()
          .slice(0,(currentPage * numPerPage))
            .hide()
          .end()
          .slice(((currentPage + 1) * numPerPage))// .slice(((currentPage + 1) * numPerPage - 1)+1)
            .hide()
          .end();
      });
      var numRows = $division.find('form div.section').length;
      var numPages = Math.ceil(numRows / numPerPage);
      var $pagerContainer = $('<div></div>');
      var $pager = $('<ul class="pagination pagination-lg"></ul>');
      for (var page = 0; page < numPages; page++) {

        $("<li class='page-number'><a href='#'>" + (page + 1) + "</a></li>")
          .bind('click', {'newPage': page}, function(event) {
          currentPage = event.data['newPage'];
          $division.trigger('repaginate');
          // marking current page
          $(this).addClass('active').siblings().removeClass('active');

        })
          .appendTo($pager).addClass('clickable');
      }
      // marking current page
      $pager.find('span.page-number:first').addClass('active');
      $pager.appendTo($pagerContainer);
      $pagerContainer.prependTo($division);
      $pagerContainer.addClass('col-sm-6 col-sm-offset-3');

      $division.trigger('repaginate');
    });
    $('ul').click(function(event){
      if($(event.target).is('li.page-number a:last')){
        $('button.btn-primary[type=submit]').addClass('pull-right').removeClass('sr-only')
      }
    })
})
