$(document).ready(function() {
    // PREVENT INSIDE CLICK DROPDOWN
    $('.dropdown-menu').on("click.bs.dropdown", function (e) {
      e.stopPropagation();
      // e.preventDefault();
    });

    var getNotificationMessages = function(){
      // pull the notification messages into the Navbar
      $.get('/polls/feed/notifications/', function(data){
        if(data !== ""){
          // $('li.notify').empty();
          $('#notification-list').prepend(data);
          var count_unseen = $('div.unseen-count').attr('count');
          // update the count if not zero
          if(count_unseen > 0){
            $('span.new-messages.badge').text(count_unseen);
        }
      };
    });
  }
    // call the notification method
    getNotificationMessages();
    // (function worker() {
    //   var message = 'unseen';
    //   var message_count;
    //   $.ajax({
    //     type: "GET",
    //     url: '/polls/feed/notifications/',
    //     data: {message,
    //          },
    //     success: function(data) {
    //       message_count = data['unseen_count'];
    //       $('#currentnotifications').empty();
    //       $('#currentnotifications').html(data['unseen_count']);
    //     },
    //     complete: function() {
    //       // compare previous data with new data
    //       var previousnotifications = parseInt($('a.notification').attr('data-notification'));
    //       var currentnotifications = message_count;
    //       var statuschanged = previousnotifications !== currentnotifications;
    //
    //       if(statuschanged){
    //         // append new data and show notification in the icon
    //             $('a.notification').attr({'data-notification':message_count})
    //             if(message_count == 0){
    //               $('a.notification').removeAttr('data-notification');
    //             }
    //       }
    //       // Schedule the next request when the current one's complete
    //       setTimeout(worker, 10000); // every 10 seconds
    //     }
    //   });
    // })();

});
