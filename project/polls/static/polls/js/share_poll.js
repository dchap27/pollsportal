$(document).ready(function(){
  // create a WebSocket connection
  const webSocketBridge = new channels.WebSocketBridge();
  webSocketBridge.connect('/ws/updates/dashboard');
  webSocketBridge.listen();
  $('.share-with-friends').hide();
  $('.share-with-groups').hide();

  // insert a check box to select all
  $('<div></div>')
  .html('<label><input type="checkbox" id="select-friends-all" /> <em> select all</em></label>')
  .prependTo('div.share-with-friends ul.share-poll');

  $('<div></div>')
  .html('<label><input type="checkbox" id="select-groups-all" /> <em> select all</em></label>')
  .prependTo('div.share-with-groups ul.share-poll');

  // toggle class base on selection
  $('.share-type').click(function(event){
    $('div.warning').remove();
    if($(event.target).is('#id_group')){
      $('.share-with-friends').hide();
      $('.share-with-groups').show();
      $('#id_group').attr('checked',true);
      $('#id_friend').attr('checked',false);
      var $initialboxes;
      // deselect groupcheck if any
      $initialboxes = $('#select-friends-all').parents('ul:first').find(':checkbox');
      if($('#select-friends-all').checked){
        // change text to 'deselect' if checked
        $('#select-friends-all').next().text(' deselect all');
        $('#select-friends-all').attr('checked',false)
      }
      $initialboxes.each(function(){
        if(this.checked){
          $(this).attr('checked',false);
        }
      })
      // style the select checkboxes
      $("#select-groups-all").parent('label')
        .css({
          borderBottom: '1px solid #ccc',
          color: '#777',
          lineHeight: 2
        });

    };
    if($(event.target).is('#id_friend')){
      $('.share-with-friends').show();
      $('.share-with-groups').hide();
      $('#id_friend').attr('checked',true);
      $('#id_group').attr('checked',false);
      var $initialboxes;
      // deselect groupcheck if any
      $initialboxes = $('#select-groups-all').parents('ul:first').find(':checkbox');
      if($('#select-groups-all').checked){
        // change text to 'deselect' if checked
        $('#select-groups-all').next().text(' deselect all');
        $('#select-groups-all').attr('checked',false)
      }
      $initialboxes.each(function(){
        if(this.checked){
          $(this).attr('checked',false);
        }
      })
      // style the select checkboxes
      $("#select-friends-all").parent('label')
        .css({
          borderBottom: '1px solid #ccc',
          color: '#777',
          lineHeight: 2
        });

    };
  })

  $('.shared-content').click(function(event) {
    if($(event.target).is('#select-friends-all') || $(event.target).is('#select-groups-all')){
      var $checkboxes;
      $checkboxes = $(event.target).parents('ul:first').find(':checkbox');
      if (event.target.checked) {
        // change text deselect if checked
        $(event.target).next().text(' deselect all');
        $checkboxes.each(function(){
          $(this).attr('checked',true);
        })
      } else {
        $(event.target).next().text(' select all');
        $checkboxes.each(function(){
          $(this).attr('checked',false);
        })
      };
      $('div.warning').remove();
      }
    })

  $('form').submit(function(event){
      // event.preventDefault()
      var $initialboxes;
      var friendCheck = $('#id_friend');
      var groupCheck = $('#id_group');
      if(groupCheck.checked){
        // deselect friendcheck if any
        $initialboxes = $('#select-friends-all').parents('ul:first').find(':checkbox');
        $initialboxes.attr('checked',false);
      }else if(friendCheck.checked){
        // deselect groupcheck if any
        $initialboxes = $('#select-groups-all').parents('ul:first').find(':checkbox');
        $initialboxes.attr('checked',false);
      }
      // check in checkboxes is empty
      var $checkboxes = $(this).children('ul.share-poll').find(':checkbox');
      var countChecked = 0;
      var numCheckboxes = ($checkboxes).length;
      var $checkb = $(this).children('ul.share-poll');
      $(':checkbox', $checkb).each(function(){
        // var $checkb = $checkboxes.children('ul.share-poll').find(':checkbox');
        if(this.checked){
          countChecked += 1;
        }
      })
      if(countChecked == 0){
        $('div.text-danger').remove();
        // // insert a check box to select all
        // $('<div></div>')
        // .addClass('warning')
        // .html('<em> You\'ve not made any selection</em>')
        // .insertAfter('div.share-type');
        var message = 'You\'ve not made any selection';
        var messageColor = 'text-danger';
        displaySuccessMessage('div.share-type',messageColor,message);
        return false;
      }else {
        $('div.text-danger').remove();
        // submit form load response into target
        $(this).ajaxSubmit(
          {
            target: '.shared-content',
            clearForm: true
          }
        );
        // send a message to stream
        webSocketBridge.stream('messagenotify').send({'activity': 'share'});
        return false;
      }
  });

});
function displaySuccessMessage(result_el,messageColor,message){
  // display a success message and disappear in 10seconds
  var message = $('<div></div>')
                    .addClass(messageColor)
                    .append(message)
                    .insertAfter(result_el)
                    .fadeIn('fast').delay(10000).fadeOut('slow')
  return message
}
