# using stream_framework to handle notification feeeds
from stream_framework.feeds.redis import RedisFeed
from stream_framework.aggregators.base import RecentVerbAggregator, NotificationAggregator
from stream_framework.feeds.aggregated_feed.redis import RedisAggregatedFeed
from stream_framework.activity import AggregatedActivity
from stream_framework.feeds.notification_feed.redis import RedisNotificationFeed


class QuestionFeed(RedisFeed):
    key_format ='feed:normal:%(user_id)s'

class AggregatedQuestionFeed(RedisAggregatedFeed):
    aggregator_class = RecentVerbAggregator
    key_format = 'feed:aggregated:%(user_id)s'

class UserQuestionFeed(QuestionFeed):
    key_format ='feed:user:%(user_id)s'


class MyNotificationFeed(RedisNotificationFeed):
    """
    class subclass RedisNotificationFeed and chooses an aggregator
    """
    # : they key format determines where the data gets stored
    key_format ='feed:notification:%(user_id)s'
    # : the aggregator controls how the activities get aggregated
    aggregator_class = NotificationAggregator
