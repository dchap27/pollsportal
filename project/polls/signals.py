from django.db.models.signals import m2m_changed, post_save
from django.dispatch import receiver
from project.polls.models import Question, Quiz, ActivePoll
from channels import Group
from json import dumps


@receiver(m2m_changed, sender=Question.user_liked.through)
def user_liked_changed(sender, instance, **kwargs):
    instance.total_likes = instance.user_liked.count()
    instance.save()

@receiver(m2m_changed, sender=Question.users_voted.through)
def users_voted_changed(sender, instance, **kwargs):
    instance.total_votes = instance.users_voted.count()
    instance.save()

@receiver(m2m_changed, sender=Quiz.user_liked.through)
def quiz_user_liked_changed(sender, instance, **kwargs):
    instance.total_likes = instance.user_liked.count()
    instance.save()

@receiver(post_save, sender=Question)
def create_poll(sender, instance, **kwargs):
    """
    Post save handler to create/update poll instances when
    Quiz or Question is created/update
    """
    try:
        poll = ActivePoll.objects.get(
            poll_type=instance.poll_type,
            single_poll=instance
        )
    except ActivePoll.DoesNotExist:
        poll = ActivePoll(
            poll_type=instance.poll_type,
            poll_title=instance.question_text,
            single_poll=instance
        )
    poll.single_poll = instance
    poll.publish = instance.publish
    poll.save()

@receiver(post_save, sender=Quiz)
def create_quiz_poll(sender, instance, **kwargs):
    try:
        poll = ActivePoll.objects.get(
                poll_type=instance.poll_type,
                quiz_poll=instance
            )
    except ActivePoll.DoesNotExist:
        poll = ActivePoll(
            poll_type=instance.poll_type,
            poll_title=instance.title,
            quiz_poll=instance)

    poll.quiz_poll = instance
    poll.publish = instance.publish
    poll.save()
