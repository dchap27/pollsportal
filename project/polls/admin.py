from django.contrib import admin
from project.polls.models import *

# Customize the Header and title
admin.site.site_header = "PollsPortal Admin"
admin.site.index_title = "Manage Site"
admin.site.site_title = "PollsPortal"
# Register your models here.
class PollSettingsAdmin(admin.ModelAdmin):
    list_display = ('question','who_can_respond','duration',
        'presentation','start_date','end_date', 'begins')

admin.site.register(PollSettings,PollSettingsAdmin)

class QuizChoiceInline(admin.TabularInline):
    model = QuizChoice

@admin.register(QuizQuestion)
class QuizQuestionAdmin(admin.ModelAdmin):
    list_display = ('quiz_question_text','question_type','quiz')
    search_fields = ('quiz_question_text',)
    inlines = [QuizChoiceInline]

@admin.register(OpenAnswer)
class OpenAnswerAdmin(admin.ModelAdmin):
    list_display = ('answer',)

class QuizQuestionInline(admin.TabularInline):
    model = QuizQuestion

@admin.register(Quiz)
class QuizAdmin(admin.ModelAdmin):
    list_display= ('title','owner','mode','attempt','quiz_duration','publish','status')
    list_editable = ['mode','quiz_duration','attempt']
    list_filter = ('owner','title','status')
    search_fields = ('title', 'owner','status')
    date_hierarchy = 'publish'
    prepopulated_fields = {'slug': ('title',)}
    inlines = [QuizQuestionInline]

@admin.register(QuizTakerScore)
class QuizTakerScoreAdmin(admin.ModelAdmin):
    list_display = ('quiz_taker','quiz','score','number_of_attempt')

class ChoiceInline(admin.TabularInline):
    model= Choice
    extra=1 # extra number of formsets to display

@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display=('owner','question_text','slug','total_likes',
        'total_votes','created','publish','status','lock_date')
    list_filter = ('owner','question_text','status')
    search_fields = ('question_text','status')
    prepopulated_fields = {'slug': ('question_text',)}
    inlines = [ChoiceInline]

@admin.register(ActivePoll)
class ActivePollAdmin(admin.ModelAdmin):
    list_display = ['poll_type','single_poll','quiz_poll']

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display=('name','question','created','active')
    list_filter=('active','created','updated')
    search_fields=('name','body')

@admin.register(FeedBack)
class FeedBackAdmin(admin.ModelAdmin):
    list_display=('user','feedback_text')
    search_fields=('user','created')
