from celery import shared_task
from django.conf import settings

from email.mime.image import MIMEImage

from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string

User = settings.AUTH_USER_MODEL

@shared_task
def new_vote(user_id, object_id):
    """
    Task to send an e-mail notification when a user voted on opinion poll.
    """
    from project.polls.models import Question
    performer = User.objects.get(id=user_id)
    question = Question.objects.select_related('owner').get(id=object_id)
    owner = question.owner

    # context for the email template
    context = {
        'owner': owner.username,
        'performer': performer.username,
        'question': question,
        'protocol':'https',
        'domain':settings.SITE_HOST
    }
    # set email variables
    subject = "A new vote occured on your poll"
    recipient = [owner.email]
    reply_to = ['noreply@pollsportal.com']

    html_content = render_to_string(
        "polls/mail/new_vote_message.html",
        context=context).strip()

    msg = EmailMultiAlternatives(subject,html_content,settings.DEFAULT_FROM_EMAIL,
        to=recipient,reply_to=reply_to)
    msg.content_subtype = 'html'  # Main content is text/html
    msg.mixed_subtype = 'related'  # This is critical, otherwise images
    # will be displayed as attachments!
    msg.send()
