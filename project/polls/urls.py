from django.conf.urls import url, include
from project.polls import views

app_name='polls'
urlpatterns = [
    #Browsing
    # ex: /polls/5/
    url(r'^(?P<question_id>[0-9]+)/(?P<slug>[-\w]+)/$',
        views.detail, name='detail'),
    # ex: /polls/5/results/
    url(r'^(?P<question_id>[0-9]+)/(?P<slug>[-\w]+)/results/$',
        views.results, name='results'),
    url(r'^feat/$', views.features, name='features'),
    url(r'^popular/$', views.popular_polls, name='popular'),
    url(r'^recent/$', views.recent_polls, name='recent'),
    url(r'^search/$', views.search_page, name='search'),
    url(r'^list-voters/$', views.get_voters_list, name='voters'),
    url(r'^graphs/$', views.dashboard_graphs, name="graphs"),
    url(r'^([-\w]+)/voted/$', views.voted_polls, name='voted_polls'),
    url(r'^([-\w]+)/liked/$', views.liked_polls, name='liked_polls'),
    url(r'^([-\w]+)/commented/$', views.commented_polls, name='commented_polls'),

    # content management
    url(r'^question/(?P<question_id>[0-9]+)/settings/(?P<poll_type>[-\w]+)/$',
        views.question_settings, name='questionsettings'),
    url(r'remove/poll/(?P<question_id>[0-9]+)/$', views.suspend_poll, name='suspend'),
    url(r'^comments/(?P<question_id>[0-9]+)/$', views.comments, name='comments'),
    # ex: /polls/5/vote/
    url(r'^vote/(?P<question_id>[0-9]+)/$', views.vote, name='vote'),
    url(r'^dashboard/user/([-\w]+)/$', views.dashboard, name='dashboard'),
    url(r'^user/([-\w]+)/$', views.user_page, name='user_page'),
    url(r'^([-\w]+)/save/$', views.question_save, name='save_page'),
    url(r'^edit/(?P<question_id>[0-9]+)/save/$', views.question_edit,
        name='edit_question'),
    url(r'^submit_a_poll/$',views.create_anonymous, name='create'),
    url(r'^like/(?P<question_id>[0-9]+)/$', views.like, name="like"),
    url(r'^new-share/(?P<question_id>[0-9]+)/(?P<poll_type>[-\w]+)/$',
        views.share, name="share"),
    url(r'^share/ranking/$', views.share_ranking, name="share_ranks"),

    # quiz Browsing
    url(r'^quiz/(?P<quiz_id>[0-9]+)/(?P<slug>[-\w]+)/$', views.quiz_detail,
        name='quiz_detail'),
    # quiz content management
    url(r'^([-\w]+)/quiz/presave/$', views.pre_create_quiz, name='pre_save_quiz'),
    url(r'^create/(?P<quiz_id>[0-9]+)/quiz/save/$', views.create_new_quiz,
        name='save_quiz'),
    url(r'^edit/(?P<quiz_id>[0-9]+)/quiz/$', views.edit_quiz, name='edit_quiz'),
    url(r'^quiz_question/edit/(?P<question_id>[0-9]+)/quiz/$',
        views.edit_quiz_question, name='edit_quiz_question'),
    url(r'^quiz_vote/(?P<quiz_id>[0-9]+)/(?P<slug>[-\w]+)/$', views.quiz_vote,
        name='quiz_vote'),

    # Friends
    url(r'^friends/([-\w]+)/$', views.friends_page, name='friends'),
    url(r'^friend/follow/$', views.friend_follow, name='friend_follow'),
    url(r'^friend/invite/$', views.friend_invite, name='friend_invite'),
    url(r'^friend/accept/([-\w]+)/$', views.friend_accept, name='friend_accept'),

    # mail
    url(r'^send_mail/$', views.send_bulk, name='sendmail'),
    url(r'^personal_mail/$', views.one_message, name='personalmail'),

    # feeds management
    url(r'^news_feed/$', views.feeds, name='feeds'),
    url(r'^feed/notifications/$', views.notification, name='notification'),
    url(r'^seen_activity/$', views.mark_as_seen,),

    url(r'^question/(?P<question_id>[0-9]+)/$', views.detail2, name='detail2'),
]
