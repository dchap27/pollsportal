from django.apps import AppConfig


class PollsConfig(AppConfig):
    name = 'project.polls'

    def ready(self):
        # import signal handlers
        import project.polls.signals
