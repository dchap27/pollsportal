import redis
import json
from django.shortcuts import render, get_object_or_404, render_to_response, redirect
from django.http import Http404, HttpResponseRedirect, HttpResponse, JsonResponse
from .models import *
from .forms import *
from project.account.models import Profile, Group, Invitation, Friendship, User
from project.account.forms import GroupForm, FriendInviteForm, AccountSettingsForm
# helps to avoid race conditions & enable use of | OR and & AND
from django.db.models import F, Q
from django.db.utils import IntegrityError
from django.forms import inlineformset_factory, modelformset_factory
# To including message interfaces
from django.contrib import messages
from django.contrib.auth.decorators import login_required
# Applying page numbers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.signing import Signer, BadSignature
from project.polls.common.decorators import ajax_required
from django.views.decorators.http import require_POST
from datetime import datetime, timedelta, date
from django.utils import timezone
from django.core.mail import EmailMessage
from django.core.mail import send_mail, send_mass_mail
from django.template import Context, loader
from django.conf import settings
# using stream_framework for Notifications
from project.polls.feeds import MyNotificationFeed
from stream_framework.feeds.notification_feed.base import BaseNotificationFeed
from project.polls.feed_managers import manager, notify
from config.tasks import new_vote, invitation_mail

# connect to redis
r = redis.StrictRedis(host=settings.REDIS_HOST,
                      port=settings.REDIS_PORT,
                      db=settings.REDIS_DB)

# GLOBAL CONSTANTS
ITEMS_PER_PAGE = 10

NOW = timezone.now()

# Create your views here.
def privacy_policy(request):
    return render (request,'polls/privacy_policy.html',{
        'head_title': "PollsPortal Privacy Policy",
    })

def terms_of_service(request):
    return render (request,'polls/terms_service.html',{
        'head_title': "Terms of Service",
    })

def help_page(request):
    return render(request,'polls/help.html',
        {
        'head_title': "PollsPortal Help Page"
        }

    )

def feedback_page(request):
    if request.method == 'POST':
        feedback_form = FeedBackForm(data=request.POST) # Post a comment
        if feedback_form.is_valid():
            # Now create a feedback object but not yet saved to database
            new_feedback = feedback_form.save(commit=False)

            new_feedback.user = request.user
            # now save the comment to the database
            new_feedback.save()

            messages.success(request,
              "Message submitted! Thank you."
            )
            return HttpResponseRedirect(reverse (
             'index'
            ))
        else:
            messages.warning(request,
              "You can't submit a blank form. Please write us a message!"
            )
    else:
        feedback_form = FeedBackForm()
    return render(request, 'polls/feedback.html',
                 {
                  'feedback_form': feedback_form
                 })

# make the list unique
# courtesy https://www.peterbe.com/plog/uniqifiers-benchmark
def make_unique(seq):
    # Not order preserving
    keys = {}
    for e in seq:
        keys[e] = 1
    return keys.keys()

def index(request):
    # Get queryset of quizzes and questions
    # exclude polls with 'draft' and 'suspended' status
    polls = ActivePoll.objects.exclude(single_poll__suspended=True,quiz_poll__suspended=True)\
                          .select_related('single_poll__owner','quiz_poll__owner')
    polls = polls.order_by('-publish')
    #create list of questions and exclude question with 'draft' status
    questions = Question.objects.exclude(
        Q(suspended=True) | Q(publish=None)
    ).order_by('-publish')  # order by most recent
    # # questions = Question.objects.exclude(suspended=True)
    # questions = questions.order_by('-publish')  # order by most recent
    # Display all actions by default
    # initialize the actions when user not logged in
    # actions =""
    # if request.user.is_authenticated():  # incase user is not logged in
    #     actions = Action.objects.all()
    #     following_ids = request.user.following.values_list('id',flat=True) # check if user follow others
    #     # following_ids.append(request.user.id)
    #
    # # if the user follows others, then
    #     if following_ids:
    #         # if user is following others, retrieve only others actions
    #         actions1 = Q(user_id__in=following_ids)
    #         actions2 = Q(user_id=request.user.id)
    #         actions = actions.filter(actions1|actions2)\
    #                          .select_related('user','user__profile')\
    #                          .prefetch_related('target')
    # if request.user.is_authenticated(): # actions when logged in
    #     actions = actions[:8]

    paginator = Paginator(polls, ITEMS_PER_PAGE)  # paging the no of items
    page = request.GET.get('page')
    try:
        polls = paginator.page(page)
    except PageNotAnInteger:
        page = 1
        # if page is not an integer, deliver first page
        polls = paginator.page(1).object_list
    except EmptyPage:
        if request.is_ajax():
            # If the request is AJAX and the page is out of range
            # return an empty page
            return HttpResponse('')
        # If page is out of range deliver last page of results
        polls = paginator.page(paginator.num_pages).object_list

    feed = manager.get_feeds(request.user.id)['aggregated']
    feed2 = MyNotificationFeed(request.user)
    unseen_messages = feed2.count_unseen()
    notification_messages = list(feed2[:10])
    activities2 = list(feed[:200])
    print(activities2)
    # num_view = r.get('poll:19:views')
    # # unpack the returned values in enrich_aggregated_activities
    # aggregated_activities = enrich_aggregated_activities(activities2)

    # The variables to render to the template
    variables = {
       'polls': polls,
       'questions': questions,
       'unseen_messages': unseen_messages,
       # 'feed_acts': enrich_activities(activities),
       'feed_aggr': enrich_aggregated_activities(activities2),
       'short_notification_messages': enrich_aggregated_activities(notification_messages),
       'show_paginator': paginator.num_pages > 1,
       'has_previous': paginator.page(page).has_previous(), # returns True or False
       'has_next': paginator.page(page).has_next(),  # returns True or False
       'page': page,
       'pages': paginator.num_pages,
       'head_title': 'Polls Portal',
       'page_title': 'Welcome to Polls Portal',
       'Welcome_message': 'A place where you can share your views and opinions',
       "show_user": False,
       "show_category":False,
       "date":timezone.now()
    }
    if request.user.is_authenticated():
        if request.is_ajax():
            return render(request, 'polls/question_feed_display.html',variables)

        return render(request, 'polls/index.html', variables)
    else:
        return render(request, 'polls/index_cover.html',variables)

def detail(request, question_id,slug):
    question = get_object_or_404(Question, pk=question_id, slug=slug)
    comments = question.comments.filter(active=True) # filter active comments
    if question.suspended == True:
        messages.info(request,
          "Poll {} you requested has been suspended for violation of our Terms of Use. If you believe this poll was\
          suspended in error, please contact support@pollsportal.com".format(question.id)
        )
        return render(request,'polls/suspended.html',
                {
                "head_title": 'Suspended'
                }
        )

    # increment total image views by 1
    total_views = r.incr('poll:{}:views'.format(question.id))
    # get number of shares received
    total_shares = r.get('question:{}:shares'.format(question.id))

    return render(request, 'polls/detail.html',
                 {
                 'question': question,
                 'comments': comments,
                 'description': question.question_text,
                 'total_views': total_views,
                 'total_shares': total_shares,
                 })

def detail2(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    comments = question.comments.filter(active=True) # filter active comments
    return render(request, 'polls/question_single_ajax.html',
                 {
                 'question': question,
                 'description': question.question_text,
                 'comments': comments,
                 })

# To display results for a particular question
def results(request, question_id, slug):
    question = get_object_or_404(Question, pk=question_id, slug=slug)
    if question.suspended == True:
        messages.info(request,
          "Poll {} you requested has been suspended for violation of our Terms of Use. If you believe this poll has\
          been suspended in error, please contact support@pollsportal.com".format(question.id)
        )
        return render(request,'polls/suspended.html',
                {
                "head_title": 'Suspended'
                }
        )
    tot_votes = (question.total_votes)
    male_votes=0
    female_votes=0
    for choice in question.choice_set.all():
        male_votes += choice.m_votes
        female_votes += choice.f_votes
    # Check if user is superuser
    if not request.user.is_superuser:
        if request.user != question.owner :
            if question.eligibility:
                profile = Profile.objects.get(user=request.user)
                if profile.gender == question.eligible_gender:
                    if not request.user in question.users_voted.all():
                        return render(request,'polls/detail.html',{
                            'question':question,
                            'error_message':"You need to vote on poll question before you can\
                            view or comment on results!"
                        })
            # Check if user have voted before viewing results
            elif not request.user in question.users_voted.all():
                return render(request,'polls/detail.html',{
                    'question':question,
                    'error_message':"You need to vote on poll question before you can\
                    view or comment on results!"
                })
    comments = question.comments.filter(active=True) # filter active comments

    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST) # Post a comment
        if comment_form.is_valid():
            # Now create a comment object but not yet saved to database
            new_comment = comment_form.save(commit=False)
            # then assign the current question to the comment
            new_comment.question = question
            new_comment.name = request.user
            # now save the comment to the database
            new_comment.save()
            this_month = NOW.month
            year = NOW.year
            # Add to the number of user comments for the month
            user_activities_this_month = r.zincrby(
                'user_{}_activity{}-{}'.format(request.user.id,this_month,year),
                'comments',1)
            # Add to global comments
            activities_count = r.zincrby(
                'total_activities{}-{}'.format(this_month,year),
                'comments',1)
            # To create an action for the addition of comments
            # action = create_action(request.user,
            #     'commented on', new_comment
            #     )
            from stream_framework.verbs.base import Comment as CommentVerb
            manager.add_action(question, request.user, CommentVerb, timezone.now())
            notify.add_notify_action(question, request.user, CommentVerb)
            #
            # # Notify other commentators of that poll question
            # new_comment_created.delay(question_id,request.user)

            comment_form=CommentForm()  # clearing the previous comment from the form

    else:
        comment_form = CommentForm()
    try:
        next_question = Question.objects.get(pk=question.id+1)
    except:
        next_question = Question.objects.get(pk=question.id)
    variables = {'question':question,
                 'next_question': next_question,
                 'comments': comments,
                 'comment_form': comment_form,
                 'tot_votes':tot_votes,
                 'male_votes':male_votes,
                 'female_votes':female_votes
             }
    if request.is_ajax():
        return render(request, 'polls/result_ajax.html', variables)
    return render(request, 'polls/result.html', variables)


def comments(request,question_id):
    next = request.META.get('HTTP_REFERER', None) or '/'
    question = get_object_or_404(Question, pk=question_id)
    this_month = NOW.month
    year = NOW.year
    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST) # Post a comment
        if comment_form.is_valid():
            # Now create a comment object but not yet saved to database
            new_comment = comment_form.save(commit=False)
            # then assign the current question to the comment
            new_comment.question = question
            new_comment.name = request.user
            # now save the comment to the database
            new_comment.save()

            # Add to the number of user comments for the month
            user_activities_this_month = r.zincrby(
                'user_{}_activity{}-{}'.format(request.user.id,this_month,year),
                'comments',1)
            # Add to global comments
            activities_count = r.zincrby(
                'total_activities{}-{}'.format(this_month,year),
                'comments',1)
            # To create an action for the addition of comments
            # action = create_action(request.user,
            #     'commented on', new_comment
            #     )
            from stream_framework.verbs.base import Comment as CommentVerb
            manager.add_action(question, request.user, CommentVerb, timezone.now())
            notify.add_notify_action(question, request.user, CommentVerb)
            #
            # # Notify other commentators of that poll question
            # new_comment_created.delay(question_id,request.user)

            comment_form=CommentForm()  # clearing the previous comment from the form
            comment_url = "/polls/comments/{}/".format(question_id)
            if request.is_ajax():
                return JsonResponse({'data':comment_url})
            messages.success(request, "comment added!")
            return HttpResponseRedirect(next)

    else:
        comment_form = CommentForm()
    variables = {
        'form':comment_form,
        'question': question
    }
    return render(request,"polls/comments.html", variables)

def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    next = request.META.get('HTTP_REFERER', None) or '/'
    if question.get_status() == "Draft" or question.get_status() == "Pending":
        if request.is_ajax():
            variables = {
               'status':'ko',
               'message':"This poll is {}".format(question.get_status()),
            }
            return JsonResponse(variables)
        messages.warning(request,"This poll is not active")
        return HttpResponseRedirect(next)
    if question.get_status() == "Closed":
        if request.is_ajax():
            variables = {
               'status':'ko',
               'message':"This poll is closed",
            }
            return JsonResponse(variables)
        messages.warning(request,"This poll is closed")
        return HttpResponseRedirect(next)

    if question.suspended == True:
        messages.info(request,
          "Poll {} you requested has been suspended for violation of our Terms of Use. If you believe this poll was\
          suspended in error, please contact support@pollsportal.com".format(question.id)
        )
        return render(request,'polls/suspended.html',
                {
                "head_title": 'Suspended'
                }
        )
    try:
        # Now get the particular choice selected
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Now redisplay the question voting form
        return render(request,'polls/detail.html',{
            'question':question,
            'error_message':"You didn't select a choice"
        })
    if not request.user.is_authenticated():
        return process_anonymous_vote(request,question,selected_choice)
    # checking if user has already profile
    profile, created = Profile.objects.get_or_create(user=request.user)
    if question.eligibility:
        if not profile.gender == question.eligible_gender:
            eligible = question.eligible_gender
            if eligible == "M":
                sex = "MALE"
            else:
                sex = "FEMALE"
            if created:
                if request.is_ajax():
                    variables = {
                       'status':'ko',
                       'message':"Sorry! Update your profile to be eligible to vote on this poll",
                    }
                    return JsonResponse(variables)
                return render(request,'polls/detail.html',{
                    'question':question,
                    'error_message':"You are not eligible to vote on this poll. For  '{}S'  only".format(sex)
                })
            if request.is_ajax():
                variables = {
                   'status':'ko',
                   'message':"Sorry! You are not eligible to vote on this poll",
                }
                return JsonResponse(variables)
            return render(request,'polls/detail.html',{
                'question':question,
                'error_message':"You are not eligible to vote on this poll. For  '{}S'  only".format(sex)
            })
    # checking if user has already voted by filtering
    user_voted = question.users_voted.filter(
        username=request.user.username
    )
    if not user_voted:
        # update the vote counter
        selected_choice.votes = F('votes') + 1  # allow for database to do the arithmetic
        # question.total_votes += 1
        question.users_voted.add(request.user)

        # group choice
        if profile.gender == 'M':
            selected_choice.m_votes = F('m_votes') + 1
        elif profile.gender == 'F':
            selected_choice.f_votes = F('f_votes') + 1
        else:
            pass # pass in cases where user hasn't update profile
        #selected_choice.votes += 1
        selected_choice.save()
        question.save()
        question.refresh_from_db()
        selected_choice.refresh_from_db() # To refresh the database to access the new value
        this_month = NOW.month
        year = NOW.year
        # Add to global votes for the month
        activities_count = r.zincrby('total_activities{}-{}'.format(this_month,year),'votes',1)
        # Add to number of user votes for the month
        user_activities_this_month = r.zincrby('user_{}_activity{}-{}'.format(request.user.id,this_month,year),'votes',1)
        # # create action when users voted
        # action = create_action(request.user, 'voted on', question)
        from .verbs import Vote as VoteVerb
        manager.add_action(question, request.user, VoteVerb, timezone.now())
        notify.add_notify_action(question, request.user, VoteVerb)
        #
        # Send a notification to the creator of the poll by email
        new_vote.delay(request.user.id,question_id)

    else:
        if request.is_ajax():
            variables = {
               'status':'ko',
               'message':"Sorry! You can vote only once.",
            }
            return JsonResponse(variables)
        return render(request,'polls/detail.html',{
            'question':question,
            'error_message':"Sorry! You can vote only once. Try another poll question!"
        })

    # Then redirect back
    # if request.META.has_key('HTTP_REFERER'): # Redirect back to where it came
    #     return HttpResponseRedirect(request.META['HTTP_REFERER'])
    if request.is_ajax():
        variables = {
            'status':'ok',
            'message': 'Your vote added successfully',
            'success_url': "/polls/question/{}".format(question.id),
            'total_votes':question.total_votes
        }
        return JsonResponse(variables)
    return HttpResponseRedirect(reverse (
     'polls:results', args=(question.id,question.slug)
    ))

def get_voters_list(request):
    """
    return list of voters as Json
    """
    if request.GET.__contains__('question'):
        question_id = request.GET['question']
    else:
        variables = {
           'status':'ko',
           'msg':'Nothing to show! You\'ve not voted'
        }
        return JsonResponse(variables)
    question = Question.objects.get(pk=question_id)
    voters = question.users_voted.all()
    variables = []
    for voter in voters:
        data = {
          'id': voter.id,
          'name': voter.username
        }
        variables.append(data)
    return JsonResponse(variables,safe=False)

def features(request):
    return render (request,'polls/features.html',{
        'head_title': "Polls Portal",
    })

@login_required(login_url='polls:login')
def dashboard(request, username):
    #user = User.objects.get(username=username)
    today = NOW.today()
    enddate = today + timedelta(90)
    tomorrow = today + timedelta(1)
    yesterday = today - timedelta(1)
    last_seven_days = today - timedelta(7)
    last_30_days = today - timedelta(30)
    # recent = Question.objects.filter(publish__gte=yesterday)
    person = get_object_or_404(User, username=username)
    polls_voted = Question.objects.filter(users_voted=person)
    polls_commented = Question.objects.filter(comments__name__iexact=username)
    polls_liked = Question.objects.filter(user_liked=person)

    # Get the questions created by the user for editing table
    single_poll = Q(single_poll__owner=person)
    quiz = Q(quiz_poll__owner=person)
    polls = ActivePoll.objects.filter(single_poll|quiz)

    my_single_polls = Question.objects.filter(owner__username=username)
    all_single_polls = Question.objects.all()
    try:
        poll_percent = (len(my_single_polls)/len(all_single_polls) * 100)
    except ZeroDivisionError:
        poll_percent = 0.0

    # polls created by user with time
    today_poll = polls.filter(publish__gte=today)
    last_seven_days_poll = polls.filter(publish__gte=last_seven_days)
    last_30_days_poll = polls.filter(publish__gte=last_30_days)

    # friends that request.user is following incase of friend invite
    friend_ids = request.user.following.values_list('id',flat=True)
    follower_ids = request.user.followers.values_list('id',flat=True)
    total_friends = len(friend_ids) + len(follower_ids)

    # Get the total votes through aggregate function
    from django.db.models import Sum
    user_votes = Question.objects.prefetch_related('users_voted')
    my_votes = 0
    for question in user_votes:
        if request.user in question.users_voted.all():
            my_votes += 1
    all_questions = Question.objects.aggregate(tot_votes=Sum('total_votes'))
    try:
        vote_percent = int((my_votes/all_questions['tot_votes']) *100)
    except (TypeError,ZeroDivisionError):
        vote_percent = 0.0

    # Get the realtime possible data
    my_active_single_polls = Question.objects.filter(
        owner__username=username,status='Active')
    my_pending_single_polls = Question.objects.filter(
        owner__username=username,status='Pending')
    mygroups = Group.objects.filter(owner=person)
    groups_user_belong = Group.objects.filter(
        members__username=username).exclude(owner=person)
    total_groups = len(mygroups) + len(groups_user_belong)
    my_questions = person.question_set.exclude(suspended=True).order_by('-id')

    # The variables to render to the template
    variables = {
       'questions': my_questions,
       'polls': polls,
       'poll_percent': "{0:.1f}".format(poll_percent),
       'total_groups': total_groups,
       'total_friends':total_friends,
       'groups_user_belong': groups_user_belong,
       'polls_voted': polls_voted,
       'polls_commented': polls_commented,
       'polls_liked': polls_liked,
       'today_poll': today_poll,
       'last_30_days_poll': last_30_days_poll,
       'vote_percent': "{0:.1f}".format(vote_percent),
       'total_pending_polls': len(my_pending_single_polls),
       'active_single_polls': len(my_active_single_polls),
       'last_seven_days_poll': len(last_seven_days_poll),
       'user': request.user,
       'username': username,
       'person':person,
       'section':'people',
       'show_user':True,
       'show_category':True,
       'today': today,
       'this_month':NOW.strftime('%B'),
       'tomorrow':tomorrow,
       'enddate':enddate,
       'settings':'settings',
       'chart_url': "/polls/graphs/"
    }
    if request.is_ajax():
        return render(request, 'polls/dashboard_ajax.html',variables)
    return render(request, 'polls/dashboard.html', variables)

def dashboard_graphs(request):
    this_month = NOW.month
    year = NOW.year
    month_name = NOW.strftime('%B')
    user_like = 0
    user_vote = 0
    user_share = 0
    user_comment = 0
    total_likes = r.zscore('total_activities%s-%s'%(this_month,year),'likes')
    user_like_this_month = r.zscore('user_%s_activity%s-%s'%(request.user.id,this_month,year),'likes')
    if total_likes != None and user_like_this_month != None:
        user_percent_like = (int(user_like_this_month) / int(total_likes)) * 100.0
        user_percent_like = float('{0:.1f}'.format(user_percent_like))
        user_like = user_like_this_month

    total_shares = r.zscore('total_activities%s-%s'%(this_month,year),'shares')
    user_share_this_month = r.zscore(
        'user_%s_activity%s-%s'%(request.user.id,this_month,year),
        'shares')
    if total_shares != None and user_share_this_month != None:
        user_percent_share = (int(user_share_this_month) / int(total_shares)) * 100.0
        user_percent_share = float('{0:.1f}'.format(user_percent_share))
        user_share = user_share_this_month

    total_votes = r.zscore('total_activities%s-%s'%(this_month,year),'votes')
    user_vote_this_month = r.zscore(
        'user_%s_activity%s-%s'%(request.user.id,this_month,year),
        'votes')
    if total_votes != None and user_vote_this_month != None:
        user_percent_vote = (int(user_vote_this_month) / int(total_votes)) * 100.0
        user_percent_vote = float('{0:.1f}'.format(user_percent_vote))
        user_vote = user_vote_this_month

    total_comments = r.zscore('total_activities%s-%s'%(this_month,year),'comments')
    user_comment_this_month = r.zscore(
        'user_%s_activity%s-%s'%(request.user.id,this_month,year),
        'comments')
    if total_comments != None and user_comment_this_month != None:
        user_percent_comment = (int(user_comment_this_month) / int(total_comments)) * 100.0
        user_percent_comment = float('{0:.1f}'.format(user_percent_comment))
        user_comment = user_comment_this_month

    chart = {
        'chart': {'type': 'pie'},
        'title': {'text': 'Activities'},
        'series': [{
            'name': 'Counts for {} - {}'.format(month_name,year),
            'data': [
                 ['likes', user_like],
                 ['Comments', user_comment],
                 ['Shares', user_share],
                 ['Votes', user_vote],
            ],
        }]
    }
    return JsonResponse(chart)

@login_required(login_url='account:login')
def user_page(request, username):
    user = get_object_or_404(User, username=username)
    if user == request.user:
        return HttpResponseRedirect(reverse (
         'polls:dashboard', args=(request.user.username,)
        ))
    polls = Question.objects.filter(owner=user).exclude(
        Q(suspended=True) | Q(publish=None)
    )
    today = NOW.today()
    yesterday = today - timedelta(1)
    votes = Question.objects.filter(users_voted=user)
    likes = Question.objects.filter(user_liked=user)
    comments = Question.objects.filter(comments__name__iexact=username)
    questions = Question.objects.filter(publish__gte=yesterday)
    friend_ids = user.following.values_list('id',flat=True)
    follower_ids = user.followers.values_list('id',flat=True)
    if request.user.is_authenticated():
        is_friend = Friendship.objects.filter(
           from_friend=request.user,
           to_friend=user
        )
    else:
        is_friend=False
    variables = {
      'questions':questions,
      'person': user,
      'username': user.username,
      'polls': polls,
      'likes':likes,
      'votes': votes,
      'comments': comments,
      'following':friend_ids,
      'followers':follower_ids
    }
    if request.is_ajax():
        return render(request, 'polls/user_page_ajax.html', variables)
    return render(request, 'polls/user_page.html', variables)

def recent_polls(request):
    # today = datetime.today()
    # yesterday = today - timedelta(2) #timedelta(2) refers to 2-days ago
    if request.GET.__contains__('username'):
        user = get_object_or_404(User, username=request.GET['username'])
        questions = Question.objects.filter(owner=user)
        questions = questions.exclude(Q(suspended=True) | Q(publish=None)).order_by('-publish')
        if request.is_ajax():
            if len(questions)> 9:
                more_view = True
            else: more_view = False
            questions = questions[:9]
            return render(request,'polls/recent_polls_ajax.html',{
                 'questions':questions,
                 'creator': user,
                 'show_user':True,
                 "show_polls":True,
                 'show_category':False,
                 'show_publish':False,
                 'more_view':more_view,
            })
        return render(request,'polls/recent_polls.html',{
             'questions':questions,
             'creator': user,
             "show_polls":True,
             'show_user':True,
             'show_category':True,
             'show_publish':True,
        })
    return HttpResponse('')

def popular_polls(request):
    popular_polls = Question.objects.filter(total_votes__gte=5)
    popular_polls = popular_polls.exclude(suspended=True)
    popular_polls = popular_polls.order_by('-total_votes')
    paginator = Paginator(popular_polls, 5)  # paging the no of items
    try:
        page = int(request.GET['page'])
    except:
        page = 1
        # if page is not an integer, deliver first page
        questions = paginator.page(1).object_list
    try:
        questions = paginator.page(page).object_list
    except:
        # if page is out of range, deliver last page of results
        questions = paginator.page(paginator.num_pages).object_list
    # The variables to render to the template
    variables = {
       'questions': questions,
       'show_paginator': paginator.num_pages > 1,
       'has_previous': paginator.page(page).has_previous(), # returns True or False
       'has_next': paginator.page(page).has_next(),  # returns True or False
       'page': page,
       'pages': paginator.num_pages,
       'next_page' : page + 1,
       'prev_page' : page - 1,
       'show_user':True,
       'show_category':True,
       "view_result":True,
    }
    return render(request, 'polls/popular_polls.html', variables)

def voted_polls(request,username):
    user = get_object_or_404(User, username=username)
    questions = Question.objects.exclude(suspended=True)
    questions = questions.filter(users_voted=user)
    paginator = Paginator(questions, ITEMS_PER_PAGE)  # paging the no of items
    try:
        page = int(request.GET['page'])
    except:
        page = 1
        # if page is not an integer, deliver first page
        questions = paginator.page(1).object_list
    try:
        questions = paginator.page(page).object_list
    except:
        # if page is out of range, deliver last page of results
        questions = paginator.page(paginator.num_pages).object_list
    if request.is_ajax():
        #questions = questions.filter(users_voted=user)
        return render(request,'polls/questions_list.html',
                     {
                      'questions':questions,
                      'view_function':'voted',
                      'username':user.username,
                      'show_votes':True,
                      'show_category':False,
                      "show_user":True,
                      'view_result':True,
                      'show_paginator': paginator.num_pages > 1,
                      'has_previous': paginator.page(page).has_previous(), # returns True or False
                      'has_next': paginator.page(page).has_next(),  # returns True or False
                      'page': page,
                      'pages': paginator.num_pages,
                      'next_page' : page + 1,
                      'prev_page' : page - 1
                     })
    else:
        return render(request,'polls/votes_list.html',
                     {
                      'questions':questions,
                      'username':user.username,
                      'show_category':True,
                      'show_votes':True,
                      "show_user":True,
                      "view_result":True,
                      'show_paginator': paginator.num_pages > 1,
                      'has_previous': paginator.page(page).has_previous(), # returns True or False
                      'has_next': paginator.page(page).has_next(),  # returns True or False
                      'page': page,
                      'pages': paginator.num_pages,
                      'next_page' : page + 1,
                      'prev_page' : page - 1,
                     })

def commented_polls(request,username):
    user = get_object_or_404(User, username=username)
    questions = Question.objects.exclude(suspended=True)
    questions = questions.filter(comments__name__iexact=user.username)
    # comments = question.comments.filter(active=True) # filter active comments
    paginator = Paginator(questions, ITEMS_PER_PAGE)  # paging the no of items
    try:
        page = int(request.GET['page'])
    except:
        page = 1
        # if page is not an integer, deliver first page
        questions = paginator.page(1).object_list
    try:
        questions = paginator.page(page).object_list
    except:
        # if page is out of range, deliver last page of results
        questions = paginator.page(paginator.num_pages).object_list
    if request.is_ajax():
        #questions = questions.filter(comments__name__iexact=user.username)
        return render(request,'polls/questions_list.html',
                     {
                      'questions': questions,
                      'view_function':'commented', # to load pagination dynamically
                      'username':user.username,
                      "show_comments":True,
                      'show_category': False,
                      "show_user":True,
                      'show_paginator': paginator.num_pages > 1,
                      'has_previous': paginator.page(page).has_previous(), # returns True or False
                      'has_next': paginator.page(page).has_next(),  # returns True or False
                      'page': page,
                      'pages': paginator.num_pages,
                      'next_page' : page + 1,
                      'prev_page' : page - 1,
                      "view_result":True
                     })
    return render(request,'polls/comments_list.html',
                 {
                  'questions': questions,
                  'username':user.username,
                  "show_comments":True,
                  'show_category': False,
                  "show_user":True,
                  "view_result":True,
                  'show_paginator': paginator.num_pages > 1,
                  'has_previous': paginator.page(page).has_previous(), # returns True or False
                  'has_next': paginator.page(page).has_next(),  # returns True or False
                  'page': page,
                  'pages': paginator.num_pages,
                  'next_page' : page + 1,
                  'prev_page' : page - 1,
                 })

def liked_polls(request,username):
    user = get_object_or_404(User, username=username)
    questions = Question.objects.exclude(suspended=True)
    questions = questions.filter(user_liked=user)
    paginator = Paginator(questions, ITEMS_PER_PAGE)  # paging the no of items
    try:
        page = int(request.GET['page'])
    except:
        page = 1
        # if page is not an integer, deliver first page
        questions = paginator.page(1).object_list
    try:
        questions = paginator.page(page).object_list
    except:
        # if page is out of range, deliver last page of results
        questions = paginator.page(paginator.num_pages).object_list
    variables = {
     'questions':questions,
     'username':user.username,
     'view_function':'liked',
     "show_likes":True,
     'show_category':True,
     "show_user":True,
     "show_votes":False,
     "view_result":True,
     'show_paginator': paginator.num_pages > 1,
     'has_previous': paginator.page(page).has_previous(), # returns True or False
     'has_next': paginator.page(page).has_next(),  # returns True or False
     'page': page,
     'pages': paginator.num_pages,
     'next_page' : page + 1,
     'prev_page' : page - 1,
    }
    if request.is_ajax():
        return render(request, 'polls/questions_list.html', variables)
    return render(request,'polls/likes_list.html', variables)

def _custom_question_save(request,form,form2):
    question = Question.objects.create(owner=request.user,
               question_text=form.cleaned_data['question'],
               # category=form.cleaned_data['category_name'],
               poll_info=form.cleaned_data['info']
               )
    # create the choices for the question
    choice1 = question.choice_set.create(
              choice_text=form.cleaned_data['choice1'])
    choice2 = question.choice_set.create(
              choice_text=form.cleaned_data['choice2']
    )
    if form.cleaned_data['choice3'] != None:
        choice3 = question.choice_set.create(
                  choice_text=form.cleaned_data['choice3']
        )
    if form.cleaned_data['choice4'] != None:
        choice4 = question.choice_set.create(
                  choice_text=form.cleaned_data['choice4']
        )
    if form.cleaned_data['choice5'] != None:
        choice5 = question.choice_set.create(
                  choice_text=form.cleaned_data['choice5']
        )
    settings = PollSettings(
        question=question,
        who_can_respond=form2.cleaned_data['who_can_respond'],
        duration=form2.cleaned_data['duration'],
        begin_when=form2.cleaned_data['begin_when'],
        presentation=form2.cleaned_data['presentation'])

    # save question to the database
    question.save()
    settings.save()
    return question


def _instant_question_save(request,form):
    question = Question.objects.create(owner=request.user,
               question_text=form.cleaned_data['question'],
               # category=form.cleaned_data['category_name'],
               poll_info=form.cleaned_data['info']
               )
    # create the choices for the question
    choice1 = question.choice_set.create(
              choice_text=form.cleaned_data['choice1'])
    choice2 = question.choice_set.get_or_create(
              choice_text=form.cleaned_data['choice2']
    )
    if form.cleaned_data['choice3'] != None:
        choice3 = question.choice_set.create(
                  choice_text=form.cleaned_data['choice3']
        )
    if form.cleaned_data['choice4'] != None:
        choice4 = question.choice_set.create(
                  choice_text=form.cleaned_data['choice4']
        )
    if form.cleaned_data['choice5'] != None:
        choice5 = question.choice_set.create(
                  choice_text=form.cleaned_data['choice5']
        )
    settings = PollSettings(
        question=question,
        who_can_respond='anyone',
        duration='Auto',
        begin_when='now',
        presentation='voters_only')
    # save question to the database
    settings.save()
    question.save()
    # poll = ActivePoll(content_object=question)
    # poll.save()
    return question


@login_required(login_url='account:login')
def question_save(request,username):
    if request.method == 'POST':
        form = QuestionSaveForm(request.POST)
        form2 = PollSettingsForm(request.POST)
        if form.is_valid() and form2.is_valid():
            # # check if poll is instant or Special
            if form.cleaned_data['instant_poll'] != True:
                #create or get question
                question = _custom_question_save(request, form, form2)
        if form.is_valid():
            if form.cleaned_data['instant_poll'] == True:
                #create or get question
                question = _instant_question_save(request,form)
            else:
                variables = {
                    'form': form,
                    'form2':form2,
                    'enddate':'enddate'
                }
                # messages.error(request, "form not filled correctly!")
                return render(request, 'polls/question_form.html', variables)
        else:
            variables = {
                'form': form,
                'form2':form2,
                'enddate':'enddate'
            }
            # messages.error(request, "form not filled correctly")
            return render(request, 'polls/question_form.html', variables)

        messages.success(request,
          "Poll created. Thanks for using PollsPortal!"
        )
        # # create an action for creating a poll
        # action = create_action(request.user, 'created a new poll', question)
        # # launch asynchronous task
        # question_created.delay(question.id)
        # # stream framework fanout
        # from stream_framework.verbs.base import Add as AddVerb
        # manager.add_action(question, request.user, AddVerb, timezone.now())

        return HttpResponseRedirect(reverse (
         'polls:user_page', args=(request.user.username,)
        ))
    # conditional check for edit of question
    elif request.GET.__contains__('question_id'):
        question_id = int(request.GET['question_id'])
        question_text = ''
        data = {}
        try:
            options = []
            i = 1
            question = Question.objects.get(
                    id=question_id,
                    owner=request.user)
            question_text = question.question_text
            for choice in question.choice_set.all():
                data.update({'choice'+str(i):choice.choice_text})
                i += 1
        except ObjectDoesNotExist:
            pass
        data.update({'question':question_text})
        form = QuestionSaveForm(data)
        form2 = PollSettingsForm()
        variables = {
            'form':form,
            'question_id':question_id
            }
        return render(request, 'polls/question_edit_form.html', variables)
    else:
        form = QuestionSaveForm()
        form2 = PollSettingsForm()
        quiz_form = QuizForm(prefix='newquiz')
        question_form = QuizQuestionForm(prefix='quizquestions')
        ChoiceFormset = modelformset_factory(QuizChoice,
           fields=('choice_text','answer'),
           extra=2,
           can_delete=False,
           max_num=5)
        # create formset with empty query
        choice_formset =  ChoiceFormset(
            queryset=QuizChoice.objects.none(),
            prefix='choices')
    today = NOW.today()
    enddate = today + timedelta(90)
    variables = {
        'form': form,
        'form2':form2,
        'today':today,
        'quiz_form':quiz_form,
        'question_form':question_form,
        'choice_formset':choice_formset,
        'enddate':'enddate'
    }
    if request.is_ajax():
        return render(request, 'polls/question_form_ajax.html', variables)
    return render(request, 'polls/question_form.html', variables)

def question_edit(request,question_id):
    if request.method == "POST":
        form = QuestionSaveForm(request.POST)
        if form.is_valid():
            try:
                question = Question.objects.get(id=question_id,
                    owner=request.user)
            except ObjectDoesNotExist:
                message.error(request, "You aren't authorized to edit this poll")
                return HttpResponseRedirect(reverse (
                 'polls:user_page', args=(request.user.username,)
                ))
            question.question_text = form.cleaned_data['question']
            # the for loop takes care of previously saved choices
            # if new choices is < previous, delete the corresponding previous
            # choice from record otherwise save choice
            i = 1
            for choice in question.choice_set.all():
                option = 'choice'+ str(i)
                if form.cleaned_data[option] != None:
                    choice.choice_text = form.cleaned_data[option]
                    choice.save()
                elif form.cleaned_data[option] == None:
                    choice.delete()
                i += 1
            # if new choices is more than previous, create new choice if it
            # DoesNotExist
            if form.cleaned_data['choice3'] != None:
                choice3, dummy = question.choice_set.get_or_create(question=question,
                    choice_text=form.cleaned_data['choice3'])
            if form.cleaned_data['choice4'] != None:
                choice4, dummy = question.choice_set.get_or_create(question=question,
                    choice_text=form.cleaned_data['choice4'])
            if form.cleaned_data['choice5'] != None:
                choice5, dummy = question.choice_set.get_or_create(question=question,
                    choice_text=form.cleaned_data['choice5'])
            question.save()
            variables = {
                'form':form,
                'question_id':question_id
                }
            messages.success(request, "successfully edited")
            return HttpResponseRedirect(reverse (
             'polls:user_page', args=(request.user.username,)
            ))

    messages.error(request, 'Access Denied!')
    return HttpResponseRedirect(reverse (
     'polls:user_page', args=(request.user.username,)
    ))

def create_anonymous(request):
    if not request.user.is_authenticated():
        try:
            user = User.objects.get(username='AnonymousUser')
        except ObjectDoesNotExist:
            user = User.objects.create(username='AnonymousUser')
        if request.method == 'POST':
            form = QuestionSaveForm(request.POST)
            if form.is_valid():
                #create or get question
                question = Question.objects.create(owner=user,
                           question_text=form.cleaned_data['question'],
                           category=form.cleaned_data['category_name'],
                           poll_info=form.cleaned_data['info']
                           )
                # create the choices for the question
                choice1 = question.choice_set.create(
                          choice_text=form.cleaned_data['choice1'])
                choice2 = question.choice_set.create(
                          choice_text=form.cleaned_data['choice2']
                )
                if form.cleaned_data['choice3'] != None:
                    choice3 = question.choice_set.create(
                              choice_text=form.cleaned_data['choice3']
                    )
                if form.cleaned_data['choice4'] != None:
                    choice4 = question.choice_set.create(
                              choice_text=form.cleaned_data['choice4']
                    )
                if form.cleaned_data['choice5'] != None:
                    choice5 = question.choice_set.create(
                              choice_text=form.cleaned_data['choice5']
                    )

                # start vote immediately
                question.publish = timezone.now() + timedelta(hours=0.25)
                question.lock_date = timezone.now() + timedelta(days=30)
                question.save()
                # create initial settings
                settings = PollSettings(question=question)
                settings.save()
                # poll = ActivePoll(content_object=question)
                # poll.save()
                messages.success(request,
                  "Thank you for using PollsPortal. Sign up to vote and create more polls"
                )
                # create an action for creating a poll
                #create_action(request.user, 'created a new poll', question)

                return HttpResponseRedirect(reverse (
                 'index',
                ))
        else:
            form = QuestionSaveForm()
        variables = {
            'form': form,
            #'profile_pic':profile_pic
        }
        return render(request, 'polls/question_form_anonymous.html', variables)

def question_settings(request,question_id,poll_type):
    next = request.META.get('HTTP_REFERER', None) or '/'
    if poll_type.lower() == "quiz":
        question = get_object_or_404(Quiz, pk=question_id)
    elif poll_type.lower() == 'opinion':
        question = get_object_or_404(Question, pk=question_id)
    else:
        return HttpResponseRedirect(next)
    user = User.objects.get(username=request.user.username)
    if question.owner != request.user:
        messages.warning(request,
          'You are not autorized to edit this poll'
        )

        return HttpResponseRedirect(reverse(
        'index'
        ))
    # Q_settings = ContentType.objects.get_for_model(question)
    # A check if settings already exists for question
    try:
        similar_settings = PollSettings.objects.get(
                question=question
            )
    except PollSettings.DoesNotExist:
        pass
    
    if request.method == 'POST':
        form = PollSettingsForm(request.POST)
        if form.is_valid():
            # create a settings for question
            if similar_settings:
                similar_settings.who_can_respond = form.cleaned_data['who_can_respond']
                similar_settings.duration = form.cleaned_data['duration']
                similar_settings.presentation = form.cleaned_data['presentation']
                similar_settings.view_poll = form.cleaned_data['view_poll']
                similar_settings.share_poll = form.cleaned_data['share_poll']
                similar_settings.comment_on_poll = form.cleaned_data['comment_on_poll']
                similar_settings.view_result = form.cleaned_data['view_result']
                similar_settings.votes_quota = form.cleaned_data['votes_quota']
                similar_settings.end_date = form.cleaned_data['end_date']
                similar_settings.start_date = form.cleaned_data['start_date']
                similar_settings.begin_when = form.cleaned_data['begin_when']
                similar_settings.save()

            else:
                pollsettings = PollSettings(
                    question=question,
                    who_can_respond=form.cleaned_data['who_can_respond'],
                    duration=form.cleaned_data['duration'],
                    presentation=form.cleaned_data['presentation'],
                    view_poll = form.cleaned_data['view_poll'],
                    share_poll = form.cleaned_data['share_poll'],
                    comment_on_poll = form.cleaned_data['comment_on_poll'],
                    view_result = form.cleaned_data['view_result'],
                    votes_quota = form.cleaned_data['votes_quota'],
                    end_date = form.cleaned_data['end_date'],
                    start_date = form.cleaned_data['start_date'],
                    begin_when = form.cleaned_data['begin_when'],
                )
                pollsettings.save()
            messages.success(request,
              "settings saved!"
            )
            return HttpResponseRedirect(reverse (
             'polls:user_page', args=(user.username,)
            ))
        else:
            form = PollSettingsForm(request.POST)
            variables = {
                'form':form,
                'question':question,
            }
            return render(request, "polls/poll_settings.html",variables)

    else:
        if similar_settings:
            initial_data = {
                "who_can_respond": similar_settings.who_can_respond,
                'duration':similar_settings.duration,
                'presentation':similar_settings.presentation,
                'begin_when': similar_settings.begin_when,
                'view_poll': similar_settings.view_poll,
                'share_poll':similar_settings.share_poll,
                'comment_on_poll': similar_settings.comment_on_poll,
                'view_result': similar_settings.view_result,
                'end_date':similar_settings.end_date,
                'start_date': similar_settings.start_date
            }
            form = PollSettingsForm(initial=initial_data)
        else:
            form = PollSettingsForm()
    variables = {'form' : form,
            'question':question }
    if request.is_ajax():
        return render(request, "polls/poll_settings_ajax.html",variables)
    return render(request, "polls/poll_settings.html",variables)

@ajax_required
@require_POST
def suspend_poll(request,question_id):
    questionid = request.POST.get('question_id')
    poll_type = request.POST.get('poll_type')
    if questionid and poll_type:
        try:
            question = Question.objects.get(id=questionid,poll_type=poll_type)
        except Question.DoesNotExist:
            try:
                question = Quiz.objects.get(id=questionid,poll_type=poll_type)
            except Quiz.DoesNotExist:
                pass
        try:
            if question.get_status() == 'Active':
                variables = {'status':"Poll is active. Request cannot be completed!"}
            else:
                question.suspended = True
                question.save()
                variables = {'status':'ok'}
            return JsonResponse(variables)
        except:
            pass
    return JsonResponse({'status':'Not completed'})

def search_page(request):
    form=SearchForm()
    questions= []
    users=[]
    show_search_results = False
    if request.GET.__contains__('query'):
        show_search_results = True
        query=request.GET['query'].strip()
        if query:
            keywords = query.split()
            q = Q()
            for keyword in keywords:
                q1 = q & Q(question_text__icontains=keyword) # to search by question
                q2 = Q(owner__username__icontains=keyword) # To search by username
                q3 = Q(category__icontains=keyword) # To search by category
                q4 = Q(id__iexact=keyword)
                q5 = Q(first_name__icontains=keyword)
                q6 = Q(last_name__icontains=keyword)
                q7 = Q(username__icontains=keyword)
            form = SearchForm({'query':query})
            questions = Question.objects.filter(q1|q2|q3|q4)
            questions = questions.exclude(suspended=True)
            users = User.objects.filter(q5|q6|q7)
    variables = {
         'form':form,
         'questions':questions,
         'users':users,
         'show_search_results':show_search_results,
         'show_user':True,
         'show_category':True,
         "view_result":True,
    }
    if request.GET.__contains__('ajax'):
        if len(questions)> 8:
            more_view = True
        else: more_view = False
        questions = questions[:8]
        return render(request,'polls/search_list_ajax.html',
        {
             'form':form,
             'questions':questions,
             'users':users,
             'show_search_results':show_search_results,
             'show_user':True,
             'show_category':True,
             'more_view': more_view
        })
    return render(request, 'polls/search.html', variables)

@ajax_required
@require_POST
def like(request, question_id):
    """ increase the no of likes and add user to list of users that liked
        same question or otherwise
    """
    action = request.POST.get('action')
    poll_type = request.POST.get('poll_type')
    questionid = request.POST.get('question_id')
    if questionid and action:
        this_month = NOW.month
        year = NOW.year
        if poll_type == "Opinion":
            try:
                question = Question.objects.get(id=questionid)
                if request.user not in question.user_liked.all():
                    question.user_liked.add(request.user)
                    # increase the number of likes in redis db for the present
                    # month
                    activities_count = r.zincrby('total_activities{}-{}'.format(this_month,year),'likes',1)
                    # increase the likes for the user in present month
                    user_activities_this_month = r.zincrby(
                        'user_{}_activity{}-{}'.format(request.user.id,this_month,year),'likes',1)
                    # action = create_action(request.user, 'liked', question)
                    from .verbs import Like as LikeVerb
                    manager.add_action(question, request.user, LikeVerb, timezone.now())
                    notify.add_notify_action(question, request.user, LikeVerb)

                else:
                    question.user_liked.remove(request.user)

                    from .verbs import Like as LikeVerb
                    manager.remove_action(question, request.user, LikeVerb, timezone.now())
                return JsonResponse({'status':'200'})
            except:
                pass
        elif poll_type == "Quiz":
            try:
                quiz = Quiz.objects.get(id=questionid)
                if action == 'like':
                    quiz.user_liked.add(request.user)
                    # increase the number of likes in redis db for the present
                    # month
                    activities_count = r.zincrby('total_activities{}-{}'.format(this_month,year),'likes',1)
                    # increase the likes for the user in present month
                    user_activities_this_month = r.zincrby('user_{}_activity{}-{}'.format(request.user.id,this_month,year),'likes',1)
                    # # create action when users liked
                    from .verbs import Like as LikeVerb
                    manager.add_action(quiz, request.user, LikeVerb, timezone.now())
                    notify.add_notify_action(quiz, request.user, LikeVerb)

                else:
                    quiz.user_liked.remove(request.user)

                return JsonResponse({'status':'200'})
            except:
                pass
    return JsonResponse({'status':'ko'})

def share_type(request):
    """
    Get type of share either group or individual.
    """
    pass

def share(request,question_id,poll_type):
    """ increase the no of shares and add user to list of users that shared
        same question or otherwise.  If request contains user_ids, then
        notify these users about the invite to vote
    """
    next = request.META.get('HTTP_REFERER', None) or '/'
    if request.method == 'POST':
        invite_type = request.POST['invite_type']
        # poll_type = request.POST['poll_type']
        if invite_type:
            this_month = NOW.month
            year = NOW.year
            try:
                if poll_type == "Quiz":
                    question = Quiz.objects.get(id=question_id)
                else:
                    question = Question.objects.get(id=question_id)
                def add_share(invite_type,data):
                    inviteeslst = []
                    for each in data:
                        if each.startswith(invite_type):
                            inviteeslst.append(data[each])
                    if inviteeslst == []:
                        message = "No {} selected. Try again!".format(invite_type)
                        return None
                    if invite_type == 'group':
                        try:
                            invited_groups = Group.objects.in_bulk(inviteeslst)
                            invited_groups = list(invited_groups.values())
                            group_members = []
                            for group in invited_groups:
                                # extract members from each group
                                group_members += group.members.all()
                            invite = make_unique(group_members)
                            invitees = list(invite)
                            copied = invitees.copy()
                        except:
                            message = "something went wrong! Try again!!"
                            return (False,None,message)
                    else:
                        invitees = User.objects.in_bulk(inviteeslst)
                        invitees = list(invitees.values())
                        copied = invitees.copy()

                    for invitee in copied:
                        # if user already voted remove from list of invite
                        try:
                            if invitee in question.users_voted.all():
                                invitees.remove(invitee)
                        except:
                            if invitee in question.quiz_takers.all():
                                invitees.remove(invitee)
                    question.users_share.add(request.user)
                    message = "invitation sent"
                    return (True, invitees, message)
                state, invitees, message = add_share(invite_type,request.POST)
                if state:
                    # update number of shares received
                    if poll_type == "Quiz":
                        total_shares = r.incr('quiz:{}:shares'.format(
                            question.id))
                        # increment shares ranking by 1
                        r.zincrby('quiz_ranking', question.id, 1)
                        # send quiz invite through email
                    else:
                        total_shares = r.incr('question:{}:shares'.format(
                            question.id))
                        # increment shares ranking by 1
                        r.zincrby('question_ranking', question.id, 1)

                    # ADD to global number of shares
                    activities_count = r.zincrby(
                        'total_activities{}-{}'.format(this_month,year),'shares',1)
                    # add to user shares for this month
                    user_activities_this_month = r.zincrby(
                        'user_{}_activity{}-{}'.format(request.user.id,this_month,year),'shares',1)
                    # send invite notification
                    from .verbs import Share as ShareVerb
                    notify.send_invite(question,request.user, ShareVerb, invitees)
                    manager.add_action(question, request.user, ShareVerb, timezone.now())

                    messages.success(request,message)
                    if request.is_ajax():
                        return HttpResponse(message)
                    return HttpResponseRedirect('/')
                else:
                    messages.warning(request, message)
                    if request.is_ajax():
                        return HttpResponse(message)
                    return HttpResponseRedirect(next)
            except:
                messages.success(request, "Something not right! we'll fix it!")
                pass
        if request.is_ajax():
            return HttpResponse("Something not right! we'll fix it!")
        return HttpResponseRedirect(next)

    if poll_type == 'Quiz':
        question = get_object_or_404(Quiz, id=question_id)
    else:
        question = get_object_or_404(Question, id=question_id)
    if question.get_status() == 'Draft' or question.get_status() == 'Pending':
        messages.error(request, "Poll is not Active!, you can't share an inactive poll")
        return HttpResponseRedirect(next)
    # groups user created
    grp = Q(owner=request.user)
    # groups user belong to
    grp2 = Q(members__username=request.user.username)
    grps = Group.objects.filter(grp|grp2) # for group invite
    groups = make_unique(grps) # make the list unique
    # friends that request.user is following incase of friend invite
    friend_ids = request.user.following.values_list('id',flat=True)
    follower_ids = request.user.followers.values_list('id',flat=True)
    friends = User.objects.in_bulk(friend_ids)
    followers = User.objects.in_bulk(follower_ids)
    copied = friends.copy()
    # iterate to remove duplication in case a user appears in both
    for each in copied:
        if each in followers.keys():
            del friends[each]
    variables = {
        'question': question,
        'groups': groups,
        'friends': friends.values(), # friends ={1:<User>}
        'followers': followers.values(),
    }
    return render(request, 'polls/share_poll.html', variables)

def share_ranking(request):
    """
    method get questions in a sorted set according to number of shares
    """
    if request.GET.__contains__('question_id'):
        question_id = request.GET['question_id']
        poll_type = request.GET['poll_type']
        if poll_type == "Quiz":
            shares = r.get('quiz:{}:shares'.format(int(question_id)))
        else:
            shares = r.get('question:{}:shares'.format(int(question_id)))
        if shares is None:
            shares = 0
        return JsonResponse({'total_shares':int(shares)})
    # get question ranking dictionary from lowest 0 to highest -1 with respective scores
    # type return is [(value,score)]
    question_ranking = r.zrange('share_ranking', 0, -1, desc=True, withscores=True)[:25]

    def enrich_share_ranking(question_ranking):
        # get question ids
        question_ranking_ids = [int(each[0]) for each in question_ranking]
        question_dict = Question.objects.in_bulk(question_ranking_ids)
        # # get most shared sorted
        most_shared = []
        # most_shared = question_ranking.sort(key=lambda x: question_ranking_ids.index(x.id))
        for question,score in question_ranking:
            q = question_dict.get(int(question))
            most_shared.append((q,int(score)))
        return most_shared
    variables = {
        'most_shared': enrich_share_ranking(question_ranking)
    }
    return render(request, 'polls/most_shared.html', variables)

def quiz_detail(request,quiz_id,slug):
    open_answers = []
    choice_answers = []
    time_allowed =''
    number_of_attempt = 0
    try:
        quiz = Quiz.objects.prefetch_related('quiz_takers').get(id=quiz_id,slug=slug)
    except (KeyError, Quiz.DoesNotExist) as e:
        raise Http404
    if request.user in quiz.quiz_takers.all():
        try:
            quiz_taker = QuizTakerScore.objects.get(quiz=quiz,quiz_taker=request.user)
            if quiz_taker.number_of_attempt >= quiz.attempt:
                open_answers = quiz_taker.open_answers.prefetch_related('quiz_question')
                choice_answers = quiz_taker.choice_answers.all()
                show_result = True
                messages.warning(request,
                    "You reached the maximum number of allowed attempt"
                )
            else:
                show_result = False
            # update number_of_attempt if quiz_taker exists otherwise pass
            number_of_attempt = quiz_taker.number_of_attempt
        except (KeyError, QuizTakerScore.DoesNotExist) as e:
            show_result = False
    else:
        show_result = False
    if request.is_ajax():
        if request.method == 'GET':
            if quiz.mode == "timed":
                time_allowed = timezone.localtime() + timedelta(minutes=quiz.quiz_duration)
                quiz_taker, dummy = QuizTakerScore.objects.get_or_create(quiz=quiz,quiz_taker=request.user)
                if quiz_taker.number_of_attempt < quiz.attempt:
                    quiz_taker.number_of_attempt += 1
                    quiz_taker.save()
                return HttpResponse(time_allowed)
    # get number of shares received
    total_shares = r.get('quiz:{}:shares'.format(quiz.id))
    variables = {
        "quiz": quiz,
        'show_result': show_result,
        'open_answers': open_answers,
        'choice_answers': choice_answers,
        'time_allowed': time_allowed,
        'number_of_attempt': number_of_attempt,
        'total_shares': total_shares
    }
    return render(request,"polls/detail.html",variables)

def quiz_vote(request,quiz_id,slug):
    next = request.META.get('HTTP_REFERER', None) or '/'
    quiz = get_object_or_404(Quiz, pk=quiz_id, slug=slug)
    questions = quiz.quizquestion_set.all().prefetch_related('participants','open_answer')
    total_questions = len(questions)
    score = 0
    unanswered_question = []
    choice_answers = []
    no_answer = []
    open_answers = []
    for question in questions:
        if question.question_type == "choice":
            name = 'question_choice{}'.format(question.id)
            try:
                submitted_answer = question.quizchoice_set.get(pk=request.POST[name])
                if submitted_answer.answer:
                    score += 1
                choice_answers.append(submitted_answer)
            except (KeyError, QuizChoice.DoesNotExist):
                unanswered_question.append(question)

        else:
            name = 'question_answer{}'.format(question.id)
            try:
                correct_answer = question.open_answer
                if correct_answer != None:
                    submitted_answer = QuizQuestion.objects.get(
                                         open_answer__answer__iexact=request.POST[name])
                    if submitted_answer == question:
                        score += 1
                    # open_answers.append((question, submitted_answer.open_answer))
                else:
                    no_answer.append(question)
                open_answers.append((question, request.POST[name]))
            except (KeyError, QuizQuestion.DoesNotExist):
                # answer not correct
                open_answers.append((question, request.POST[name]))
                pass
        # add request.user to list of those that answered the question
        question.participants.add(request.user)

    quiz_score, dummy = QuizTakerScore.objects.get_or_create(
        quiz=quiz,
        quiz_taker=request.user
    )
    if quiz_score.number_of_attempt <= quiz.attempt:
        quiz_score.score = score
        if quiz.mode == 'notTimed':
            quiz_score.number_of_attempt += 1
        quiz_score.save()
        for ans in choice_answers:
            quiz_score.choice_answers.add(ans)
        for question, ans in open_answers:
            answer = SubmittedOpenAnswer.objects.create(
                quiz_question=question,
                answerer=request.user,
                submitted_answer=ans
                )
            quiz_score.open_answers.add(answer)
        # checks if quiz result should be shown according to owner's settings
        for settings in quiz.settings.all():
            if settings.view_result != 'none':
                show_result = True
                messages.success(request,
                    "Number of attempt: {}. \nScore {}/{}".format(
                        quiz_score.number_of_attempt, quiz_score.score,total_questions))
            else:
                show_result = False
                messages.success(request,
                    "Your score has been recorded. Thanks for taking the test!")
        variables = {
            'show_result': show_result,
            'quiz': quiz,
            'number_of_attempt':quiz_score.number_of_attempt,
            'choice_answers': quiz_score.choice_answers.all(),
            'open_answers': quiz_score.open_answers.prefetch_related('quiz_question')
        }
        if request.is_ajax():
            return render(request, "polls/quiz_result_ajax.html", variables)
        return render(request, "polls/detail.html", variables)
    messages.warning(request,"You've reached the maximum number of allowed attempt")
    return HttpResponseRedirect(next)

def pre_create_quiz(request,username):
    """
    """
    next = request.META.get('HTTP_REFERER', None) or '/'
    # create a quiz form for new quiz
    quiz_form = QuizForm(prefix='newquiz')
    variables = {
        'quiz_form':quiz_form,
    }
    if request.method == 'POST':
        quiz_form = QuizForm(request.POST, prefix='newquiz')
        if quiz_form.is_valid():
            try:
                quiz, dummy = Quiz.objects.get_or_create(
                    owner=request.user,
                    title=request.POST['newquiz-title']
                )
                quiz.mode = request.POST['newquiz-mode']
                quiz.attempt = request.POST['newquiz-attempt']
                # check if quiz_duration exists
                name = 'newquiz-quiz_duration'
                if name in request.POST and request.POST['newquiz-mode'] == 'timed':
                    duration = request.POST[name]
                    quiz.quiz_duration = duration
                elif (name not in request.POST) and request.POST['newquiz-mode'] == 'timed':
                    if quiz.quiz_duration == None:
                        quiz.quiz_duration = 1 # set default time to 1 minute
                quiz.save()
            except IntegrityError:
                # append a number to title if already exists
                quizzes = Quiz.objects.filter(title=request.POST['newquiz-title']).count()
                new_title = request.POST['newquiz-title'] +'-'+ str(quizzes + 1)
                quiz, dummy = Quiz.objects.get_or_create(
                    owner=request.user,
                    title=new_title
                )
                quiz.mode = request.POST['newquiz-mode']
                # check if quiz_duration exists
                name = 'newquiz-quiz_duration'
                if name in request.POST and request.POST['newquiz-mode'] == 'timed':
                    duration = request.POST[name]
                    quiz.quiz_duration = duration
                elif (name not in request.POST) and request.POST['newquiz-mode'] == 'timed':
                    if quiz.quiz_duration == None:
                        quiz.quiz_duration = 1 # set default time to 1 minute
                quiz.save()
        else:
            return HttpResponseRedirect(next)
        QuizQuestionFormset = modelformset_factory(QuizQuestion,
            form=QuizQuestionForm,
            fields=('quiz_question_text','question_type'),
            can_delete=False,
            )
        ChoiceFormset = inlineformset_factory(QuizQuestion, QuizChoice,
           form=QuizChoiceForm,
           fields=('choice_text','answer'),
           extra=2,
           can_delete=False,
           max_num=5)
        # create formset
        question_formset = QuizQuestionFormset(queryset=QuizQuestion.objects.none(),
            prefix='quizquestions')
        questions = quiz.quizquestion_set.all()
        formset = []
        choice_formset =  ChoiceFormset(
            queryset=QuizChoice.objects.none(),
            prefix='choices'
            )
        formset.append((question_formset,choice_formset))
        variables = {
            'quiz':quiz,
            'quiz_questions':questions,
            'question_formset':question_formset,
            'formset':formset,
        }
        return render(request,
            "polls/quiz_form.html",
            variables
            )
    if request.GET.__contains__('quiz_edit'):
        quiz_id = request.GET['quiz_edit']
        quiz = get_object_or_404(Quiz, id=quiz_id)
        if quiz.owner != request.user:
            messages.warning(request, "Access Denied! You cannot modify other people's poll")
            return HttpResponseRedirect(next)
        quiz_form = QuizForm(instance=quiz, prefix="newquiz")
        QuizQuestionFormset = inlineformset_factory(Quiz, QuizQuestion,
            form=QuizQuestionForm,
            fields=('quiz_question_text','question_type'),
            can_delete=True,
            extra=0
            )
        ChoiceFormset = inlineformset_factory(QuizQuestion, QuizChoice,
           form=QuizChoiceForm,
           fields=('choice_text','answer'),
           can_delete=True,
           extra=1,
           max_num=5)
        # create formset
        question_formset = QuizQuestionFormset(instance=quiz, prefix='quizquestions')
        questions = quiz.quizquestion_set.all()
        formset = []
        if len(questions) > 0:
            for question,form in zip(questions,question_formset):
                choice_formset =  ChoiceFormset(
                    instance=question,
                    prefix='choices{}'.format(question.id)
                    )
                formset.append((question,form,choice_formset))
        else:
            choice_formset =  ChoiceFormset(
                queryset=QuizChoice.objects.none(),
                prefix='choices'
                )
            # if no question has ever been set and user want to edit
            if len(question_formset) == 0:
                QuizQuestionFormset = inlineformset_factory(Quiz, QuizQuestion,
                    form=QuizQuestionForm,
                    fields=('quiz_question_text','question_type'),
                    can_delete=True,
                    extra=1
                    )
                question_formset = QuizQuestionFormset(instance=quiz, prefix='quizquestions')
            formset.append(('',question_formset[0],choice_formset))
        variables = {
            'edit': True,
            'quiz':quiz,
            'question_formset':question_formset,
            'formset':formset,
            'quiz_edit_form':quiz_form
        }
        return render(request,"polls/quiz_form.html", variables)
    return render(request,"polls/quiz_form_ajax.html", variables)

def create_new_quiz(request,quiz_id):
    """
    """
    quiz = get_object_or_404(Quiz,id=quiz_id)
    QuizQuestionFormset = modelformset_factory(
        QuizQuestion,
        form=QuizQuestionForm,
        fields=('quiz_question_text','question_type'),
        can_delete=False,
    )
    ChoiceFormset = inlineformset_factory(QuizQuestion, QuizChoice,
        form=QuizChoiceForm,
        fields=('choice_text','answer'),
        can_delete=False,
        max_num=5)

    if request.method == 'POST':
        question_formset = QuizQuestionFormset(request.POST, prefix='quizquestions')
        errors = []
        if question_formset.is_valid():
            inst = question_formset.save(commit=False)
            for question in inst:
                question_type = question.question_type
                instance, dummy = QuizQuestion.objects.get_or_create(
                    quiz=quiz,
                    quiz_question_text=question.quiz_question_text
                )
            if 'quizquestions-open_answer' in request.POST:
                answer = request.POST['quizquestions-open_answer']
                open_answer = OpenAnswer(answer=answer)
                open_answer.save()
                instance.question_type = question_type
                instance.open_answer = open_answer
                instance.save()
            else:
                choice_formset =  ChoiceFormset(request.POST,
                    instance=instance,
                    prefix='choices'
                    )
                try:
                    if choice_formset.is_valid():
                        choices = choice_formset.save()
                except ValidationError:
                    errors.append(instance.quiz_question_text)

            if request.is_ajax():
                variables = {
                  'quiz': quiz,
                }
                return render(request,
                    "polls/quiz_details_ajax.html",
                    variables
                    )
        if errors != []:
            messages.success(request,
                "check errors with following questions: {}".format(",".join(errors))
                )
        return HttpResponseRedirect("/")

def edit_quiz(request,quiz_id):
    """
    """
    next = request.META.get('HTTP_REFERER', None) or '/'
    quiz = get_object_or_404(Quiz,id=quiz_id)
    if quiz.owner != request.user:
        messages.warning(request, "Access Denied! You cannot modify other people's poll")
        return HttpResponseRedirect(next)
    QuizQuestionFormset = inlineformset_factory(
        Quiz, QuizQuestion,
        form=QuizQuestionForm,
        fields=('quiz_question_text','question_type'),
        can_delete=True,
    )

    ChoiceFormset = inlineformset_factory(QuizQuestion, QuizChoice,
        form=QuizChoiceForm,
        fields=('choice_text','answer'),
        can_delete=True,
        extra=1,
        max_num=5)
    if request.method == 'POST':
        quiz_edit_form = QuizForm(request.POST, instance=quiz, prefix="newquiz")
        if quiz_edit_form.is_valid():
            quiz_edit_form.save()
            # check if quiz_duration exists
            name = 'newquiz-quiz_duration'
            if name in request.POST:
                duration = request.POST[name]
                quiz.quiz_duration = duration
                quiz.save()
        else:
            return HttpResponseRedirect(next)
        question_formset = QuizQuestionFormset(request.POST, instance=quiz, prefix='quizquestions')
        errors = []
        if question_formset.is_valid():
            question_formset.save()
            # use the instances saved in database, in case bound data doesnt change
            # these ensures that each changed choic object is saved
            instances = quiz.quizquestion_set.all()
            for instance in instances:
                if instance.question_type == "choice":
                    if instance.open_answer != None:
                        instance.open_answer = None
                    instance.save()
                else:
                    # check if open_answer exists
                    name = 'quizquestions-open_answer{}'.format(instance.id)
                    if name in request.POST:
                        answer = request.POST[name]
                        open_answer = OpenAnswer(answer=answer)
                        open_answer.save()
                        instance.open_answer = open_answer
                        instance.save()
                    else:
                        if instance.open_answer == None:
                            # user must provide and answer
                            messages.warning(request,
                                "you didn't provide an answer for '{}' ".format(instance))
                            return HttpResponseRedirect(next)
                try:
                    choice_formset =  ChoiceFormset(request.POST,
                        instance=instance,
                        prefix='choices{}'.format(instance.id)
                        )
                    if choice_formset.is_valid():
                        choices = choice_formset.save()
                except ValidationError:
                    choice_formset =  ChoiceFormset(request.POST,
                        instance=instance,
                        prefix='choices'
                        )
                    try:
                        if choice_formset.is_valid():
                            choices = choice_formset.save()
                    except:
                        errors.append(instance.quiz_question_text)

            if errors != []:
                messages.error(request,
                    "check errors with following questions: {}".format(",".join(errors))
                    )
            else:
                messages.success(request, "changes saved")
        if request.is_ajax():
            return render(request,
                "polls/quiz_edit_result_ajax.html",
                new_formset(quiz)
                )
        return HttpResponseRedirect("/")
    return HttpResponseRedirect(next)

@ajax_required
def edit_quiz_question(request,question_id):
    """
    """
    question = QuizQuestion.objects.select_related('quiz').get(id=question_id)
    question_form = QuizQuestionForm(instance=question)
    ChoiceFormset = inlineformset_factory(QuizQuestion, QuizChoice,
        form=QuizChoiceForm,
        fields=('choice_text','answer'),
        extra=1,
        can_delete=True,
        max_num=5)
    quiz = question.quiz
    if quiz.owner != request.user:
        return HttpResponse("Access Denied! You're not authorized to edit this quiz")
    if request.method == 'POST':
        question_form = QuizQuestionForm(request.POST,instance=question)
        if question_form.is_valid():
            instance = question_form.save()
            if instance.question_type == "choice":
                if instance.open_answer != None:
                    instance.open_answer = None
                instance.save()
            else:
                # check if open_answer exists
                name = 'quizquestions-open_answer{}'.format(instance.id)
                if name in request.POST:
                    answer = request.POST[name]
                    open_answer = OpenAnswer(answer=answer)
                    open_answer.save()
                    instance.open_answer = open_answer
                    instance.save()
                else:
                    if instance.open_answer == None:
                        # user must provide and answer
                        messages.warning(request,
                            "you didn't provide an answer for '{}' ".format(instance))
                        return HttpResponseRedirect(next)
            choice_formset = ChoiceFormset(request.POST, instance=instance,
                prefix="choices{}".format(question.id))
            if choice_formset.is_valid():
                instances = choice_formset.save()
                variables = {
                    'quiz': question.quiz,
                }
                return render(request,
                    "polls/quiz_details_ajax.html",
                    variables
                    )
    choice_formset = ChoiceFormset(instance=question, prefix="choices{}".format(question.id))
    variables = {
        'quiz': question.quiz,
        'quiz_question': question,
        'form': question_form,
        'choice_formset': choice_formset
    }
    return render(request, "polls/quiz_question_single_form.html", variables)


def friends_page(request,username):
    user = get_object_or_404(User, username=username)

    following = \
        [friendship.to_friend for friendship in user.friend_set.all()]
    # This list questions by users that exists in friends above
    q1 = Q(owner__in=following)
    followers = \
        [friendship.from_friend for friendship in user.to_friend_set.all()]
    # This list questions by users that exists in friends above
    q2 = Q(owner__in=followers)
    questions = Question.objects.filter(q1 | q2).order_by('-id')[:10]
    variables = {
        'username': username,
        'following': following,
        'followers': followers,
        'questions': questions,
        'show_user': True,
        'show_category': True,
    }
    if request.is_ajax():
        return render(request, 'polls/friends_page_ajax.html', variables)
    return render(request, 'polls/friends_page.html', variables)


@login_required(login_url='account:login')
def friend_follow(request):

    if request.is_ajax():
        action = request.POST.get('action')
        user_id = request.POST.get('user_id')
        if user_id and action:
            try:
                user = User.objects.get(id=user_id)
                if action == 'follow':
                    friendship = Friendship(
                       from_friend=request.user, to_friend=user)
                    friendship.save()
                    # # create an action after adding a friend
                    # action = create_action(request.user, 'now follows', user)
                    # from .verbs import FollowVerb
                    # manager.add_action(friendship, request.user, FollowVerb, timezone.now())
                    # notify.add_notify_action(friendship, request.user, FollowVerb)
                    # # notify by email
                    # new_follower.delay(user, request.user)
                else:
                    Friendship.objects.filter(
                               from_friend=request.user,
                               to_friend = user).delete()
                return JsonResponse({'status':'ok'})
            except User.DoesNotExist:
                return JsonResponse({'status':'ko'})
        return JsonResponse({'status':'ko'})

    if request.GET.__contains__('username'):
        try:
            user = get_object_or_404(User, username=request.GET['username'])
            friend = Friendship.objects.filter(
                       from_friend=request.user,
                       to_friend = user)
            if friend:
                Friendship.objects.filter(
                           from_friend=request.user,
                           to_friend = user).delete()
                messages.success(request,
                  'You now unfollow %s.' % user.username
                )
            else:
                friendship = Friendship(
                   from_friend=request.user, to_friend=user)
                friendship.save()
                messages.success(request,
                  'You now follow %s.' % user.username
                )
            # # create an action after adding a friend
            # action = create_action(request.user, 'now follows', user)
            # from .verbs import FollowVerb
            # manager.add_action(friendship, request.user, FollowVerb, timezone.now())
            # notify.add_notify_action(friendship, request.user, FollowVerb)
            # # notify by email
            # new_follower.delay(user, request.user)
        except:
            messages.info(request,
              "Sorry the request could not be processed"
            )
        return HttpResponseRedirect(reverse (
         'polls:user_page', args=(user.username,)
        ))
    else:
        raise Http404

@login_required(login_url='account:login')
def friend_invite(request):

    if request.method == 'POST':
        form = FriendInviteForm(request.POST)
        if form.is_valid():
            invitation = Invitation(
              name=form.cleaned_data['name'],
              email=form.cleaned_data['email'],
              code = User.objects.make_random_password(20),
              sender = request.user
            )
            invitation.save()
            # try:
            invitation_mail.delay(
                invitation.code,
                request.user.id,
                form.cleaned_data['email'],
                form.cleaned_data['name'])
            # invitation.send()
            messages.success(request,
              "An invitation email was sent to:  %s" % (invitation.email)
            )
            # except:
            #     messages.warning(request,
            #       'There was an error while sending the invitation.'
            #     )
            return HttpResponseRedirect(reverse (
             'polls:friend_invite',
            ))
    else:
        form = FriendInviteForm()
    variables = {
      'form': form,
      #'profile_pic':profile_pic
    }
    return render(request, 'polls/friend_invite.html', variables)

def friend_accept(request,code):
    invitation = get_object_or_404(Invitation, code__exact=code)
    # Stores the ID of the object in the user's session
    request.session['invitation'] = invitation.id
    return HttpResponseRedirect(reverse (
     'account:register'
    ))

@login_required(login_url='account:login')
def send_bulk(request):
    if not request.user.is_superuser:
        return HttpResponseRedirect(reverse (
         'polls:friend_invite'
        ))
    if request.method == 'POST':
        form = SendMailForm(request.POST)
        if form.is_valid():
            messages = []
            subject = form.cleaned_data['subject']
            template = loader.get_template("polls/mail/broadcast_message.html")
            #get email of each user and send mail one by one
            users = User.objects.exclude(email="")
            for user in users.exclude(username='AnonymousUser'):
                email = user.email
                context = Context({
                   'name' : user.username,
                   'protocol' : "https",
                   'domain':"www.pollsportal.com",
                })
                ind_message = template.render(context)
                msg = EmailMessage(subject, ind_message,'PollsPortal Info <info@pollsportal.com>',[email])
                msg.content_subtype = 'html' #present the mail in 'text/html'
                msg.send()

            return HttpResponseRedirect(reverse (
             'index'
            ))
        else:
            messages.warning(request,
              "Please, fill in the appropriate values correctly!"
            )
    else:
        form = SendMailForm()
    return render(request, 'polls/bulk_mail_form.html',
             {

              'form': form
             })

@login_required(login_url='account:login')
def one_message(request):
    if not request.user.is_superuser:
        return HttpResponseRedirect(reverse (
         'polls:friend_invite'
        ))
    if request.method == 'POST':
        form = OneMessageForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            template = loader.get_template("polls/mail/broadcast_message.html")
            email = form.cleaned_data['email']
            context = Context({
               'name' : form.cleaned_data['name'],
               'protocol' : "https",
               'domain':"www.pollsportal.com",
            })
            message = template.render(context)
            msg = EmailMessage(subject, message,'Ahmad Adelaja <ahmad.adelaja@pollsportal.com>',[email])
            msg.content_subtype = 'html' #present the mail in 'text/html'
            msg.send()

            return HttpResponseRedirect(reverse (
             'index'
            ))
        else:
            messages.warning(request,
              "Please, fill in the appropriate values correctly!"
            )
    else:
        form = OneMessageForm()
    return render(request, 'polls/message_form.html',
             {
              'form': form
             })

# FEEDS SECTION
@login_required(login_url='account:login')
def feeds(request):
    user = get_object_or_404(User, username=request.user.username)
    feed = manager.get_feeds(request.user.id)['normal']
    activities = list(feed[:25])
    variables = {
        'feed_actions': enrich_activities(activities),
    }
    return render(request, "polls/feeds/normal_feeds.html", variables)

def enrich_activities(activities):
    '''
    Load the models attached to these activities
    (Normally this would hit a caching layer like memcached or redis)
    '''
    action_ids = [a.object_id for a in activities]
    actor_ids = [a.actor_id for a in activities]
    action_dict = ActivePoll.objects.in_bulk(action_ids)
    actor_dict = User.objects.in_bulk(actor_ids)
    for a in activities:
        a.action1 = action_dict.get(a.object_id)
        if a.action1.poll_type == 'Opinion':
            a.action = a.action1.single_poll
        else:
            a.action = a.action1.quiz_poll
        a.actor = actor_dict.get(a.actor_id)
        if a.verb.id == 9:
            Follow = Friendship.objects.filter(from_friend=a.actor,to_friend_id=a.object_id)
            if len(Follow) != 0:  # if the friendship exists
                a.following = Follow[0]
            else:  # if friendship not exists (i.e unfollow)
                a.following = None
    return activities

def enrich_aggregated_activities(aggregated_activities):
    '''
    Load the models attached to these aggregated activities
    (Normally this would hit a caching layer like memcached or redis)
    '''
    action_ids = []
    actors_ids = []
    for aggregated_activity in aggregated_activities:
        for activity in aggregated_activity.activities:
            action_ids.append(activity.object_id)
            actors_ids.append(activity.actor_id)

    action_dict = ActivePoll.objects.in_bulk(action_ids)
    actors_dicts = User.objects.in_bulk(actors_ids)
    for aggregated_activity in aggregated_activities:
        performer_per_group_ids = []

        for activity in aggregated_activity.activities:
            if activity.verb.id == 9:
                Follow = Friendship.objects.filter(
                    from_friend=activity.actor_id,to_friend_id=activity.object_id
                    )
                if len(Follow) != 0:  # if the friendship exists
                    activity.following = Follow[0]
                else:  # if friendship not exists (i.e unfollow)
                    activity.following = None
            list_of_actors = aggregated_activity.actor_ids
            activity.action1 = action_dict.get(activity.object_id)
            try:
                if activity.action1.poll_type == 'Opinion':
                    activity.action = activity.action1.single_poll
                else:
                    activity.action = activity.action1.quiz_poll
            except AttributeError as e:
                return
            activity.performer = actors_dicts.get(activity.actor_id)
            activity.performer1 = actors_dicts.get(list_of_actors[0])
            if len(list_of_actors) >= 2:
                activity.performer2 = actors_dicts.get(list_of_actors[1])
            if len(list_of_actors) >= 3:
                activity.performer3 = actors_dicts.get(list_of_actors[2])
            if aggregated_activity.actor_count >= 2 and len(aggregated_activity.object_ids) >= 2:
                performer_per_group_ids.append(activity.actor_id)
                performer_objs = User.objects.in_bulk(list_of_actors)
                activity.performers_obj_list = list(performer_objs.values())
                activity.performer_grp = performer_per_group_ids

    return (aggregated_activities)

@login_required(login_url='account:login')
def mark_as_seen(request):
    """
    mark activity as seen or read and remove from notification feed if read
    """
    activities_ids = request.GET['action_ids']
    # create a list of ids if GET ids is > 1
    if activities_ids.startswith("["):
        act = activities_ids.strip("]")
        act = act.strip("[")
        act_ids = act.split(",")
        activities_ids=[]
        for each in act_ids:
            activities_ids.append(int(each))
    else:
        activity_id = int(activities_ids)
        activities_ids = [activity_id]
    # check if request mark as read or seen only
    if request.GET['mark_read'] == 'True':
        unread = True
    else:
        unread = None
    notify.mark_as_seen_or_read(request.user,activities_ids,unread)
    return HttpResponse("marked")

@login_required(login_url='account:login')
def notification(request):
    """
    A notification request that gets feeds for request.user
    Enrich the feeds generated for display
    """
    user = get_object_or_404(User, username=request.user.username)
    feeds = MyNotificationFeed(user)
    notification_messages = list(feeds[:35])
    messages = notification_messages[:10]
    unseen_messages = feeds.count_unseen()
    # # notify feed owner of changes in realtime
    # from channels import Group
    # from json import dumps
    # Group("notify-%s"%request.user.id).send(
    #     {'text': dumps(unseen_messages)})
    if request.is_ajax():
        if unseen_messages > 10 and unseen_messages < 35:
            messages = notification_messages[:unseen_messages]
        variables = {
            'unseen_messages': unseen_messages,
            'short_notification_messages':enrich_aggregated_activities(messages),}
        # if request.GET.__contains__('message'):
        #     return JsonResponse({'unseen_count':unseen_messages})
        return render(
                    request,
                    "polls/feeds/short_notification_messages.html",
                    variables)
    feeds.mark_all() # mark all as seen
    unseen_messages = feeds.count_unseen()
    variables = {
        'unseen_messages': unseen_messages,
        'aggregated_messages': enrich_aggregated_activities(notification_messages),
        }
    return render(request, "polls/feeds/notification_messages.html", variables)

def error_404(request):
    variables = {
        "head_title": "Page Not Found"
    }
    return render(request,'polls/error_404.html', variables)

def error_500(request):
    variables = {
        "head_title": "Uh! that's bad! $500"
    }
    return render(request,'polls/error_500.html', variables)

def vote_cookie_handler(request, response, question_id):
        """
        set the cookie for anonymous visitor to vote
        """
        from user_agents import parse  # using user_agents to parse strings
        ip = str(request.META['REMOTE_ADDR']) # get the ip address of the client
        user_agent = str(request.META['HTTP_USER_AGENT']) # get user_agent strings
        ua_string = parse(user_agent)
        os_info = ua_string.os.family + ua_string.os.version_string
        browser_info = ua_string.browser.family + ua_string.browser.version_string
        device_info = ua_string.device.family #+ ua_string.device.brand + ua_string.device.model
        print(device_info)
        anony_info = os_info + browser_info + device_info + ip
        response.set_signed_cookie('anonytoken%s'%question_id,anony_info)

def process_anonymous_vote(request, question, selected_choice):
    if question.pollsettings.who_can_respond == "anyone":
        # check for existence of anonymous voting cookie
        try:
            request.get_signed_cookie('anonytoken%s'%question.id)
            print('cookie present')
        except KeyError:
            # process vote if cookie never existed
            selected_choice.votes = F('votes') + 1  # allow for database to do the arithmetic
            question.anonymous_votes = F('anonymous_votes') + 1
            question.total_votes = F('total_votes') + 1
            selected_choice.save()
            question.save()
            question.refresh_from_db()
            selected_choice.refresh_from_db()
            # get the response object to allow for later adding of cookie
            variables = {
                'question': question,
                'action':'success',
            }
            response = render(request, 'polls/vote_thank.html',variables)
            # add cookie to the response after successful voting
            vote_cookie_handler(request,response,question.id)
            print('cookie not present')
            return response
        except BadSignature:
            # apply soft rejection if cookie is tampered or expired
            pass
        print('cookie is present')
        variables = {
                'question': question,
                'action':'soft rejection',
            }
        return render(request, 'polls/vote_thank.html', variables)
    # Now redisplay the question voting form
    return render(request,'polls/detail.html',{
            'question':question,
            'error_message':"Poll is for {}".format(question.pollsettings.who_can_respond)
        })