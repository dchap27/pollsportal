from django.db import models
from django.conf import settings
from django.utils import timezone
from datetime import datetime, timedelta, date
from django.core.urlresolvers import reverse # use for get_absolute_url
from django.template.defaultfilters import slugify
from django.core.exceptions import ValidationError
# Adding generic relations to modelse
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.fields import GenericRelation
from channels.binding.websockets import WebsocketBinding
# Create your models here.
class Quiz(models.Model):
    MODE = (
      ('notTimed','self-paced'),
      ('timed','set time')
    )
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    poll_type = models.CharField(max_length=15,default='Quiz')
    slug = models.SlugField(max_length=220,db_index=True,unique=True)
    mode = models.CharField(max_length=15,choices=MODE,default='notTimed')
    quiz_duration = models.PositiveIntegerField(null=True,blank=True)
    attempt = models.PositiveIntegerField(db_index=True,default=3)
    quiz_takers = models.ManyToManyField(settings.AUTH_USER_MODEL,through='QuizTakerScore',
                                         related_name='quiztakers')
    total_likes = models.PositiveIntegerField(db_index=True,default=0)
    user_liked = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='quizlikes')
    users_share= models.ManyToManyField(settings.AUTH_USER_MODEL,blank=True, related_name='quizsharers')
    publish = models.DateTimeField(null=True,blank=True)
    created = models.DateTimeField(auto_now_add=True)
    suspended = models.BooleanField(default=False)
    lock_date = models.DateTimeField(default=timezone.now)
    status = models.CharField(max_length=22,default='Draft')
    # settings = GenericRelation(PollSettings,content_type_field='content_type',
    #                     object_id_field='object_id',related_query_name='quizzes')

    class Meta:
        verbose_name_plural = "Quizzes" # To display plural of category

    def __str__(self):
        return "{}".format(self.title)

    def get_status(self):
        # determine status of quiz
        if self.suspended:
            status = "Suspended"
        elif self.publish is None or self.publish <= self.created:
            status = "Draft"
        elif self.publish > timezone.now():
            status = "Pending"
        elif self.lock_date <= timezone.now():
            status = "Closed"
        elif self.publish <= timezone.now():
            status = "Active"
        return status

    def get_absolute_url(self):
        return reverse('polls:quiz_detail', args=[self.id,self.slug])

    def create_activity(self, performer,verb,timenow):
        from stream_framework.activity import Activity
        # from polls.verbs import Like as LikeVerb
        from django.utils.timezone import make_naive
        import pytz
        activity = Activity(
            performer.id,
            verb,
            self.id,
            self.owner_id,
            time=make_naive(timenow, pytz.utc)
        )
        return activity

    def save(self,*args,**kwargs):
        # determine status of quiz
        if self.suspended:
            self.status = "Suspended"
        elif self.publish is None or self.publish <= self.created:
            self.status = "Draft"
        elif self.publish > timezone.now():
            self.status = "Pending"
        elif self.lock_date <= timezone.now():
            self.status = "Closed"
        elif self.publish <= timezone.now():
            self.status = "Active"
        if not self.id:
            self.slug = slugify(self.title)
        super(Quiz,self).save(*args,**kwargs)


class OpenAnswer(models.Model):
    answer = models.CharField(max_length=60)

    def __str__(self):
        return self.answer


class QuizQuestion(models.Model):
    MODE = (
      ('choice','Multiple choice'),
      ('open','open-ended')
    )
    quiz = models.ForeignKey(Quiz,on_delete=models.CASCADE)
    quiz_question_text = models.CharField(max_length=160)
    question_type = models.CharField(max_length=18,choices=MODE,default='choice')
    participants = models.ManyToManyField(settings.AUTH_USER_MODEL,blank=True)
    open_answer = models.OneToOneField(OpenAnswer,null=True,blank=True,
                                        on_delete=models.CASCADE)

    def __str__(self):
        return "{}".format(self.quiz_question_text)


class QuizChoice(models.Model):
    quiz_question = models.ForeignKey(QuizQuestion, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=160)
    answer = models.BooleanField(default=False)

    def __str__(self):
        return "{}".format(self.choice_text)


class SubmittedOpenAnswer(models.Model):
    quiz_question = models.ForeignKey(QuizQuestion, on_delete=models.CASCADE)
    answerer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    submitted_answer = models.CharField(max_length=60)

    def __str__(self):
        return "{}".format(self.submitted_answer)


class QuizTakerScore(models.Model):
    quiz_taker = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    score = models.PositiveIntegerField(db_index=True,default=0)
    number_of_attempt = models.PositiveIntegerField(db_index=True,default=0)
    choice_answers = models.ManyToManyField(QuizChoice,blank=True,
                                              related_name="selectedanswers")
    open_answers = models.ManyToManyField(SubmittedOpenAnswer,blank=True,
                                       related_name='writtenanswers')
    date_answered = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "score: {}".format(self.score)


class Question(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,null=True)
    question_text = models.CharField(max_length=160)
    poll_type = models.CharField(max_length=16,default='Opinion')
    # remove unique=True from slug field to allow flexibility
    slug = models.SlugField(max_length=200,db_index=True)  # unique=True removed
    category = models.CharField(max_length=22,default='General')
    total_votes = models.PositiveIntegerField(db_index=True,default=0)
    anonymous_votes = models.PositiveIntegerField(default=0,blank=True,null=True)
    users_voted= models.ManyToManyField(settings.AUTH_USER_MODEL,blank=True, related_name='voters')
    users_share= models.ManyToManyField(settings.AUTH_USER_MODEL,blank=True, related_name='sharers')
    publish = models.DateTimeField(null=True,blank=True)
    created = models.DateTimeField(auto_now_add=True)
    poll_info = models.CharField(max_length=200,null=True,blank=True)
    eligibility=models.BooleanField(default=False)
    eligible_gender = models.CharField(max_length=10,null=True,blank=True)
    suspended = models.BooleanField(default=False)
    lock_date = models.DateTimeField(default=timezone.now)
    total_likes = models.PositiveIntegerField(db_index=True,default=0)
    user_liked = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='likes')
    quota = models.PositiveIntegerField(null=True,blank=True)
    status = models.CharField(max_length=22,default='Draft')
    

    def __str__(self):
        return "{}".format(self.question_text)

    def was_published_recently(self):
        return self.publish >= timezone.now() - timedelta(days=1)

    def get_status(self):
        """Set the status of each poll according to publish date"""
        if self.suspended:
            return "Suspended"
        elif self.publish is None or self.publish <= self.created:
            return "Draft"
        elif self.publish >= timezone.now():
            return "Pending"
        elif self.quota == self.total_votes or self.lock_date <= timezone.now():
            return "Closed"
        elif self.publish <= timezone.now():
            return "Active"
        return self.status

    def get_absolute_url(self):
        return reverse('polls:detail', args=[self.id,self.slug])

    def save(self,*args,**kwargs):
        # determine the status of poll
        if self.suspended:
            self.status = "Suspended"
        elif self.publish is None or self.publish <= self.created:
            self.status = "Draft"
        elif self.publish > timezone.now():
            self.status = "Pending"
        elif self.quota == self.total_votes or self.lock_date <= timezone.now():
            self.status = "Closed"
        elif self.publish <= timezone.now():
            self.status = "Active"
        if not self.id:
            self.slug = slugify(self.question_text)
        result = super(Question,self).save(*args,**kwargs)
        return result

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=160)
    votes=models.IntegerField(default=0)
    m_votes=models.IntegerField(default=0)
    f_votes=models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text

class ActivePoll(models.Model):
    poll_type = models.CharField(max_length=15)
    poll_title = models.CharField(max_length=200)
    publish = models.DateTimeField(null=True,blank=True)
    quiz_poll = models.ForeignKey(Quiz, null=True, blank=True, on_delete=models.CASCADE)
    single_poll = models.ForeignKey(Question, null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "ActivePolls"

    def __str__(self):
        return "{}".format(self.poll_title)

    def get_absolute_url(self):
        if self.single_poll != None and self.quiz_poll is None:
            return reverse('polls:detail', args=[self.single_poll.id,self.single_poll.slug])
        return reverse('polls:quiz_detail', args=[self.quiz_poll.id,self.quiz_poll.slug])

    def create_activity(self, performer,verb,timenow):
        from stream_framework.activity import Activity
        # from polls.verbs import Like as LikeVerb
        from django.utils.timezone import make_naive
        import pytz

        if self.single_poll != None and self.quiz_poll is None:
            owner_id = self.single_poll.owner_id
        else:
            owner_id = self.quiz_poll.owner_id
        activity = Activity(
            performer.id,
            verb,
            self.id,
            owner_id,
            time=make_naive(timenow, pytz.utc)
        )
        return activity

class Comment(models.Model):
    question = models.ForeignKey(Question, related_name='comments')
    name = models.CharField(max_length=25)
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ('created',)

    def __str__(self):
        return '{}'.format(self.question)

    def get_absolute_url(self):
        return reverse('polls:results', args=[self.question.id,self.question.slug])

class FeedBack(models.Model):
    user=models.ForeignKey(settings.AUTH_USER_MODEL)
    feedback_text=models.TextField()
    created=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}'.format(self.feedback_text)

class QuestionUpdate(WebsocketBinding):

    model = Question
    stream = 'questionupdates'
    fields = ["status","total_likes",'total_votes','users_share']

    @classmethod
    def group_names(cls, instance):
        return ["binding-counts"]

    def has_permission(self, user, action, pk):
        if action == "create":
            return False
        return True

class PollSettings(models.Model):
    # constants
    default_end_date = timezone.now() + timedelta(days=90)
    WHOTORESPOND = (
        ('registered-users', 'Registered users only'),
        ('anyone','Anyone including anonymous'),
        ('group', 'Group Participants'),
        ('male','Male only'),
        ('female','Female only'),
    )
    DURATION = (
        ('Auto', 'Auto lock'),
        ('quota', 'Lock after votes quota'),
        ('custom','Set custom day/time')
    )
    BEGINS = (
        ('now','now'),
        ('1hr', '1 hour from now'),
        ('5mins','5 mins from now'),
        ('5hr', '5 hours from now'),
        ('next_day','24 hours'),
        ('custom', 'set date')
    )
    PRESENTATION = (
        ('hide_everybody', 'Hide result till closing date'),
        ('show_everyone', 'Show result to everyone'),
        ('voters_only', 'Show result after voting')
    )
    PEOPLE_CATEGORY = (
        ('everyone','Everyone (all registered users)'),
        ('participants','Participants (by group)'),
        ('anonymous','Everyone (include unregistered user)'),
        ('none', 'Nobody')
    )

    question = models.OneToOneField(Question,primary_key=True)
    who_can_respond = models.CharField(max_length=25,choices=WHOTORESPOND,default='anyone')
    duration = models.CharField(max_length=25,choices=DURATION,default='Auto')
    votes_quota=models.PositiveIntegerField(null=True,blank=True)
    end_date = models.DateTimeField(null=True,blank=True)
    start_date = models.DateTimeField(null=True,blank=True,default=timezone.now)
    begin_when = models.CharField(max_length=15,choices=BEGINS,blank=True,null=True)
    presentation = models.CharField(max_length=15,choices=PRESENTATION,default='voters_only')
    view_poll = models.CharField(max_length=50,choices=PEOPLE_CATEGORY,default='everyone')
    # vote_on_poll = models.CharField(max_length=50,choices=PEOPLE_CATEGORY,default='everyone')
    share_poll = models.CharField(max_length=50,choices=PEOPLE_CATEGORY,default='everyone')
    comment_on_poll = models.CharField(max_length=50,choices=PEOPLE_CATEGORY,default='everyone')
    view_result = models.CharField(max_length=50,choices=PEOPLE_CATEGORY,default='everyone')

    class Meta:
        verbose_name_plural = "Poll settings" # To display plural of category

    def __str__(self):
        return "Settings for {}".format(self.question)

    def enddate(self):
        if self.duration == "Auto":
            try:
                self.end_date = self.question.publish + timedelta(days=60)
            except TypeError:
                self.end_date = timezone.now() + timedelta(days=60)
            self.question.lock_date = self.end_date
            try:
                self.question.quota = None
            except:
                pass
            return self.end_date
        elif self.duration == 'quota':
            try:
                self.question.quota = self.votes_quota
            except:
                pass
            try:
                self.end_date = self.question.publish + timedelta(days=60)
            except TypeError:
                self.end_date = timezone.now() + timedelta(days=60)
            self.question.lock_date = self.end_date
            return self.votes_quota
        elif self.duration == 'custom':
            if self.end_date == None:
                self.question.lock_date = default_end_date
            else:
                self.question.lock_date = self.end_date
            try:
                self.question.quota = None
            except:
                pass
            return self.end_date
        else:
            return None

    def begins(self):
        if self.begin_when == 'now':
            self.question.publish = timezone.now()
            self.start_date = self.question.publish
            return self.question.publish
        if self.begin_when == '5mins':
            self.question.publish = timezone.now() + timedelta(minutes=5)
            self.start_date = self.question.publish
            return self.question.publish
        elif self.begin_when == '1hr':
            self.start_date = timezone.now() + timedelta(hours=1)
            self.question.publish = self.start_date
            return self.start_date
        elif self.begin_when == '5hr':
            self.start_date = timezone.now() + timedelta(hours=5)
            self.question.publish = self.start_date
            return self.start_date
        elif self.begin_when == 'next_day':
            self.start_date = timezone.now() + timedelta(days=1)
            self.question.publish = self.start_date
            return self.start_date
        elif self.begin_when == 'custom':
            self.question.publish = self.start_date
            return self.start_date
        else:
            self.question.publish = None
            return self.question.publish

    def responder(self):
        if self.who_can_respond == 'anyone':
            try:
                self.question.eligibility = False
                return self.question.eligibility
            except:
                pass
        elif self.who_can_respond == 'male':
            try:
                self.question.eligibility = True
                self.question.eligible_gender = 'M'
                return self.question.eligibility
            except:
                pass
        elif self.who_can_respond == 'female':
            try:
                self.question.eligibility = True
                self.question.eligible_gender = 'F'
                return self.question.eligibility
            except:
                pass
        return None

    def save(self,*args,**kwargs):
        self.begins()
        self.responder()
        self.enddate()
        self.question.save()
        super(PollSettings,self).save(*args,**kwargs)