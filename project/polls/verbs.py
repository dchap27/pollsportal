from stream_framework.verbs import register
from stream_framework.verbs.base import Verb


class Vote(Verb):
    id = 5
    infinitive = 'vote'
    past_tense = 'voted'

register(Vote)

class Like(Verb):
    id = 6
    infinitive = 'like'
    past_tense = 'liked'

register(Like)

class Share(Verb):
    id = 7
    infinitive = 'share'
    past_tense = 'shared'

register(Share)

class UpdateProfile(Verb):
    id = 8
    infinitive = 'update profile'
    past_tense = 'updated profile'

register(UpdateProfile)

# subclass the follow verb
class FollowVerb(Verb):
    id = 9
    # Add singular to the class
    infinitive = 'follow'
    past_tense = 'followed'
    singular = 'follows'

register(FollowVerb)
