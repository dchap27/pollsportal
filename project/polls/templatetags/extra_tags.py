from django import template
from django.template.defaultfilters import stringfilter # to convert the filter to string
register = template.Library()

@register.simple_tag
def percent(value,value2):
    try:
        new_percent = (value * 100)/value2
        new_percent = "{0:.1f}".format(new_percent)
    except:
        new_percent = "0"
    new_percent = (new_percent) + "%"
    return (new_percent)

@register.simple_tag(name="percentnosymbol")
def percent_no_symbol(value,value2):
    try:
        new_percent = (value * 100)/value2
        new_percent = "{0:.1f}".format(new_percent)
    except:
        new_percent = "0"
    new_percent = (new_percent)
    return (new_percent)

@register.filter
@stringfilter
def capitalize(value):
    return value.capitalize()

@register.filter(name="choicelist")
def choice_list(value):
    lst = []
    for choice in value:
        lst.append(choice.choice_text)
    return lst

@register.filter(name="choicevotes")
def choice_votes(value):
    lst = []
    for choice in value:
        lst.append(int(choice.votes))
    return lst

@register.filter(name="subtract")
@stringfilter
def substract(value, arg):
    try:
        conversion = int(value)
        arg = int(arg)
        result = conversion - arg
        if result < 0:
            return 0
        return result
    except:
        return value

@register.simple_tag(name="myactors_count")
def actors_count_in_activity(actors_list, actor_id):
    """no_of_action_performed = actors_list.count(actor_id)
    """
    lst=[]
    for each in actors_list:
        lst.append(str(each))
    return lst.count(str(actor_id))

@register.simple_tag
def activity_captured(activity_ids):
    if len(activity_ids) == 1:
        return (str(activity_ids[0]))
    return list(activity_ids)
