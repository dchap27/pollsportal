from django.contrib.auth.models import User
from stream_framework.feed_managers.base import Manager
from stream_framework.feed_managers.base import FanoutPriority
from .models import ActivePoll
from project.account.models import Friendship
from project.polls.feeds import AggregatedQuestionFeed, QuestionFeed, UserQuestionFeed, \
        MyNotificationFeed

# Notification method
def real_time_notification(feed, user_id):
    """
    send update notification of unseens to user in real time using channels
    """
    # notify feed owner of changes in realtime
    from channels import Group
    from json import dumps

    message = {
        "feedcount": feed.count_unseen(),
        "stream_name": "messagenotify",
    }
    Group("notify-%s"%user_id).send(
            # WebSocket text frame, with JSON content
            {'text': dumps(message)}
        )

class QuestionManager(Manager):
    # this has both a normal feed and an aggregated feed (more like
    # how facebook or wanelo uses feeds)
    feed_classes = dict(
        normal=QuestionFeed,
        aggregated=AggregatedQuestionFeed
    )
    user_feed_class = UserQuestionFeed

    def add_action(self, action, performer, verbobj, time):
        # Get the actual active poll objects
        try:
            poll = ActivePoll.objects.get(
                poll_type=action.poll_type,
                single_poll=action
            )
        except:
            poll = ActivePoll.objects.get(
                poll_type=action.poll_type,
                quiz_poll=action
            )
        activity = poll.create_activity(performer, verbobj, time)
        # add user activity adds it to the user feed, and starts the fanout
        self.add_user_activity(performer.id, activity)

    def remove_action(self, action, performer, verbobj, time):
        # Get the actual active poll objects
        try:
            poll = ActivePoll.objects.get(
                poll_type=action.poll_type,
                single_poll=action
            )
        except:
            poll = ActivePoll.objects.get(
                poll_type=action.poll_type,
                quiz_poll=action
            )
        activity = poll.create_activity(performer, verbobj, time)
        # removes the pin from the user's followers feeds
        self.remove_user_activity(performer.id, activity)

    def get_user_follower_ids(self, user_id):
        ids = Friendship.objects.filter(to_friend=user_id).values_list('from_friend_id', flat=True)
        return {FanoutPriority.HIGH:ids}

manager = QuestionManager()

class MyNotification (object):
    '''
    Abstract the access to the notification feed
    '''
    def mark_as_seen_or_read(self,user,activity,unread=None):
        """
        method takes activity_ids, user and retrieve notification feeds
        for the user Then mark selected activity as seen or read
        """
        feed = MyNotificationFeed(user)
        storage = feed.timeline_storage
        activities = list(feed[:])
        i=0
        seen_activities = []
        for a in activities:
            for each_id in activity:
                if each_id in a.activity_ids:
                    act = activities[i]
                    seen_activities.append(act)
            i += 1
        # feedmarker = feed.feed_markers
        seen_activity_ids = storage.activities_to_ids(seen_activities)
        if unread:
            # mark activity as seen and read and remove from feed
            feed.remove_many_aggregated(seen_activities)
        else:
            feed.mark_activities(seen_activity_ids)  # mark as seen
        # notify feed owner of changes in realtime
        real_time_notification(feed,user.id)

    def add_notify_action(self, action, performer, verbobj):
        """
        We notify users about activities that is peculiar to each user.
        For instance all users who previously like or vote on poll Will
        be notify about that same action performed on same object by
        another user.
        Remove performer from list of notifiers since he needs no notification
        on actions he performed.
        """
        from stream_framework.activity import Activity
        from django.utils.timezone import make_naive
        import pytz
        from django.utils import timezone

        if verbobj.id == 9:
            # i.e a follow action:
            # there won't be target such that action.user is none
            activity = Activity(
                performer.id, verbobj, action.to_friend_id,
                time=make_naive(timezone.now(), pytz.utc)
            )
            feed = MyNotificationFeed(action.to_friend)
            feed.insert_activity(activity)
            feed.add(activity)
            # send notification
            real_time_notification(feed,action.to_friend.id)
            return

        # get the ActivePoll object
        try:
            poll = ActivePoll.objects.get(
                poll_type=action.poll_type,
                single_poll=action
            )
        except:
            poll = ActivePoll.objects.get(
                poll_type=action.poll_type,
                quiz_poll=action
            )

        # create the activity
        activity = Activity(
            performer.id, verbobj, poll.id, action.owner,
            time=make_naive(timezone.now(), pytz.utc)
        )
        if performer != action.owner:
            # Notify the owner of the object if not the performer
            feed = MyNotificationFeed(action.owner)
            feed.insert_activity(activity)
            feed.add(activity)
            # send notification
            real_time_notification(feed,action.owner.id)


        if verbobj.id == 2: # comment verb
            comments = action.comments.filter(active=True)
            # make the list unique
            # courtesy https://www.peterbe.com/plog/uniqifiers-benchmark
            def make_unique(seq):
                # Not order preserving
                keys = {}
                for e in seq:
                    keys[e] = 1
                return keys.keys()

            users_who_commented = [each.name for each in comments]
            # get list of users who already commmented and notify them of new comments
            users_to_notify = make_unique(
                [User.objects.get(username=a) for a in users_who_commented]
                )
            users_to_notify = list(users_to_notify)
            if action.owner in users_to_notify:
                users_to_notify.remove(action.owner) # avoiding duplicate notification
            if performer in users_to_notify:
                users_to_notify.remove(performer) # don't notify performer of his actions

            for each_user in users_to_notify:
                feed = MyNotificationFeed(each_user)
                feed.insert_activity(activity)
                feed.add(activity)
                # send notification
                real_time_notification(feed,each_user.id)
            return

        if verbobj.id == 5: # vote verb
            users_to_notify = action.users_voted.exclude(
                username=performer.username
                )
            if action.owner in users_to_notify:
                users_to_notify = users_to_notify.exclude(
                                  username=action.owner.username)
            for each_user in users_to_notify:
                feed = MyNotificationFeed(each_user)
                feed.insert_activity(activity)
                feed.add(activity)
                # send notification
                real_time_notification(feed,each_user.id)
            return

        if verbobj.id == 6: # like verb
            users_to_notify = action.user_liked.exclude(
                username=performer.username
                )
            if action.owner in users_to_notify:
                users_to_notify = users_to_notify.exclude(
                                  username=action.owner.username)
            for each_user in users_to_notify:
                feed = MyNotificationFeed(each_user)
                feed.insert_activity(activity)
                feed.add(activity)
                # send notification
                real_time_notification(feed,each_user.id)
            return

        # if verbobj.id == 7: # share verb
        #     users_to_notify = action.users_share.exclude(
        #         username=performer.username
        #         )
        #     if action.user in users_to_notify:
        #         users_to_notify = users_to_notify.exclude(
        #                            username=action.user.username)
        #     for each_user in users_to_notify:
        #         feed = MyNotificationFeed(each_user)
        #         feed.insert_activity(activity)
        #         feed.add(activity)
        #         # send notification
        #         real_time_notification(feed,each_user.id)
        #     return

    def send_invite(self, action, performer, verbobj, invitees):
        """
        This method sends invite to vote to invitees. The list of invitees
        is imported from values submitted in POST method of share in views.py.
        The verb is 'share' with id=7 and removed from add_notify_action() since
        other user need not notify about action
        """
        from stream_framework.activity import Activity
        from django.utils.timezone import make_naive
        import pytz
        from django.utils import timezone

        # Get the actual active poll objects
        try:
            poll = ActivePoll.objects.get(
                poll_type=action.poll_type,
                single_poll=action
            )
        except:
            poll = ActivePoll.objects.get(
                poll_type=action.poll_type,
                quiz_poll=action
            )

        # create the activity with verb_id=7
        activity = Activity(
            performer.id, verbobj, poll.id, action.owner,
            time=make_naive(timezone.now(), pytz.utc)
        )
        # notify owner of poll about new share if not performer
        if performer != action.owner:
            feed = MyNotificationFeed(action.owner)
            feed.insert_activity(activity)
            feed.add(activity)
            # send notification
            real_time_notification(feed,action.owner.id)

        # send invite to invitees
        copied = invitees.copy()
        if action.owner in copied:
            # remove from list
            invitees.remove(action.owner)
        for each_user in invitees:
            feed = MyNotificationFeed(each_user)
            feed.insert_activity(activity)
            feed.add(activity)
            # send notification
            real_time_notification(feed,each_user.id)
        return

notify = MyNotification()
