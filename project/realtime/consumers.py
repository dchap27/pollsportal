import logging
import json

from channels import Channel, Group
from channels.sessions import channel_session
from channels.auth import channel_session_user, channel_session_user_from_http
from channels.security.websockets import allowed_hosts_only
from urllib.parse import parse_qs
from channels.generic.websockets import WebsocketDemultiplexer, JsonWebsocketConsumer
from project.polls.models import QuestionUpdate
from project.polls.views import r, NOW

log = logging.getLogger(__name__)


class MessageNotificationConsumer(JsonWebsocketConsumer):
    http_user_and_session = True
    # def connect(self, message, multiplexer,**kwargs):
    #     if message.user:
    #         multiplexer.group_send(
    #             "notify-%s"%self.message.user.id,
    #             multiplexer.stream,
    #             {"status":"I just connected!"})
    #         # Accept connection
    #         message.reply_channel.send({"accept": True})
    #         # Send data with the multiplexer
    #         log.debug("%s is connected to %s"%(message.user.id,multiplexer.stream))
    #     else:
    #         # reject connection
    #         message.reply_channel.send({"close": True})

    def connection_groups(self, **kwargs):
        """
        Group(s) to make people join when they connect and leave when they
        disconnect. Make sure to return a list/tuple, not a string!
        """
        connect_path = self.path.split('/')

        if "dashboard" in connect_path:
            return ["notify-%s"%self.message.user.id,"dashboard-updates"]
        elif "updates" in connect_path:
            return ["notify-%s"%self.message.user.id]
        return []

    def disconnect(self, message, multiplexer, **kwargs):
        """
        Removes the user from the group when they disconnect.

        Channels will auto-cleanup eventually, but it can take a while, and having old
        entries cluttering up your group will reduce performance.
        """
        log.debug("Stream %s is closed"%multiplexer.stream)

    def receive(self, content, multiplexer, **kwargs):
        if content['activity'] in ['like','comment','vote','share']:
            msg = {
                "activity": True,
                'action': content['activity'],
                "chart_type":'pie',
                "chart_url": "/polls/graphs/",
            }
            # Simple echo
            multiplexer.group_send("dashboard-updates",multiplexer.stream,{'message':msg})

# class DashboardUpdates(JsonWebsocketConsumer):
#     """This class add and create live updates for members connected
#     to the dashboard. The group is added to channel_layer automatically
#     """
#     http_user_and_session = True
#     def connect(self, message, multiplexer, **kwargs):
#         # Accept connection
#         message.reply_channel.send({"accept": True})
#         # Send data with the multiplexer
#         print("%s is connected to %s"%(message.user.id,multiplexer.stream))
#     def connection_groups(self, **kwargs):
#         """
#         Group(s) to make people join when they connect and leave when they
#         disconnect. Make sure to return a list/tuple, not a string!
#         """
#         connect_path = self.path.split('/')
#         if "dashboard" in connect_path:
#             return ["dashboard-updates"]
#         return []
#     def disconnect(self, message, multiplexer, **kwargs):
#         """
#         Removes the user from the group when they disconnect.
#
#         Channels will auto-cleanup eventually, but it can take a while, and having old
#         entries cluttering up your group will reduce performance.
#         """
#         log.debug("Stream %s is closed"%multiplexer.stream)


class Demultiplexer(WebsocketDemultiplexer):
    # http_user_and_session = True
    consumers = {
        "questionupdates": QuestionUpdate.consumer,
        "messagenotify": MessageNotificationConsumer,
    }
    def connection_groups(self, **kwargs):
        return ["binding-counts",]
