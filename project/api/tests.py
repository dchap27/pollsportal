from django.test import TestCase
from project.polls.models import Question

# Create your tests here.

class QuestionTestCase(TestCase):
	""" This class defines the test suite for the Question Model in polls """

	def setUp(self):
		"""
		Define test client and other variables
		"""
		self.question_text = "Who is your teacher"
		self.question = Question(question_text=self.question_text)

	def test_model_can_create_a_question(self):
		"""
		Test the Question model can create a question
		"""
		old_count = Question.objects.count()
		self.question.save()
		new_count = Question.objects.count()
		self.assertNotEqual(old_count, new_count)
