from django.shortcuts import render, get_object_or_404, render_to_response, redirect
from django.http import Http404, HttpResponseRedirect, HttpResponse, JsonResponse
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from project.account.forms import *
from .models import Profile, AccountSettings, Group, Membership, Invitation, VerifyRegistration, User, Friendship
from django.core.urlresolvers import reverse
# To including message interfaces
from django.contrib import messages
from project.polls.common.decorators import ajax_required
from datetime import datetime,date
from django.utils import timezone
from django.db.utils import IntegrityError
from config.tasks import welcome

# Login user
def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'],
                                password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(reverse (
                     'index'
                    ))
                else:
                    return HttpResponse('Disabled account')
            else:
                messages.warning(request,"Your username and password didn't match"\
          " Please try again.")
    else:
        form = LoginForm()
    return render(request, 'registration/login.html', {'form': form})

# Registration page
def _create_user(username,password,gender,email):
    state = False
    try:
        user = User.objects.create_user(
          username=username,
          password = password,
          email=email
        )
        state = True
    except IntegrityError:
        state = False
        return (state, None)
    # create a profile for the user
    profile = Profile.objects.create(
       user=user,
       gender = gender
      )
    return (state, user)

def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            # check if the registration is through invitation
            if 'invitation' in request.session:
                invitation = \
                 Invitation.objects.get(id=request.session['invitation'])
                dummy, user = _create_user(cd['username'],cd['password2'],
                         cd['gender'],cd['email'])
                if not dummy:
                    messages.warning(request, 'username already taken')
                    return redirect('account:register')
                # Create friendship from user to sender.
                friendship = Friendship(
                  from_friend=user, to_friend=invitation.sender
                )
                friendship.save()

                # # create activity
                # action1 = create_action(user, 'follows', invitation.sender)
                # from .verbs import FollowVerb
                # manager.add_action(friendship, request.user, FollowVerb, timezone.now())
                # notify.add_notify_action(friendship, request.user, FollowVerb)

                # Create friendship from sender to user.
                friendship = Friendship (
                  from_friend=invitation.sender, to_friend=user
                )
                friendship.save()

                # initiate polladmin to follow new user immediately
                admin = User.objects.get(username='polladmin')
                friendship = Friendship(
                   from_friend=admin, to_friend=user)
                friendship.save()

                # Delete the invitation from the database and session.
                invitation.delete()
                del request.session['invitation']
                # variables = {
                #   'username': user.username,
                # }
                # Sending a welcome message
                welcome.delay(user.id)

                # Authenticate new user and login
                user_auth = authenticate(username=cd['username'],
                                    password=cd['password'])
                if user_auth is not None:
                    if user_auth.is_active:
                        login(request, user, backend='django.contrib.auth.backends.ModelBackend')
                        return HttpResponseRedirect(reverse (
                         'index'
                        ))

                return render(request, 'registration/register_done.html',
                            variables)

            # create a temporary verification database for the user
            verification = VerifyRegistration(
              username=cd['username'],
              gender=cd['gender'],
              email=cd['email'],
              password=cd['password2'],
              verify_code=User.objects.make_random_password(25)
            )
            try:
                verification.send()
                verification.save()
                messages.success(request,
                  'Account creation successful. Kindly, '\
                  'check your email %s to verify your account.' % verification.email
                )
            except:
                messages.warning(request,
                  'There was an error creating your account. Please, try again!'
                )

    else:
        form = RegistrationForm()
    variables = {'form': form}
    return render(request, 'registration/register.html', variables)

def verify_registration(request,code):
    try:
        verification = get_object_or_404(VerifyRegistration, verify_code__exact=code)
        # Stores the ID of the object in the user's session
        request.session['verification'] = verification.id
        return complete_registration(request)
    except:
        messages.warning(request, "The link has expired")
        form = RegistrationForm()
        return render(request, 'registration/register.html', {'form': form})

def complete_registration(request):
    try:
        if 'verification' in request.session:
            # Retrieve the verification object.
            verify = \
            VerifyRegistration.objects.get(id=request.session['verification'])
            # Create user account and profile
            dummy, user = _create_user(verify.username, verify.password,
                verify.gender, verify.email)
            if not dummy:
                messages.warning(request, 'username already taken')
                return redirect('account:register')
            password = verify.password
            variables = {
              'username': user.username,
            }
            # initiate polladmin to follow new user immediately
            admin = User.objects.get(username='polladmin')
            friendship = Friendship(
               from_friend=admin, to_friend=user)
            friendship.save()

            # Sending a welcome message
            welcome.delay(user.id)
            # Delete the verification from the database and session.
            verify.delete()
            del request.session['verification']
            # Authenticate new user and login
            user_auth = authenticate(username=user.username,
                                password=password)
            if user_auth is not None:
                if user_auth.is_active:
                    login(request, user, backend='django.contrib.auth.backends.ModelBackend')
                    return HttpResponseRedirect(reverse (
                     'index'
                    ))

            return render(request, 'registration/register_done.html', variables)
        else:
            messages.warning(request, 'The verification link has expired')
    except:
        messages.warning(request, "Something went wrong! Try again!")
    return HttpResponseRedirect(reverse (
     'account:register'))

def save_social_profile(request,backend,user,response,*args,**kwargs):
    from .models import Profile
    if backend.name == 'facebook':
        try:
            profile = Profile.objects.get(user=user)
        except:
            profile = Profile(user_id=user.id)
        if response.get('gender') == 'male':
            profile.gender = 'M'
        elif response.get('gender') == 'female':
            profile.gender = 'F'
        profile.save()

    elif backend.name == 'google-oauth2':
        try:
            profile = Profile.objects.get(user=user)
        except:
            profile = Profile.objects.create(user=user)
        if response.get('gender') == 'male':
            profile.gender = 'M'
        elif response.get('gender') == 'female':
            profile.gender = 'F'
        profile.save()

@login_required(login_url='account:login')
def edit_profile(request):
    profile = Profile.objects.get_or_create(user=request.user)
    if request.method == 'POST':
        user_form = UserEditForm(instance=request.user,
                    data=request.POST)
        profile_form = ProfileEditForm(instance=request.user.profile,
                    data=request.POST,
                    files=request.FILES)
        if user_form.is_valid() and profile_form.is_valid():
            # since these forms are ModelForm
            user_form.save()
            profile_form.save()
            profile, prev_state = profile
            # create an action for updating profile
            # if profile.gender == 'M':
            #     action = create_action(request.user, 'updated his profile')
            # elif profile.gender == 'F':
            #     action = create_action(request.user, 'updated her profile')
            # else:
            #     action = create_action(request.user, 'updated profile')

            messages.success(request,
            "Profile updated successfully")
            # from .verbs import UpdateProfile as UpdateProfileVerb
            # manager.add_action(profile, request.user, UpdateProfileVerb, timezone.now())

            return HttpResponseRedirect(reverse (
             'index',
            ))
    else:
        user_form = UserEditForm(instance=request.user)
        profile_form = ProfileEditForm(instance=request.user.profile)
    return render(request,
             'account/edit_profile.html',
             {
             'user_form': user_form,
             'profile_form': profile_form,
             })

def get_or_add_to_group(request):
    """
    get list of groups for user or add a member to a group
    """
    next = request.META.get('HTTP_REFERER', None) or '/'
    # groups user created
    groups = Group.objects.filter(owner=request.user)
    # groups user belong to
    groups_belong = Group.objects.filter(members__username=request.user.username)
    if request.method == 'POST':
        group_name = request.POST['group_name']
        if group_name:
            try:
                group = Group.objects.get(group_name=group_name)
                if 'add_new_member' in request.POST:
                    add_new_member = request.POST['add_new_member']
                    member = User.objects.get(id=add_new_member)
                else: # if no selection made from browser
                    member = None
                    message = "You have made no selection! Try again!!"
            except Group.DoesNotExist:
                status = 'ko'
                message = "Something went wrong! Group doesn't exist"
            except User.DoesNotExist:
                status = 'ko'
                message = "{} is not a registered user".format(add_new_member)
            else:
                # check if user membership should be removed
                if 'remove-membership' in request.POST:
                    # remove member from group
                    Membership.objects.filter(person=request.user,group=group).delete()
                    status = 'ok'
                    message = "You've been removed from {}".format(group)
                elif 'delete-group' in request.POST:
                    try:
                        # Delete group
                        Group.objects.filter(group_name=group_name,owner=request.user).delete()
                        status = 'ok'
                        message = "Group deleted"
                    except:
                        status = 'ko'
                        message = "Something is not right! Try again later!!"
                # check if member is already a member of group
                elif member not in group.members.all() and member != None:
                    membership = Membership(
                        person=member,
                        group=group,
                        invited_by=request.user,
                        date_joined=timezone.now()
                    )
                    membership.save()
                    message= '{} successfully added to {}'.format(member,group.group_name.upper())
                    status = 'ok'
                else:
                    if member != None:
                        message = '{} already a member of {}'.format(member,group.group_name.upper())
                    status = 'ko'
            variables={
                'status':status,
                'message':message,
                'group':group_name,
            }
            if request.is_ajax():
                return JsonResponse(variables)
            return HttpResponseRedirect(next)
        if request.is_ajax():
            return JsonResponse({'status':'ko','message':'Something went wrong!'})
        return HttpResponseRedirect(next)
    group_form = GroupForm()
    variables = {
        'groups':groups,
        'groups_belong':groups_belong,
        'group_form': group_form
    }
    return render(request, "account/group_list.html", variables)

def create_group(request):
    """Method create new group
    """
    next = request.META.get('HTTP_REFERER', None) or '/'
    if request.method == 'POST':
        group_form = GroupForm(data=request.POST)
        if group_form.is_valid():
            group_name = group_form.cleaned_data['group_name']
            # try:
            group = Group(
                owner=request.user,
                group_name=group_name
            )
            group.save()

            if request.is_ajax():
                groups = Group.objects.filter(owner=request.user)
                return render(request, "account/group_form.html", {'groups':groups})
            return HttpResponseRedirect(next)
        messages.error(request,"Something went wrong!")
        if request.is_ajax():
            return JsonResponse({'status':'ko','message':'Something went wrong! Group NOT created!!'})
        return HttpResponseRedirect(next)
    group_form = GroupForm()
    variables = {'group_form':group_form}
    return render(request, "account/group_form.html", variables)

@ajax_required
def get_group_members(request):
    """
    Get members of a group and return a json object
    """
    status = 'ko'
    message = ""
    members = []
    if request.GET.__contains__('group_name'):
        group_name = request.GET['group_name']
        try:
            group = Group.objects.get(group_name=group_name)
            members_query = group.members.all()
            for member in members_query:
                data = {
                    'name': member.username,
                    'id':member.id,
                    'email':member.email
                }
                members.append(data)
            status = 'ok'
        except Group.DoesNotExist:
            status = 'ko'
            message = "%s doesn't exist in our database"%group_name
    variables = {
            'status':status,
            'message': message,
            'member': members,
        }
    # safe is set to False if the data is a list and not dict
    return JsonResponse(variables)

@ajax_required
def get_friends_list(request):
    """
    """
    # friends that request.user is following incase of friend invite
    friend_ids = request.user.following.values_list('id',flat=True)
    follower_ids = request.user.followers.values_list('id',flat=True)
    friends = User.objects.in_bulk(friend_ids)
    variables = []
    friend_names = [friends[friend].username for friend in friends]
    for friend in friends:
        data = {
            'name':friends[friend].username,
            'id':friends[friend].id,
            'email':friends[friend].email,
        }
        variables.append(data)
    return JsonResponse(variables,safe=False)

@login_required
def my_accountsettings(request):
    acct_settings = AccountSettings.objects.get_or_create(user=request.user)
    if request.method == 'POST':
        settings_form = AccountSettingsForm(data=request.POST) # Post a comment
        if settings_form.is_valid():
            # Now create a settings object but not yet saved to database
            new_settings = settings_form.save(commit=False)
            new_settings.user = request.user

            new_settings.save()

            return HttpResponseRedirect(reverse (
             'index'
            ))
    else:
        settings_form = AccountSettingsForm(instance=request.user)
    return render(request, 'account/account_settings_form.html',
                 {
                  'settings_form': settings_form
                 })
