from django.db import models
from django.conf import settings # used to create user profile
from django.contrib.auth.models import AbstractUser
from django.core.mail import EmailMessage
from django.core.mail import send_mail
from django.template.loader import get_template
from django.template import Context
from django.utils import timezone


# Create your models here.
class User(AbstractUser):
    GENDER = (
      (None,'Select gender'),
      ('M','Male'),
      ('F','Female')
    )
    gender = models.CharField(max_length=6,choices=GENDER,null=True)
    address = models.CharField(max_length=60,blank=True, null=True)
    # we are setting symmetrical=False to define a non-symmetric relation.
    # This is, if I follow you, it doesn't mean you automatically follow me
    following = models.ManyToManyField('self',
                                        through='Friendship',
                                        related_name='followers',
                                        symmetrical=False)
    is_teacher = models.BooleanField(default=False)
    is_premium = models.BooleanField(default=False)

class Profile(models.Model):
    GENDER = (
      (None,'Select gender'),
      ('M','Male'),
      ('F','Female')
    )
    user = models.OneToOneField(settings.AUTH_USER_MODEL,primary_key=True)
    gender = models.CharField(max_length=6,choices=GENDER,
                      blank=True,null=True)

    def __str__(self):
        return "{}'s profle".format(self.user.username)

class Subject(models.Model):
    name = models.CharField(max_length=30,)

    def __str__(self):
        return self.name

class Teacher(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    discipline = models.ManyToManyField(Subject)


class Group(models.Model):
    """
    Create database table for creation of groups
    """
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='creator')
    group_name = models.CharField(max_length=120,unique=True)
    members = models.ManyToManyField(settings.AUTH_USER_MODEL,through='Membership',through_fields=('group','person'))

    def __str__(self):
        return self.group_name


class Membership(models.Model):
    """
    Member details for each group
    """
    person = models.ForeignKey(User, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    date_joined = models.DateField()
    invited_by = models.ForeignKey(User,related_name='inviter')


class VerifyRegistration(models.Model):
    username = models.CharField(max_length=30)
    gender = models.CharField(max_length=6)
    email = models.EmailField()
    password = models.CharField(max_length=20)
    verify_code=models.CharField(max_length=25)

    def __str__(self):
        return "{}, {}".format(self.username,self.email)

    def send(self):
        subject = 'Confirm your email'
        link = 'http://{}/account/verify/email/{}/ok'.format(settings.SITE_HOST,
                                            self.verify_code,
        )
        home='http://www.pollsportal.com/'
        template = get_template("account/mail/verify_reg_email.html")
        context = {
           'link' : link,
           'home': home
        }
        message = template.render(context)
        send_mail(
          subject, message,settings.DEFAULT_FROM_EMAIL, [self.email]
        )


class Friendship(models.Model):
    from_friend = models.ForeignKey(User,
        related_name='friend_set'   #  this is like Following
        )
    to_friend = models.ForeignKey(User,
        related_name ='to_friend_set'  #this refer to followers
        )
    created = models.DateTimeField(default=timezone.now,db_index=True,)

    def __str__(self):
        return "{} follows {}".format(
        self.from_friend.username,self.to_friend.username
        )


class Invitation(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    code = models.CharField(max_length=20)
    sender = models.ForeignKey(User)

    def __str__(self):
        return "{}, {}".format(self.sender.username,self.email)

    def send(self):
        subject = 'Invitation to join PollsPortal'
        link = 'http://{}/polls/friend/accept/{}/'.format(settings.SITE_HOST,
                                            self.code,
        )
        template = get_template("polls/mail/invitation_email.html")
        context = {
           'name' : self.name,
           'link' : link,
           'domain':"www.pollsportal.com",
           'sender': self.sender,
        }
        print(self.sender)
        message = template.render(context)
        msg = EmailMessage(
          subject, message,settings.DEFAULT_FROM_EMAIL, [self.email]
        )
        msg.content_subtype = 'html' #present the mail in 'text/html'
        msg.send()


class AccountSettings(models.Model):
    user = models.OneToOneField(User,primary_key=True)
    vote_notify = models.BooleanField(default=True)
    follow_notify = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = "Account settings" # To display plural of category
