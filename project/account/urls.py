from django.conf.urls import url, include
from project.account import views
from django.contrib.auth import views as auth_views


app_name='account'
urlpatterns = [
    # sessions management
    url(r'^login/$', views.user_login, name='login'),
    url(r'^logout/$', auth_views.logout,{'template_name':'registration/loggedout.html'},name='logout'),
    # Account creation
    url(r'^verify/email/([-\w]+)/ok$', views.verify_registration, name='verify'),
    url(r'^register/$', views.register, name='register'),

    #Account Management
    url(r'^user/edit/$', views.edit_profile, name='edit_profile'),
    url(r'^settings/$', views.my_accountsettings, name='account_settings'),
    # Group Management
    url(r'^my-group-list/$', views.get_or_add_to_group, name="mygroups"),
    url(r'^create-new-group/$', views.create_group, name="create_group"),
    url(r'^get-group-members/$', views.get_group_members, name="group_members"),
    url(r'^friends-list/$', views.get_friends_list, name="friends_list"),

    # Password management
    url(r'^settings/password/$', auth_views.PasswordChangeView.as_view(
        template_name='registration/password_change.html',
        success_url='/account/settings/password/done'),
        name='password_change'),
    url(r'^settings/password/done/$', auth_views.PasswordChangeDoneView.as_view(
        template_name='registration/password_change_done.html'),
        name='password_change_done'),

    # restore password urls
    url(r'^reset/$',
        auth_views.PasswordResetView.as_view(
        template_name='registration/password_reset.html',
        email_template_name='registration/password_reset_email.html',
        subject_template_name='registration/password_reset_subject.txt',
        success_url='/account/reset/done'
        ),
        name='password_reset'),
    url(r'^reset/done/$',
        auth_views.PasswordResetDoneView.as_view(
        template_name='registration/password_reset_done.html'),
        name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.PasswordResetConfirmView.as_view(
        template_name='registration/password_reset_confirm.html',
        success_url='/account/reset/complete'),
        name='password_reset_confirm'),
    url(r'^reset/complete/$',
        auth_views.PasswordResetCompleteView.as_view(
        template_name='registration/password_reset_complete.html'),
        name='password_reset_complete'),
]
