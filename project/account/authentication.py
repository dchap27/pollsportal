from django.conf import settings
from .models import User



class EmailAuthBackend(object):
    """
    Authenticate using email Account
    """
    def authenticate(self,username=None, password=None):
        try:
            user = User.objects.get(email=username)
            if user.check_password(password):
                return user
            return None
        except User.DoesNotExist:
            return None

    def get_user(self,user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
