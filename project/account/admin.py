from django.contrib import admin
from project.account.models import *

# Register your models here.
class UserAdmin(admin.ModelAdmin):
	list_display = ('username','email','first_name','last_name','is_staff')

class AccountSettingsAdmin(admin.ModelAdmin):
    list_display = ('user','vote_notify','follow_notify')

class ProfileAdmin(admin.ModelAdmin):
    list_display=('user','gender',)

class MembershipInline(admin.TabularInline):
    model = Membership

class GroupAdmin(admin.ModelAdmin):
    list_display=('group_name','owner')
    inlines = [MembershipInline]

class FrienshipAdmin(admin.ModelAdmin):
    list_display = ('from_friend','to_friend',)

class InvitationAdmin(admin.ModelAdmin):
     list_display=('sender','email')

admin.site.register(User,UserAdmin)
admin.site.register(AccountSettings,AccountSettingsAdmin)
admin.site.register(Profile,ProfileAdmin)
admin.site.register(Group,GroupAdmin)
admin.site.register(Friendship,FrienshipAdmin)
admin.site.register(Invitation,InvitationAdmin)
