import re
from django import forms
from django.core.exceptions import ObjectDoesNotExist
from project.account.models import Profile, AccountSettings, Group, User

GENDER = (
      (None,'Select gender'),
      ('M','Male'),
      ('F','Female')
    )

class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

class RegistrationForm(forms.Form):
    username = forms.CharField(label='Username', max_length=30)
    gender = forms.ChoiceField(label='gender',choices=GENDER)
    email = forms.EmailField(label='Email')
    password1 = forms.CharField(
      label='Password',
      min_length=6,
      widget=forms.PasswordInput()
    )
    password2 = forms.CharField(
    label='Confirm password',
    widget=forms.PasswordInput()
    )

    def clean_username(self):
        username = self.cleaned_data['username']
        if not re.search(r'^\w+$', username):
            raise forms.ValidationError('Username can only contain \
            alphanumeric characters and the underscore.'
        )
        # To restrict username i.e reserved names!!
        if username.lower() in ['pollsportal',"polls",'pollportal',
               'pollsportals','admin','Admin','polladmin',
               'polladmins','pollsadmin'
               ]:
            raise forms.ValidationError('Username is already taken')
        if username in ['fuck','sex','prick']:
            raise forms.ValidationError("Sorry! You're not allowed to use such username")
        try:
            User.objects.get(username=username)
        except ObjectDoesNotExist:
            return username
        raise forms.ValidationError('Username is already taken.')

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            User.objects.get(email=email)
        except ObjectDoesNotExist:
            return email
        raise forms.ValidationError('Email is already registered')

    def clean_password2(self):
        if 'password1' in self.cleaned_data:
            password1 = self.cleaned_data['password1']
            password2 = self.cleaned_data['password2']
            if password1 == password2:
                return password2
        raise forms.ValidationError('Passwords do not match.')

class UserEditForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name','last_name')

class ProfileEditForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('gender',)

class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ['group_name']

    def clean_group_name(self):
        group_name = self.cleaned_data['group_name']
        try:
            Group.objects.get(group_name=group_name)
        except ObjectDoesNotExist:
            return group_name
        # if already exists
        raise forms.ValidationError("{} already taken!".format(group_name))
        # count = Group.objects.filter(group_name__icontains=group_name).count()
        # group_name += str(count)
        # return group_name

class FriendInviteForm(forms.Form):
    name = forms.CharField(label="Friend's name")
    email = forms.EmailField(label="Friend's Email")

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            User.objects.get(email=email)
        except ObjectDoesNotExist:
            return email
        raise forms.ValidationError(
            '{} is already a registered user.'.format(email))


class AccountSettingsForm(forms.ModelForm):
    class Meta:
        model= AccountSettings
        fields = ('vote_notify','follow_notify',)
